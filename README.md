# Team 12 Social Application

# Description

Social app is a SPA that allows the users to upload photos to your service and share them as public for everyone or private only for their followers. Users should be able to review their posts, to delete them or change their privacy, to update their profile or request delete of the account. They can also view, comment and like posts shared by others. They should be able to review their followers and the people they follow and the posts they liked. They could receive notifications if someone followed them or liked their post.

## Getting started

### Front-end
Make sure you have the [Angular CLI](https://github.com/angular/angular-cli#installation) installed globally. We use npm to manage the dependencies. Run `npm install` to resolve all dependencies (might take a minute).

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Back-end
Install dependencies with `npm install`
Create a ``.env`` file in root directory with config properties:
-   PORT=`BE serving port` default:3000
-   DB_TYPE=`Database type` default:mysql
-   DB_HOST=`Database host` default:localhost
-   DB_PORT=`Database port` default:3306
-   DB_USERNAME=`Database username` default:root
-   DB_PASSWORD=`Database password` default:root
-   DB_DATABASE_NAME=`Database/Schema name` default:social_app
-   TOKEN_SECRET_KEY=`ACCESS JWT Secret key` default:key
-   TOKEN_EXPIRES_IN=`ACCESS JWT expire time in zeit/ms` default:60000 1min
-   IMGUR_API_ROUTE=`https://api.imgur.com/3/`
-   IMGUR_API_CLIENT_ID=`Your IMGUR Client-ID`
    
----------

## Database

The example codebase uses [Typeorm](http://typeorm.io/) with a mySQL database.

Create `ormconfig.json` config file for typeORM database settings in root directory of BE
Set mysql database settings in `ormconfig.json`

    {
        "type": "mysql",
        "host": "localhost",
        "port": 3306,
        "username": "your_db_username",
        "password": "your_db_password",
        "database": "social_app",
        "entities": ["./src/data/entities/*.entity.ts"]
    }

    

Start local mysql server and create new database with the name you specified in the ``.env``

On application start, tables for all entities will be created.

----------

## NPM scripts

- `npm start` - Start application
- `npm run start:watch` - Start application in watch mode
- `npm run test` - run Jest test runner 
- `npm run start:prod` - Build application
- `npm run start:dev` - Compiling in watch mode
- Seeding
    -    ``npm run seed`` Executes roles seed
    -   ``npm run profile-seed -- --profiles=N`` generates N profiles (default: 20)
    -   ``npm run post-seed -- --posts=N`` generates N posts (default: 20)


----------

## Start application

- `npm start`
- Test api with `http://localhost:3000/swag-api/` in your favourite browser

----------

# Authentication
 
This applications uses JSON Web Token (JWT) to handle authentication. The token is passed with each request using the `Authorization` header with `Bearer` scheme. The JWT authentication middleware handles the validation and authentication of the token. Refresh token exists for the sole purpose of forging new valid access tokens.

----------
 
# Swagger API docs

This example repo uses the NestJS swagger module for API documentation. [NestJS Swagger](https://github.com/nestjs/swagger) - [www.swagger.io](https://swagger.io/)       


# Built With

- Angular 8 - framework used for our client.

- Angular Material - to design our components in the client.

- NestJS - framework for building our server.

- Akita - for state management.

- TypeORM - an ORM that can run in Node.js;

- JWT - for authentication.

- Imgur API - for uploading and serving images



# Authors and Contributors

- Yavor Filipow
- Iveta Krasteva
