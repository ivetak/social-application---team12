import { JwtHelperService } from '@auth0/angular-jwt';
import { SessionState, UserDataState } from './app/auth/state/session.store';
import 'hammerjs';
import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { enableAkitaProdMode, persistState } from '@datorama/akita';
import { debounceTime } from 'rxjs/operators';

if (environment.production) {
  enableProdMode();
  enableAkitaProdMode();
}

const jwtService = new JwtHelperService();

persistState({
  include: ['session', 'theme'],
  preStorageUpdate(storeName, state) {
    if (storeName !== 'session') {
      return state;
    }
    let currentSession = { user: state.user, token: state.token };
    if (currentSession.token && jwtService.isTokenExpired(currentSession.token.token)) {
      currentSession = { user: null, token: null };
    }

    const sessionState: SessionState = {
      ...state,
      ...currentSession,
      oldSessions: state.oldSessions.map((session: UserDataState) => {
        return jwtService.isTokenExpired(session.token.token)
          ? { ...session, token: null }
          : { ...session };
      }),
    };
    return sessionState;
  },
  preStoreUpdate(storeName: string, state: any) {
    return state;
  },
  preStorageUpdateOperator: () => debounceTime(100),
});

platformBrowserDynamic()
  .bootstrapModule(AppModule)
  .catch(err => console.error(err));
