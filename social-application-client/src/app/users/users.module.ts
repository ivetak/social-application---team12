import { NgModule } from '@angular/core';
import { UsersRoutingModule } from './users-routing.module';
import { ProfileComponent } from './profile/profile.component';
import { SharedModule } from '../shared';
import { ProfileFollowersComponent } from './profile-followers/profile-followers.component';
import { ProfileFollowingComponent } from './profile-following/profile-following.component';
import { ProfileSettingsComponent } from './profile-settings/profile-settings.component';
import { ProfilePostsComponent } from './profile-posts/profile-posts.component';
import { UserComponent } from './user/user.component';
import { UserListComponent } from './user-list/user-list.component';
import { ProfileCreatePostComponent } from '../users/profile-create-post/profile-create-post.component';
import { UploadAvatarComponent } from './profile-settings/upload-avatar/upload-avatar.component';
import { ProfileNotificationsComponent } from './profile-notifications/profile-notifications.component';

@NgModule({
  declarations: [
    ProfileComponent,
    ProfileFollowersComponent,
    ProfileFollowingComponent,
    ProfileSettingsComponent,
    ProfilePostsComponent,
    ProfileCreatePostComponent,
    UserComponent,
    UserListComponent,
    UploadAvatarComponent,
    ProfileNotificationsComponent,
  ],
  imports: [SharedModule, UsersRoutingModule],
})
export class UsersModule {}
