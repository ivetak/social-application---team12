import { Follower } from './../../shared/interfaces/users/follower.interface';
import { UsersQuery } from './../state/user.query';
import { SessionQuery } from '../../auth/state/session.query';
import { ActivatedRoute } from '@angular/router';
import { User } from './../state/user.model';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { switchMap, filter, map, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-profile-followers',
  templateUrl: './profile-followers.component.html',
  styleUrls: ['./profile-followers.component.scss'],
})
export class ProfileFollowersComponent implements OnInit, OnDestroy {
  public followersArray: Follower[];

  private loggedUser: User;
  private unsubscribe$: Subject<any>;
  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly sessionQuery: SessionQuery,
    private readonly usersQuery: UsersQuery,
  ) {
    this.unsubscribe$ = new Subject<any>();
    this.activatedRoute.data.pipe().subscribe(({ followers }) => {
      this.followersArray = followers;
    });

    this.sessionQuery.currentUser$
      .pipe(
        switchMap((loggedUser: User) => {
          this.loggedUser = loggedUser;
          return this.usersQuery.selectActive();
        }),
        switchMap((activeUser: User) =>
          this.usersQuery.selectUserFollowers(String(activeUser.id)),
        ),
        filter(profileFollowers => !!profileFollowers),
        takeUntil(this.unsubscribe$),
      )
      .subscribe(profileFollowers => {
        this.followersArray = profileFollowers.map((follower: User) => {
          const isFollow = !this.loggedUser.following.every(
            (user: User) => !(user.username === follower.username),
          );
          return {
            ...follower,
            isFollow,
          } as Follower;
        });
      });
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
