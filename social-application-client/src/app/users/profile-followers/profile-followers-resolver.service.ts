import { UserService } from '../state/user.service';
import { User } from '../state/user.model';
import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';
import { UsersQuery } from '../state/user.query';
import { Observable, zip } from 'rxjs';
import { switchMap, filter, take, map } from 'rxjs/operators';
import { SessionQuery } from 'src/app/auth/state/session.query';

@Injectable({ providedIn: 'root' })
export class ProfileFollowersResolver implements Resolve<User[]> {
  constructor(
    private readonly userService: UserService,
    private readonly usersQuery: UsersQuery,
    private readonly sessionQuery: SessionQuery,
  ) {}
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<any> | Promise<any> | any {
    return zip(
      this.usersQuery.selectActive(),
      this.sessionQuery.currentUser$,
    ).pipe(
      switchMap(([activeUser, loggedUser]) => {
        this.userService.getUserFollowers(String(activeUser.id));

        return zip(
          this.usersQuery.selectUserFollowers(String(activeUser.id)),
          this.sessionQuery.select('user'),
        );
      }),
      filter(
        ([profileFollowers, loggedUser]) => !!profileFollowers && !!loggedUser,
      ),
      map(([profileFollowers, loggedUser]) => {
        return profileFollowers.map((follower: User) => {
          const isFollower = !loggedUser.followers.every(
            (user: User) => !(user.username === follower.username),
          );

          return {
            ...follower,
            isFollower,
          };
        });
      }),
      filter((following: User[]) => !!following),
      take(1),
    );
  }
}
