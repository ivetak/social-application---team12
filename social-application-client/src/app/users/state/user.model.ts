import { UserRole } from './../../shared/enum';
import { ID } from '@datorama/akita';

export interface User {
  id: ID;
  username: string;
  email: string;
  avatar: string;
  roles: UserRole[];
  followers: User[];
  following: User[];
  followersCount: number;
  followingCount: number;
  postsCount: number;
}

/**
 * A factory function that creates User
 */
export function createUser(params: Partial<User>) {
  return params as User;
}
