import { PostsService } from './../../explore/state/posts.service';
import { SessionStore } from './../../auth/state/session.store';
import { UserStore } from './user.store';
import { UserService } from './user.service';
import { TestBed, async } from '@angular/core/testing';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { SessionQuery } from '../../auth/state/session.query';
import { of, interval, throwError } from 'rxjs';
import { API_CONFIG } from '../../config';
import { first, delay } from 'rxjs/operators';
describe('UserService', () => {

    let getUserService: () => UserService;

    let userStore;
    let sessionStore;
    let sessionQuery;
    let http;
    let postsService;

    beforeEach(async(() => {
        jest.clearAllMocks();

        userStore = {
            add() { },
            setActive() { },
            setError() { },
            setLoading() { },
            upsertMany() { },
            upsert() { },
            update() { },
            remove() { },
        };

        sessionStore = {
            update() { },


        };
        sessionQuery = {
            // get isLoggedIn$() {
            //     return of(true);
            // }
            get currentUser$() {
                return of(true);
            },
            get select() {
                return of(true);
            }
        };

        http = {
            get() { return of(); },
            post() { },
            put() { },
            delete() { },
            patch() { },
        };

        postsService = {
            getUserPosts() { }
        };


        TestBed.configureTestingModule({
            declarations: [],
            imports: [
                HttpClientModule
            ],
            providers: [
                UserService,
                HttpClient,
                UserStore,
                SessionStore,
                SessionQuery,
                PostsService,
            ],
        })
            .overrideProvider(UserStore, { useValue: userStore })
            .overrideProvider(SessionStore, { useValue: sessionStore })
            .overrideProvider(SessionQuery, { useValue: sessionQuery })
            .overrideProvider(PostsService, { useValue: postsService })
            .overrideProvider(HttpClient, { useValue: http });

        getUserService = () => TestBed.get(UserService);


    }));

    afterEach(async () => {
        jest.clearAllMocks();

    });

    it('should create', () => {
        expect(getUserService()).toBeTruthy();
    });

    it('constructor should subscribe for the current user', (done) => {
        const user = {};
        const spyOnConstructor = jest.spyOn(sessionQuery, 'currentUser$', 'get').mockReturnValue(of(user));

        const userService1 = getUserService();

        sessionQuery.currentUser$.subscribe(() => {
            expect(userService1).toHaveProperty('loggedUser', user);
            done();
        });
    });

    it('getActiveUser() should call http.get', () => {

        const username = 'string';
        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/${username}`;

        const mockedUserData = 'mockedUserData';
        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => of(mockedUserData));

        getUserService().getActiveUser(username);

        expect(http.get).toHaveBeenCalledWith(url);
        expect(http.get).toHaveBeenCalledTimes(1);
    });

    it('getActiveUser() should call userStore.add', (done) => {

        const username = 'string';
        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/${username}`;

        const mockedUserData = 'mockedUserData';
        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => of(mockedUserData));
        const spyOnUserStoreAdd = jest.spyOn(userStore, 'add').mockImplementation(() => { });

        getUserService().getActiveUser(username).subscribe(() => {
            expect(userStore.add).toHaveBeenCalledWith(mockedUserData);
            expect(userStore.add).toHaveBeenCalledTimes(1);
            done();
        });
    });

    it('getActiveUser() should call userStore.setActive', (done) => {

        const user = {
            id: '2',
            username: 'string',
        };
        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/${user.username}`;

        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => of(user));
        const spyOnUserStoreAdd = jest.spyOn(userStore, 'add').mockImplementation(() => { });
        const spyOnUserStoreSetActive = jest.spyOn(userStore, 'setActive').mockImplementation(() => { });

        getUserService().getActiveUser(user.username).subscribe(() => {
            expect(userStore.setActive).toHaveBeenCalledWith(user.id);
            expect(userStore.setActive).toHaveBeenCalledTimes(1);
            done();
        });
    });


    it('Unsuccessful method getActiveUser() should notify', (done) => {

        const user = {

            username: 'string',
        };
        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/${user.username}`;

        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => throwError('error'));
        const spyOnUserStoreAdd = jest.spyOn(userStore, 'add').mockImplementation(() => { });
        const spyOnUserStoreSetActive = jest.spyOn(userStore, 'setActive').mockImplementation(() => { });
        const spyOnError = jest.spyOn(userStore, 'setError');

        getUserService().getActiveUser(user.username).pipe(first()).subscribe(() => { done(); },
            (newError) => {
                expect(userStore.setError).toHaveBeenCalledWith('error');
                expect(userStore.setError).toHaveBeenCalledTimes(1);
                done();
            },
        );
    });

    it('getUser() should call http.get', () => {

        const user = {
            id: '2',
            username: 'string',
        };
        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/${user.username}`;

        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => of(user));

        getUserService().getUser(user.username);

        expect(http.get).toHaveBeenCalledWith(url);
        expect(http.get).toHaveBeenCalledTimes(1);
    });

    it('getUser() should call userStore.add', (done) => {
        const user = {
            id: '2',
            username: 'string',
        };
        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/${user.username}`;

        const mockedUserData = 'mockedUserData';
        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => of(mockedUserData));
        const spyOnUserStoreAdd = jest.spyOn(userStore, 'add').mockImplementation(() => { });

        getUserService().getActiveUser(user.username).subscribe(() => {
            expect(userStore.add).toHaveBeenCalledWith(mockedUserData);
            expect(userStore.add).toHaveBeenCalledTimes(1);
            done();
        });
    });

    it('Unsuccessful method getUser() should notify', (done) => {

        const user = {
            id: '2',
            username: 'string',
        };
        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/${user.username}`;

        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => throwError('error'));
        const spyOnUserStoreAdd = jest.spyOn(userStore, 'add').mockImplementation(() => { });
        const spyOnError = jest.spyOn(userStore, 'setError');

        getUserService().getActiveUser(user.username).pipe(first()).subscribe(() => { done(); },
            (newError) => {
                expect(userStore.setError).toHaveBeenCalledWith('error');
                expect(userStore.setError).toHaveBeenCalledTimes(1);
                done();
            },
        );
    });

    it('get() should call http.get', () => {

        const user = {
            id: '2',
            username: 'string',
        };
        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}?username=${user.username}`;

        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => of(user));

        getUserService().get(user.username);

        expect(http.get).toHaveBeenCalledWith(url);
        expect(http.get).toHaveBeenCalledTimes(1);
    });

    it('get() should call userStore.upsertMany', (done) => {

        const user = {
            id: '2',
            username: 'string',
        };
        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}?username=${user.username}`;

        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => of(user));
        const spyUserUpsertMany = jest.spyOn(userStore, 'upsertMany').mockImplementation(() => { });

        getUserService().get(user.username);

        http.get().pipe(delay(200)).subscribe(() => {
            expect(userStore.upsertMany).toHaveBeenCalledWith(user);
            done();

        });
    });

    it('get() should call userStore.setLoading', () => {

        const user = {
            id: '2',
            username: 'string',
        };
        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}?username=${user.username}`;


        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => of(user));
        const spyUserStore = jest.spyOn(userStore, 'setLoading').mockImplementation(() => of(true));

        getUserService().get(user.username);

        expect(userStore.setLoading).toHaveBeenCalledWith(true);

    });

    it('Unsuccessful method get() should notify', () => {

        const user = {
            id: '2',
            username: 'string',
        };
        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}?username=${user.username}`;
        const error = new Error('error');

        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => throwError(error.message));
        const spyOnError = jest.spyOn(userStore, 'setError');

        getUserService().get(user.username);

        expect(userStore.setError).toHaveBeenCalledWith(error.message);
        expect(userStore.setError).toHaveBeenCalledTimes(1);
    });


    it('followUser() should call http.put', () => {

        const user = {
            id: '2',
            username: 'string',
        };

        const userToFollowId = '2';
        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/${userToFollowId}/followers`;

        const spyOnHttp = jest.spyOn(http, 'put').mockImplementation(() => of(user));

        getUserService().followUser(userToFollowId);

        expect(http.put).toHaveBeenCalledWith(url, {});
        expect(http.put).toHaveBeenCalledTimes(1);
    });

    it('followUser() should call postsService.getUserPosts', () => {

        const user = {
            id: '3',
            username: 'string',
        };

        const userToFollowId = '2';
        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/${userToFollowId}/followers`;

        const spyOnHttp = jest.spyOn(http, 'put').mockImplementation(() => of(user));
        const spyOnUserPosts = jest.spyOn(postsService, 'getUserPosts').mockImplementation(() => of(userToFollowId));

        getUserService().followUser(userToFollowId);

        expect(postsService.getUserPosts).toHaveBeenCalledWith(user.id);
        expect(postsService.getUserPosts).toHaveBeenCalledTimes(1);
    });

    it('followUser() should call userStore.upsert', () => {

        const user = {
            id: '3',
            username: 'string',
        };

        const userToFollowId = '2';
        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/${userToFollowId}/followers`;

        const spyOnHttp = jest.spyOn(http, 'put').mockImplementation(() => of(user));
        const spyOnUserPosts = jest.spyOn(postsService, 'getUserPosts').mockImplementation(() => of(userToFollowId));
        const spyOnUsersUpsertMany = jest.spyOn(userStore, 'upsert').mockImplementation(() => { });

        getUserService().followUser(userToFollowId);

        expect(userStore.upsert).toHaveBeenCalledWith(user.id, user);
        expect(userStore.upsert).toHaveBeenCalledTimes(2);
    });

    it('followUser() should call sessionStore.update', (done) => {

        const user = {
            id: '3',
            username: 'string',
        };

        const userToFollowId = '2';
        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/${userToFollowId}/followers`;

        const spyOnHttp = jest.spyOn(http, 'put').mockImplementation(() => of(user));
        const spyOnUpdate = jest.spyOn(sessionStore, 'update').mockImplementation(() => { });

        getUserService().followUser(userToFollowId);
        http.put().subscribe(() => {
            expect(sessionStore.update).toHaveBeenCalledWith(expect.any(Function));
            expect(sessionStore.update).toHaveBeenCalledTimes(1);

            done();
        });
    });

    it('unfollowUser() should call http.delete', () => {

        const user = {
            id: '2',
            username: 'string',
        };

        const userToFollowId = '2';
        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/${userToFollowId}/followers`;

        const spyOnHttp = jest.spyOn(http, 'delete').mockImplementation(() => of(user));

        getUserService().unfollowUser(userToFollowId);

        expect(http.delete).toHaveBeenCalledWith(url, {});
        expect(http.delete).toHaveBeenCalledTimes(1);
    });


    it('unfollowUser() should call postsService.getUserPosts', () => {

        const user = {
            id: '3',
            username: 'string',
        };

        const userToFollowId = '2';
        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/${userToFollowId}/followers`;

        const spyOnHttp = jest.spyOn(http, 'delete').mockImplementation(() => of(user));
        const spyOnUserPosts = jest.spyOn(postsService, 'getUserPosts').mockImplementation(() => of(userToFollowId));

        getUserService().unfollowUser(userToFollowId);

        expect(postsService.getUserPosts).toHaveBeenCalledWith(user.id);
        expect(postsService.getUserPosts).toHaveBeenCalledTimes(1);
    });

    it('unfollowUser() should call userStore.upsert', () => {

        const user = {
            id: '3',
            username: 'string',
        };

        const userToFollowId = '2';
        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/${userToFollowId}/followers`;

        const spyOnHttp = jest.spyOn(http, 'delete').mockImplementation(() => of(user));
        const spyOnUserPosts = jest.spyOn(postsService, 'getUserPosts').mockImplementation(() => of(userToFollowId));
        const spyOnUsersUpsertMany = jest.spyOn(userStore, 'upsert').mockImplementation(() => { });

        getUserService().unfollowUser(userToFollowId);

        expect(userStore.upsert).toHaveBeenCalledWith(user.id, user);
        expect(userStore.upsert).toHaveBeenCalledTimes(2);
    });

    it('unfollowUser() should call sessionStore.update', (done) => {

        const user = {
            id: '3',
            username: 'string',
        };

        const userToFollowId = '2';
        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/${userToFollowId}/followers`;

        const spyOnHttp = jest.spyOn(http, 'delete').mockImplementation(() => of(user));
        const spyOnUpdate = jest.spyOn(sessionStore, 'update').mockImplementation(() => { });

        getUserService().unfollowUser(userToFollowId);
        http.delete().subscribe(() => {
            expect(sessionStore.update).toHaveBeenCalledWith(expect.any(Function));
            expect(sessionStore.update).toHaveBeenCalledTimes(1);

            done();
        });
    });

    it('getUserFollowers() should call http.get', () => {

        const user = {
            id: '2',
            username: 'string',
        };
        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/${user.id}/followers`;


        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => of(user));

        getUserService().getUserFollowers(user.id);

        expect(http.get).toHaveBeenCalledWith(url);
        expect(http.get).toHaveBeenCalledTimes(1);
    });

    it('getUserFollowers() should call userStore.upsert', () => {

        const user = {
            id: '3',
            username: 'string',
        };

        const userToUnfollowId = '2';
        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/${user.id}/followers`;

        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => of(user));
        const spyOnUsersUpsert = jest.spyOn(userStore, 'upsert').mockImplementation(() => { });

        getUserService().getUserFollowers(userToUnfollowId);

        const fn = (us) => ({
            followers: [],
        });

        expect(userStore.upsert).toHaveBeenCalledWith(user, expect.any(Function));
        expect(userStore.upsert).toHaveBeenCalledTimes(1);
    });

    it('getUserFollowers() should call userStore.setLoading', () => {

        const user = {
            id: '2',
            username: 'string',
        };
        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/${user.id}/followers`;

        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => of(user));
        const spyUserStore = jest.spyOn(userStore, 'setLoading').mockImplementation(() => of(false));

        getUserService().getUserFollowers(user.id);

        expect(userStore.setLoading).toHaveBeenCalledWith(false);
        expect(userStore.setLoading).toHaveBeenCalledTimes(1);

    });

    it('Unsuccessful method getUserFollowers() should notify', () => {

        const user = {
            id: '2',
            username: 'string',
        };
        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/${user.id}/followers`;


        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => throwError('error'));
        const spyOnError = jest.spyOn(userStore, 'setError');

        getUserService().getUserFollowers(user.id);

        expect(userStore.setError).toHaveBeenCalledWith('error');
        expect(userStore.setError).toHaveBeenCalledTimes(1);
    });

    it('getUserFollowing() should call http.get', () => {

        const user = {
            id: '2',
            username: 'string',
        };
        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/${user.id}/following`;

        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => of(user));

        getUserService().getUserFollowing(user.id);

        expect(http.get).toHaveBeenCalledWith(url);
        expect(http.get).toHaveBeenCalledTimes(1);
    });

    it('getUserFollowing() should call userStore.upsert', () => {

        const user = {
            id: '3',
            username: 'string',
        };

        const userToUnfollowId = '2';
        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/${user.id}/following`;

        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => of(user));
        const spyOnUsersUpsert = jest.spyOn(userStore, 'upsert').mockImplementation(() => { });

        getUserService().getUserFollowing(userToUnfollowId);

        const fn = (us) => ({
            followers: [],
        });

        expect(userStore.upsert).toHaveBeenCalledWith(user, expect.any(Function));
        expect(userStore.upsert).toHaveBeenCalledTimes(1);
    });

    it('getUserFollowing() should call userStore.setLoading', () => {

        const user = {
            id: '2',
            username: 'string',
        };
        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/${user.id}/following`;

        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => of(user));
        const spyUserStore = jest.spyOn(userStore, 'setLoading').mockImplementation(() => of(false));

        getUserService().getUserFollowing(user.id);

        expect(userStore.setLoading).toHaveBeenCalledWith(false);
        expect(userStore.setLoading).toHaveBeenCalledTimes(1);

    });

    it('Unsuccessful method getUserFollowing() should notify', () => {

        const user = {
            id: '2',
            username: 'string',
        };
        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/${user.id}/following`;

        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => throwError('error'));
        const spyOnError = jest.spyOn(userStore, 'setError');

        getUserService().getUserFollowing(user.id);

        expect(userStore.setError).toHaveBeenCalledWith('error');
        expect(userStore.setError).toHaveBeenCalledTimes(1);
    });

    it('updateSearchTerm() should call userStore.update', () => {

        const search = {
            searchTerm: 'user',
        };

        const spyUserStore = jest.spyOn(userStore, 'update').mockImplementation(() => { });

        getUserService().updateSearchTerm(search.searchTerm);

        expect(userStore.update).toHaveBeenCalledWith(search);
        expect(userStore.update).toHaveBeenCalledTimes(1);

    });

    it('searchUsers() should call userStore.setLoading', () => {

        const searchTerm = 'user';

        const spyUserStore = jest.spyOn(userStore, 'setLoading').mockImplementation(() => of(true));

        getUserService().searchUsers(searchTerm);

        expect(userStore.setLoading).toHaveBeenCalledWith(true);
        expect(userStore.setLoading).toHaveBeenCalledTimes(1);

    });

    it('Successful method changeAvatar() should call http.put', () => {

        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/avatar`;

        const blob = {
            size: 2,
            type: 'png',
            slice() { return new Blob(); },
        };

        const mockedAvatarData = new FormData();
        mockedAvatarData.append('avatar', blob);

        const spyOnHttp = jest.spyOn(http, 'put').mockImplementation(() => of(mockedAvatarData));

        getUserService().changeAvatar(blob).subscribe(() => {
            expect(http.put).toHaveBeenCalledWith(url, mockedAvatarData);

        });

    });
    it('Successful method changeAvatar() should call userStore.update', () => {

        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/avatar`;

        const user = {
            id: '2',
        };
        const blob = {
            size: 2,
            type: 'png',
            slice() { return new Blob(); },
        };

        const mockedAvatarData = new FormData();
        mockedAvatarData.append('avatar', blob);

        const spyOnHttp = jest.spyOn(http, 'put').mockImplementation(() => of(mockedAvatarData));
        const spyOnStoreUpdate = jest.spyOn(userStore, 'update').mockImplementation(() => { });

        getUserService().changeAvatar(blob).subscribe(() => {
            expect(userStore.update).toHaveBeenCalledTimes(1);
            expect(userStore.update).toHaveBeenCalledWith(user.id, user);
        });

    });

});
