import { SessionQuery } from '../../auth/state/session.query';
import { PostsService } from './../../explore/state/posts.service';
import { SessionStore, SessionState } from './../../auth/state/session.store';
import { map, catchError } from 'rxjs/operators';
import { API_CONFIG } from './../../config/index';
import { Injectable } from '@angular/core';
import { ID, arrayAdd } from '@datorama/akita';
import { HttpClient } from '@angular/common/http';
import { UserStore } from './user.store';
import { User } from './user.model';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class UserService {
  private loggedUser: User;

  constructor(
    private readonly userStore: UserStore,
    private readonly sessionStore: SessionStore,
    private readonly sessionQuery: SessionQuery,
    private readonly http: HttpClient,
    private readonly postsService: PostsService,
  ) {
    this.sessionQuery.currentUser$.subscribe((loggedUser: User) => {
      this.loggedUser = loggedUser;
    });
  }

  getActiveUser(username: string) {
    return this.http
      .get<User>(
        `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/${username}`,
      )
      .pipe(
        map((user: User) => {
          this.userStore.add(user);
          this.userStore.setActive(user.id);
          return user;
        }),
        catchError(err => {
          this.userStore.setError(err);
          return err;
        }),
      );
  }

  getUser(username: string): Observable<unknown> {
    return this.http
      .get<User>(
        `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/${username}`,
      )
      .pipe(
        map((user: User) => {
          this.userStore.add(user);
        }),
        catchError(err => {
          this.userStore.setError(err);
          return err;
        }),
      );
  }

  get(username: string) {
    this.userStore.setLoading(true);
    this.http
      .get<User[]>(
        `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}?username=${username}`,
      )
      .subscribe(
        entities => {
          this.userStore.upsertMany(entities);
        },
        error => this.userStore.setError(error),
        () => this.userStore.setLoading(false),
      );
  }

  followUser(userToFollowId: string) {
    this.http
      .put<User>(
        `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/${userToFollowId}/followers`,
        {},
      )
      .subscribe((followedUser: User) => {
        this.postsService.getUserPosts(String(followedUser.id));
        this.userStore.upsert(followedUser.id, followedUser);
        this.sessionStore.update((sessionState: SessionState) => ({
          ...sessionState,
          user: {
            ...sessionState.user,
            following: [...sessionState.user.following, followedUser],
            followingCount: sessionState.user.followingCount + 1,
          },
        }));
        this.userStore.upsert(this.loggedUser.id, this.loggedUser);
      });
  }

  unfollowUser(userToFollowId: string) {
    this.http
      .delete<User>(
        `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/${userToFollowId}/followers`,
        {},
      )
      .subscribe((unfollowedUser: User) => {
        this.postsService.getUserPosts(String(unfollowedUser.id));
        this.userStore.upsert(unfollowedUser.id, unfollowedUser);
        this.sessionStore.update((sessionState: SessionState) => ({
          ...sessionState,
          user: {
            ...sessionState.user,
            following: [
              ...sessionState.user.following.filter(
                followedUser =>
                  followedUser.username !== unfollowedUser.username,
              ),
            ],
            followingCount: sessionState.user.followingCount - 1,
          },
        }));
        this.userStore.upsert(this.loggedUser.id, this.loggedUser);
      });
  }

  getUserFollowers(userId: string) {
    this.http
      .get<User[]>(
        `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/${userId}/followers`,
      )
      .subscribe(
        (users: User[]) => {
          this.userStore.upsert(userId, entity => ({
            followers: entity.followers,
          }));
        },
        error => this.userStore.setError(error),
        () => this.userStore.setLoading(false),
      );
  }

  getUserFollowing(userId: string) {
    this.http
      .get<User[]>(
        `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/${userId}/following`,
      )
      .subscribe(
        (users: User[]) => {
          this.userStore.upsert(userId, entity => ({
            followers: entity.followers,
          }));
        },
        error => this.userStore.setError(error),
        () => this.userStore.setLoading(false),
      );
  }

  add(user: User) {
    this.userStore.add(user);
  }

  update(id, user: Partial<User>) {
    this.userStore.update(id, user);
  }

  remove(id: ID) {
    this.userStore.remove(id);
  }

  updateSearchTerm(searchTerm: string) {
    this.userStore.update({ searchTerm });
  }

  searchUsers(keyword: string) {
    this.userStore.setLoading(true);
  }

  changeAvatar(avatar: Blob) {
    const formData = new FormData();
    formData.append('avatar', avatar);
    return this.http
      .put<User>(
        `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/avatar`,
        formData,
      )
      .pipe(
        map((updatedUser: User) => {
          this.userStore.update(updatedUser.id, updatedUser);
          return true;
        }),
      );
  }
}
