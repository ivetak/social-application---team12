import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { UserStore, UserState } from './user.store';
import { User } from './user.model';
import { switchMap, filter, throttle } from 'rxjs/operators';
import { interval, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UsersQuery extends QueryEntity<UserState, User> {
  public searchTerm$ = this.select('searchTerm');
  public searchUsers$ = this.searchTerm$.pipe(
    throttle(_ => interval(1000)),
    switchMap(searchTerm =>
      !!searchTerm && searchTerm.length > 0
        ? this.selectAll({
            filterBy: entity =>
              entity.username.toLowerCase().includes(searchTerm) ||
              entity.email
                .split('@')[0]
                .toLowerCase()
                .includes(searchTerm),
          })
        : of([]),
    ),
  );

  constructor(protected store: UserStore) {
    super(store);
  }

  getUsers(value: string) {
    return this.selectAll({
      filterBy: entity => entity.username.toLowerCase().includes(value),
    });
  }

  selectUserFollowing(id: string) {
    return this.selectEntity(id, entity => entity.following);
  }

  selectUserFollowers(id: string) {
    return this.selectEntity(id, entity => entity.followers);
  }
}
