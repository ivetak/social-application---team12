import { avatarUrlFilter } from './../../common';
import { User } from './user.model';
import { Injectable } from '@angular/core';
import {
  EntityState,
  EntityStore,
  StoreConfig,
  ActiveState,
} from '@datorama/akita';

export interface UserState extends EntityState<User>, ActiveState {
  searchterm: string;
}

const initialState: UserState = {
  active: null,
  searchterm: null,
};

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'user', idKey: 'id' })
export class UserStore extends EntityStore<UserState, User> {
  constructor() {
    super(initialState);
  }

  akitaPreAddEntity(user: User) {
    return {
      ...user,
      avatar: avatarUrlFilter(user.avatar),
      followers: [
        ...user.followers.map(follower => ({
          ...follower,
          avatar: avatarUrlFilter(follower.avatar),
        })),
      ],
      following: [
        ...user.following.map(followedUser => ({
          ...followedUser,
          avatar: avatarUrlFilter(followedUser.avatar),
        })),
      ],
    };
  }

  akitaPreUpdateEntity(_, user: User) {
    return {
      ...user,
      avatar: avatarUrlFilter(user.avatar),
      followers: [
        ...user.followers.map(follower => ({
          ...follower,
          avatar: avatarUrlFilter(follower.avatar),
        })),
      ],
      following: [
        ...user.following.map(followedUser => ({
          ...followedUser,
          avatar: avatarUrlFilter(followedUser.avatar),
        })),
      ],
    };
  }
}
