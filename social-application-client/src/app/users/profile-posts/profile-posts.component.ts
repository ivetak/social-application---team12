import { SessionQuery } from './../../auth/state/session.query';
import { UsersQuery } from './../state/user.query';
import {
  profilePostsRequestSkip,
  profilePostsQueryStep,
  profilePostsQueryStepIncrement,
  profilePostsRequestSkipThreshold,
  profilePostsRequestSkipIncrement,
  profilePostsQueryDefaultLimit,
  profilePostsRequestDefaultLimit,
} from './../../shared/constants/posts.constants';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Post } from '../../explore/state/post.model';
import { PostsQuery } from '../../explore/state/posts.query';
import { PostsService } from '../../explore/state/posts.service';
import { ActivatedRoute } from '@angular/router';
import { first, takeUntil, map, tap } from 'rxjs/operators';
import { Observable, Subject, of } from 'rxjs';
import { User } from '../state/user.model';
import { EntityActions } from '@datorama/akita';

@Component({
  selector: 'app-profile-posts',
  templateUrl: './profile-posts.component.html',
  styleUrls: ['./profile-posts.component.scss'],
})
export class ProfilePostsComponent implements OnInit, OnDestroy {
  public posts$: Observable<Post[]>;
  public skip = profilePostsRequestSkip; // request
  public step = profilePostsQueryStep; // query
  public requestLoading$: Observable<boolean>;
  public postRequestEnd$: Observable<boolean>;
  public postsQueryEnd$: Observable<boolean>;

  private loggedUser: User;
  private followedByLoggedUser: boolean;
  private postsCount: number;
  private user: User;
  private unsubscribe$: Subject<any>;
  constructor(
    private readonly postsQuery: PostsQuery,
    private readonly postsService: PostsService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly usersQuery: UsersQuery,
    private readonly sessionQuery: SessionQuery,
  ) {
    this.unsubscribe$ = new Subject<any>();
    this.usersQuery
      .selectActive()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((activeUser: User) => {
        this.user = activeUser;
      });
    this.sessionQuery.currentUser$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(loggedUser => {
        if (
          this.loggedUser &&
          loggedUser.id !== '0' &&
          loggedUser.followingCount !== this.loggedUser.followingCount
        ) {
          this.resetPosts();
        }
        this.loggedUser = loggedUser;
        this.followedByLoggedUser =
          loggedUser.following
            .map(followedUser => followedUser.username)
            .includes(this.user.username) ||
          loggedUser.username === this.user.username;
      });
    this.posts$ = this.activatedRoute.data.pipe(
      tap(console.log),
      map(({ posts }) => posts.slice(0, this.step)),
    );
  }

  ngOnInit() {
    if (this.step !== profilePostsQueryStep) {
      this.posts$ = this.queryPosts(this.step);
    }
    this.postsQuery
      .selectEntityAction()
      .pipe(first())
      .subscribe(() => (this.posts$ = this.queryPosts(this.step)));

    this.postsQuery
      .selectCount(post => post.author.username === this.user.username)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(count => (this.postsCount = count));
    this.requestLoading$ = this.postsQuery.selectLoading();
    this.postsQueryEnd$ = this.postsQuery
      .selectCount(
        (entity: Post) => entity.author.username === this.user.username,
      )
      .pipe(map(postsCount => this.step > postsCount));
    this.postRequestEnd$ = this.postsQuery.postEnd$;
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public postsOnScroll() {
    this.step += profilePostsQueryStepIncrement;
    this.posts$ = this.queryPosts(this.step);
    if (
      !(this.step % profilePostsRequestSkipThreshold) ||
      this.step > this.postsCount
    ) {
      this.skip += profilePostsRequestSkipIncrement;
      this.requestPosts(this.skip);
    }
  }

  private queryPosts(
    step: number,
    limit: number = profilePostsQueryDefaultLimit,
  ): Observable<Post[]> {
    return this.postsQuery
      .selectAll({
        filterBy: [
          entity =>
            this.followedByLoggedUser
              ? entity.author.username === this.user.username
              : (entity.author.username === this.user.username &&
                  entity.public) ||
                this.loggedUser.username === this.user.username,
        ],
      })
      .pipe(
        map((posts: Post[]) => posts.slice(0, step)),
        takeUntil(this.unsubscribe$),
      );
  }

  private requestPosts(
    skip: number,
    limit: number = profilePostsRequestDefaultLimit,
  ) {
    this.postsService
      .getUserPosts(this.user.username, skip, limit)
      .pipe(first())
      .subscribe(() => {
        this.posts$ = this.queryPosts(this.step);
      });
  }

  private resetPosts() {
    this.skip = profilePostsRequestSkip;
    this.step = profilePostsQueryStep;
    this.requestPosts(this.skip);
    this.posts$ = this.queryPosts(this.step);
  }
}
