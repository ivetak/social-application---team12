import { SessionQuery } from 'src/app/auth/state/session.query';
import { User } from 'src/app/users/state/user.model';
import { switchMap, filter, take } from 'rxjs/operators';
import { UsersQuery } from './../state/user.query';
import { PostsQuery } from './../../explore/state/posts.query';
import { PostsService } from './../../explore/state/posts.service';
import { Observable } from 'rxjs';
import { Post } from './../../explore/state/post.model';
import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';

@Injectable({ providedIn: 'root' })
export class ProfilePostsResolver implements Resolve<Post[]> {
  constructor(
    private readonly postsService: PostsService,
    private readonly postsQuery: PostsQuery,
    private readonly userQuery: UsersQuery,
    private readonly sessionQuery: SessionQuery,
  ) {}
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<any> | Promise<any> | any {
    let activeProfileUser: User;
    return this.userQuery.selectActive().pipe(
      switchMap((activeUser: User) => {
        activeProfileUser = activeUser;
        return this.postsService.getUserPosts(String(activeUser.id));
      }),
      switchMap(() => {
        return this.sessionQuery.currentUser$;
      }),
      switchMap((loggedUser: User) => {
        const followedByLoggedUser =
          loggedUser.following
            .map(followedUser => followedUser.username)
            .includes(activeProfileUser.username) ||
          loggedUser.username === activeProfileUser.username;

        return this.postsQuery.selectAll({
          filterBy: entity =>
            followedByLoggedUser
              ? entity.author.id === activeProfileUser.id
              : (entity.author.id === activeProfileUser.id && entity.public) ||
                loggedUser.username === activeProfileUser.username,
        });
      }),
      filter((posts: Post[]) => !!posts),
      take(1),
    );
  }
}
