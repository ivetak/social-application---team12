import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  Component,
  OnInit,
  ViewChild,
  Output,
  EventEmitter,
} from '@angular/core';
import { CropperComponent } from 'angular-cropperjs';
import {
  ImageTypeValidator,
  ImageSizeValidator,
} from '../../../shared/validators';
import {
  validImageTypes,
  validImageSize,
} from '../../../shared/constants/validation.constants';

@Component({
  selector: 'app-upload-avatar',
  templateUrl: './upload-avatar.component.html',
  styleUrls: ['./upload-avatar.component.scss'],
})
export class UploadAvatarComponent implements OnInit {
  @Output() public changeAvatar: EventEmitter<Blob>;

  public changeAvatarForm: FormGroup;
  public imageCropperFile: File;
  public imageCropperUrl: string | ArrayBuffer;
  public cropperConfig;

  @ViewChild('angularCropper', { static: false })
  public angularCropper: CropperComponent;

  constructor(private readonly fb: FormBuilder) {
    this.changeAvatar = new EventEmitter<Blob>();
    this.changeAvatarForm = this.fb.group({
      avatar: [
        null,
        Validators.compose([
          Validators.required,
          ImageTypeValidator(validImageTypes),
          ImageSizeValidator(validImageSize),
        ]),
      ],
    });
  }

  ngOnInit() {
    this.cropperConfig = {
      center: true,
      guides: true,
      viewMode: 0,
      responsive: true,
      dragMode: 'move',
      aspectRatio: 1,
      initialAspectRatio: 1,
      scalable: false,
      minCanvasWidth: 200,
      minCanvasHeight: 200,
      minContainerHeight: 200,
      minContainerWidth: 200,
      minCropBoxWidth: 270,
      minCropBoxHeight: 270,
      movable: true,
      zoomable: true,
      modal: false,
      background: false,
      cropBoxMovable: false,
      cropBoxResizable: false,
      toggleDragModeOnDblclick: false,
      zoom: e => {},
      crop: e => {},
      cropstart: e => {},
      cropend: e => {},
      ready: e => {
        const container = this.angularCropper.cropper.getContainerData();
        this.angularCropper.cropper.setCropBoxData({
          height: container.height,
          width: container.width,
          left: 0,
          top: 0,
        });
      },
    };
  }

  uploadAvatar() {
    this.angularCropper.cropper.getCroppedCanvas().toBlob(blob => {
      this.changeAvatar.emit(blob);
    });
  }

  uploadedImage(avatar: File) {
    console.log(avatar);
    this.changeAvatarForm.patchValue({ avatar });
    if (this.changeAvatarForm.controls.avatar.valid) {
      this.imageCropperFile = this.changeAvatarForm.controls.avatar.value;
      const fr = new FileReader();
      fr.onloadend = e => {
        this.imageCropperUrl = fr.result;
      };
      fr.readAsDataURL(avatar);
    } else {
      console.log(this.changeAvatarForm.controls.avatar.errors);

      this.changeAvatarForm.patchValue({ avatar: null });
    }
  }

  resetComponent() {
    this.changeAvatarForm.reset();
    this.imageCropperFile = null;
    this.imageCropperUrl = null;
  }
}
