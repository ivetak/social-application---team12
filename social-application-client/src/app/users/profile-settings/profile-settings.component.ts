import { APP_THEMES } from './../../config/theme.config';
import { ThemeService } from './../../core/state/theme.service';
import { ThemeQuery } from './../../core/state/theme.query';
import { Observable, Subject } from 'rxjs';
import { ToastrNotificationService } from '../../common/notification/toastr-notification.service';
import { UploadAvatarComponent } from './upload-avatar/upload-avatar.component';
import { UserService } from './../state/user.service';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-profile-settings',
  templateUrl: './profile-settings.component.html',
  styleUrls: ['./profile-settings.component.scss'],
})
export class ProfileSettingsComponent implements OnInit, OnDestroy {
  public darkTheme$: Observable<boolean>;
  public themes: string[];

  @ViewChild('uploadAvatar', { static: false })
  uploadAvatar: UploadAvatarComponent;

  private unsubscribe$: Subject<any>;
  constructor(
    private readonly userService: UserService,
    private readonly toastrNotificationService: ToastrNotificationService,
    private readonly themeQuery: ThemeQuery,
    private readonly themeService: ThemeService,
  ) {
    this.unsubscribe$ = new Subject<any>();
    this.darkTheme$ = this.themeQuery.isDark$;
    this.themes = this.themeQuery.availableThemes;
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public changeAvatar(avatar: Blob) {
    this.userService.changeAvatar(avatar).subscribe((success: boolean) => {
      if (success) {
        this.toastrNotificationService.avatarChangedNotification();
        this.uploadAvatar.resetComponent();
      }
    });
  }

  public changeTheme(themeName: string) {
    this.themeService.changeTheme(APP_THEMES[themeName]);
  }

  public toggleDark() {
    this.themeService.switchLight();
  }
}
