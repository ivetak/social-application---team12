import { SessionQuery } from '../../auth/state/session.query';
import { ActivatedRoute } from '@angular/router';
import { Follower } from './../../shared/interfaces/users/follower.interface';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from '../state/user.model';
import { UserService } from '../state/user.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit {
  @Input() user: Follower;

  @Input() profile: Follower;

  @Output() toggleFollow = new EventEmitter<boolean>();

  public loggedUser$: Observable<User>;

  constructor(
    private readonly userService: UserService,
    private readonly sessionQuery: SessionQuery,
  ) {
    this.loggedUser$ = this.sessionQuery.currentUser$;
  }

  ngOnInit() {}

  public toggleFollowing() {
    if (!this.user.isFollow) {
      this.userService.followUser(this.user.id);
      this.toggleFollow.emit(true);
    } else {
      this.userService.unfollowUser(this.user.id);
      this.toggleFollow.emit(false);
    }
  }
}
