import {
  profileNotificationsRequestSkip,
  profileNotificationsQueryStep,
  profileNotificationsQueryStepIncrement,
  profileNotificationsRequestDefaultLimit,
  profileNotificationsQueryDefaultLimit,
  profileNotificationsRequestSkipIncrement,
  profileNotificationsRequestSkipThreshold,
} from './../../shared/constants/posts.constants';
import { NotificationsService } from './../../notifications/state/notifications.service';
import { NotificationsQuery } from './../../notifications/state/notifications.query';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { first, takeUntil, map } from 'rxjs/operators';
import { Subject, Observable, of } from 'rxjs';
import { Notification } from '../../notifications/state/notification.model';

@Component({
  selector: 'app-profile-notifications',
  templateUrl: './profile-notifications.component.html',
  styleUrls: ['./profile-notifications.component.scss'],
})
export class ProfileNotificationsComponent implements OnInit, OnDestroy {
  public notifications$: Observable<Notification[]>;
  public skip: number = profileNotificationsRequestSkip;
  public step: number = profileNotificationsQueryStep;
  public requestLoading$: Observable<boolean>;
  public notificationsRequestEnd$: Observable<boolean>;
  public notificationsQueryEnd$: Observable<boolean>;

  private notificationsCount: number;
  private readonly unsubscribe$: Subject<any>;
  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly notificationsQuery: NotificationsQuery,
    private readonly notificationsService: NotificationsService,
  ) {
    this.unsubscribe$ = new Subject<any>();
    this.activatedRoute.data.pipe(first()).subscribe(({ notifications }) => {
      this.notifications$ = of(notifications);
    });
  }

  ngOnInit() {
    if (this.step !== profileNotificationsQueryStep) {
      this.notifications$ = this.queryNotifications(this.step);
    }

    this.requestLoading$ = this.notificationsQuery.selectLoading();
    this.notificationsQueryEnd$ = this.notificationsQuery.selectCount().pipe(
      map((notificationsCount: number) => {
        this.notificationsCount = notificationsCount;
        return this.step > notificationsCount;
      }),
    );
    this.notificationsRequestEnd$ = this.notificationsQuery.notificationsEnd$;
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public notificationsOnScroll() {
    this.step += profileNotificationsQueryStepIncrement;
    this.notifications$ = this.queryNotifications(this.step);
    if (
      !(this.step % profileNotificationsRequestSkipThreshold) ||
      this.step > this.notificationsCount
    ) {
      this.skip += profileNotificationsRequestSkipIncrement;
      this.requestNotifications(this.skip);
    }
  }

  private queryNotifications(
    step: number,
    limit: number = profileNotificationsQueryDefaultLimit,
  ): Observable<Notification[]> {
    return this.notificationsQuery
      .selectAll({
        limitTo: step,
      })
      .pipe(
        map((notifications: Notification[]) => notifications.slice(0, step)),
        takeUntil(this.unsubscribe$),
      );
  }

  private requestNotifications(
    skip: number,
    limit: number = profileNotificationsRequestDefaultLimit,
  ) {
    this.notificationsService.getNotifications(skip, limit);
  }
}
