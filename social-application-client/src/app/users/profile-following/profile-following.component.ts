import { SessionQuery } from 'src/app/auth/state/session.query';
import { UsersQuery } from './../state/user.query';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Follower } from '../../shared/interfaces/users/follower.interface';
import { User } from '../state/user.model';
import { switchMap, filter, map, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-profile-following',
  templateUrl: './profile-following.component.html',
  styleUrls: ['./profile-following.component.scss'],
})
export class ProfileFollowingComponent implements OnInit, OnDestroy {
  public followingArray: Follower[];
  public loggedUser: User;

  private unsubscribe$: Subject<any>;
  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly usersQuery: UsersQuery,
    private readonly sessionQuery: SessionQuery,
  ) {
    this.unsubscribe$ = new Subject<any>();
    this.activatedRoute.data.pipe().subscribe(({ following }) => {
      this.followingArray = following;
    });

    this.sessionQuery.currentUser$
      .pipe(
        switchMap((loggedUser: User) => {
          this.loggedUser = loggedUser;
          return this.usersQuery.selectActive();
        }),
        switchMap((activeUser: User) => {
          return this.usersQuery.selectUserFollowing(String(activeUser.id));
        }),
        filter(profileFollowing => {
          return !!profileFollowing;
        }),
        takeUntil(this.unsubscribe$),
      )
      .subscribe(profileFollowing => {
        this.followingArray = profileFollowing.map((follower: User) => {
          const isFollow = !this.loggedUser.following.every(
            (user: User) => !(user.username === follower.username),
          );
          return {
            ...follower,
            isFollow,
          } as Follower;
        });
      });
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
