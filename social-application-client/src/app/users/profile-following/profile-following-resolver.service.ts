import { Follower } from './../../shared/interfaces/users/follower.interface';
import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';
import { User } from '../state/user.model';
import { UserService } from '../state/user.service';
import { UsersQuery } from '../state/user.query';
import { Observable, zip } from 'rxjs';
import { switchMap, filter, take, map } from 'rxjs/operators';
import { SessionQuery } from 'src/app/auth/state/session.query';

@Injectable({ providedIn: 'root' })
export class ProfileFollowingResolver implements Resolve<Follower[]> {
  constructor(
    private readonly userService: UserService,
    private readonly usersQuery: UsersQuery,
    private readonly sessionQuery: SessionQuery,
  ) {}
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<any> | Promise<any> | any {
    return zip(
      this.usersQuery.selectActive(),
      this.sessionQuery.currentUser$,
    ).pipe(
      switchMap(([activeUser, loggedUser]) => {
        this.userService.getUserFollowing(String(activeUser.id));

        return zip(
          this.usersQuery.selectUserFollowing(String(activeUser.id)),
          this.sessionQuery.select('user'),
        );
      }),
      filter(([profileFollowing, loggedUser]) => {
        return !!profileFollowing && !!loggedUser;
      }),
      map(([profileFollowing, loggedUSer]) => {
        return profileFollowing.map((follower: User) => {
          const isFollow = !loggedUSer.following.every(
            (user: User) => !(user.username === follower.username),
          );
          return {
            ...follower,
            isFollow,
          };
        });
      }),
      filter((following: User[]) => !!following),
      take(1),
    );
  }
}
