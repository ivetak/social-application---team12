import { NotificationsQuery } from './../../notifications/state/notifications.query';
import { Router } from '@angular/router';
import { UserService } from './../state/user.service';
import { User } from './../state/user.model';
import { switchMap, map, takeUntil } from 'rxjs/operators';
import { SessionQuery } from './../../auth/state/session.query';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { UsersQuery } from '../state/user.query';
import { Observable, Subject } from 'rxjs';

@Component({
  selector: 'app-profile-details',
  templateUrl: './profile-details.component.html',
  styleUrls: ['./profile-details.component.scss'],
})
export class ProfileDetailsComponent implements OnInit, OnDestroy {
  @Input() owner = false;
  @Input() public user: User;

  public followedByLoggedUser: Observable<boolean>;
  public unreadNotificationsCount$: Observable<number>;

  private unsubscribe$: Subject<any>;
  constructor(
    private readonly userQuery: UsersQuery,
    private readonly sessionQuery: SessionQuery,
    private readonly userService: UserService,
    private readonly notificationsQuery: NotificationsQuery,
  ) {
    this.unsubscribe$ = new Subject<any>();
    this.unreadNotificationsCount$ = this.notificationsQuery.unreadNotificationsCount$;
  }

  ngOnInit() {
    if (!this.user) {
      this.userQuery
        .selectActive()
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe((activeUser: User) => {
          this.user = activeUser;
        });
      this.followedByLoggedUser = this.userQuery.selectActive().pipe(
        switchMap((activeUser: User) => {
          this.user = activeUser;
          return this.sessionQuery.currentUser$;
        }),
        map((loggedUser: User) =>
          loggedUser.following
            .map((followedUser: User) => followedUser.username)
            .includes(this.user.username),
        ),
      );
    }
    this.followedByLoggedUser = this.sessionQuery.currentUser$.pipe(
      map((loggedUser: User) =>
        loggedUser.following
          .map((followedUser: User) => followedUser.username)
          .includes(this.user.username),
      ),
    );
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  follow() {
    this.userService.followUser(String(this.user.id));
  }

  unfollow() {
    this.userService.unfollowUser(String(this.user.id));
  }
}
