import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
} from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable, merge } from 'rxjs';
import { filter, take, switchMap } from 'rxjs/operators';
import { UserService } from '../state/user.service';
import { SessionQuery } from '../../auth/state/session.query';
import { ShowUser } from '../../shared/models/users';
import { UsersQuery } from '../state/user.query';
import { User } from '../state/user.model';

@Injectable({ providedIn: 'root' })
export class UserProfileResolver implements Resolve<User> {
  constructor(
    private readonly sessionQuery: SessionQuery,
    private readonly usersQuery: UsersQuery,
    private readonly userService: UserService,
    private readonly router: Router,
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<any> | Promise<any> | any {
    const username = route.params.username;
    return merge(
      this.userService.getActiveUser(username).pipe(
        switchMap(() => this.usersQuery.selectActive()),
        filter(activeUser => !!activeUser),
      ),
      this.usersQuery
        .selectError()
        .pipe(filter(err => !!err && err.condition === 404)),
    ).pipe(take(1));
  }
}
