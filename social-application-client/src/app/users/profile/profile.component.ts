import { UsersQuery } from './../state/user.query';
import { SessionQuery } from './../../auth/state/session.query';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { User } from '../state/user.model';
import { first, switchMap, map, takeUntil } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit, OnDestroy {
  public user: User;
  public user$: Observable<User>;
  public isProfileOwner$: Observable<boolean>;

  private unsubscribe$: Subject<any>;
  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly sessionQuery: SessionQuery,
    private readonly usersQuery: UsersQuery,
  ) {
    this.unsubscribe$ = new Subject<any>();
    this.isProfileOwner$ = route.data.pipe(
      map(({ user }) => {
        if (user.username) {
          this.user = user as User;
        } else {
          router.navigate(['error/404']);
        }
        return user;
      }),
      switchMap(() => this.sessionQuery.currentUser$),
      map((loggedUser: User) => {
        return loggedUser.username === this.user.username;
      }),
    );

    this.user$ = this.usersQuery.selectActive();

    this.usersQuery.selectActive().pipe(takeUntil(this.unsubscribe$)).subscribe((activeUser: User) => {
      this.user = activeUser;
    });
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
