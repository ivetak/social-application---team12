import { NotificationResolver } from './../notifications/notification-resolver.service';
import { ProfileNotificationsComponent } from './profile-notifications/profile-notifications.component';
import { ProfileOwnerGuard } from './../common/guards/profile-owner.guard';
import { AuthenticatedGuard } from './../common/guards/authenticated.guard';
import { ProfileCreatePostComponent } from './profile-create-post/profile-create-post.component';
import { ProfilePostsResolver } from './profile-posts/profile-posts-resolver.service';
import { ProfilePostsComponent } from './profile-posts/profile-posts.component';
import { ProfileComponent } from './profile/profile.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserProfileResolver } from './profile/user-profile-resolver.service';
import { ProfileFollowersComponent } from './profile-followers/profile-followers.component';
import { ProfileFollowingComponent } from './profile-following/profile-following.component';
import { ProfileSettingsComponent } from './profile-settings/profile-settings.component';
import { ProfileFollowersResolver } from './profile-followers/profile-followers-resolver.service';
import { ProfileFollowingResolver } from './profile-following/profile-following-resolver.service';

const routes: Routes = [
  {
    path: ':username',
    component: ProfileComponent,
    resolve: { user: UserProfileResolver },
    canActivate: [AuthenticatedGuard],
    pathMatch: 'prefix',
    children: [
      {
        path: '',
        redirectTo: 'posts',
        pathMatch: 'full',
      },
      {
        path: 'posts',
        component: ProfilePostsComponent,
        resolve: { posts: ProfilePostsResolver },
      },
      {
        path: 'create-post',
        component: ProfileCreatePostComponent,
        canActivate: [ProfileOwnerGuard],
      },
      {
        path: 'followers',
        component: ProfileFollowersComponent,
        resolve: { followers: ProfileFollowersResolver },
      },
      {
        path: 'following',
        component: ProfileFollowingComponent,
        resolve: { following: ProfileFollowingResolver },
      },
      {
        path: 'notifications',
        component: ProfileNotificationsComponent,
        canActivate: [ProfileOwnerGuard],
        resolve: { notifications: NotificationResolver },
      },
      {
        path: 'settings',
        component: ProfileSettingsComponent,
        canActivate: [ProfileOwnerGuard],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    UserProfileResolver,
    ProfilePostsResolver,
    ProfileFollowersResolver,
    ProfileFollowingResolver,
    NotificationResolver,
  ],
})
export class UsersRoutingModule {}
