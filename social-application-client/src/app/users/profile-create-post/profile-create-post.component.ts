import { BehaviorSubject } from 'rxjs';
import { ToastrNotificationService } from '../../common/notification/toastr-notification.service';
import { Router, ActivatedRoute } from '@angular/router';
import { PostsService } from './../../explore/state/posts.service';
import { Component, OnInit } from '@angular/core';
import { CreatePost } from '../../shared/models/posts/create-post.model';

@Component({
  selector: 'app-profile-create-post',
  templateUrl: './profile-create-post.component.html',
  styleUrls: ['./profile-create-post.component.scss'],
})
export class ProfileCreatePostComponent implements OnInit {
  public postCreateLoading$: BehaviorSubject<boolean>;

  constructor(
    private readonly postsService: PostsService,
    private readonly router: Router,
    private route: ActivatedRoute,
    private readonly toastrNotificationService: ToastrNotificationService,
  ) {
    this.postCreateLoading$ = new BehaviorSubject<boolean>(false);
  }

  ngOnInit() {}

  createPost(post: CreatePost) {
    this.postCreateLoading$.next(true);
    this.postsService.createPost(post).subscribe(
      (created: boolean) => {
        if (created) {
          this.toastrNotificationService.createdPostNotification();
          this.router.navigate(['../posts'], { relativeTo: this.route });
        } else {
          this.toastrNotificationService.createPostError(
            'There was a problem creating post',
            'Try again',
            this.createPost,
            post,
          );
        }
      },
      () => {},
      () => {
        this.postCreateLoading$.next(false);
      },
    );
  }
}
