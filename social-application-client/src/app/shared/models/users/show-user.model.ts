import { UserRole } from '../enum/user-role.enum';

export class ShowUser {
    public id: string;

    public username: string;

    public roles: UserRole[];
}
