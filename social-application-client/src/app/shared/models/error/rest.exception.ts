export class RestException {
  error: string;
  message: string;
  condition: number;
}
