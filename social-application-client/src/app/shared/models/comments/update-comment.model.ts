export class UpdateComment {
    public id: string | number;
    public content: string;
}
