import { PostPrivacy } from './../../enum/post-privacy.enum';
export class CreatePost {
  description: string;
  public: boolean;
  image: Blob;
}
