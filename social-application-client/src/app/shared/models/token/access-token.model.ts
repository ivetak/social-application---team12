import { Deserializable } from '../';

export class AccessAPIToken implements Deserializable {
  accessToken: string;
  accessTokenExpiresAt: string;

  deserialize(input: any): this {
    Object.assign(this, input);
    return this;
  }
}
