import { Deserializable } from './../deserializable.model';

export class RefreshAPIToken implements Deserializable {
  refreshToken: string;
  refreshTokenExpiresAt: string;

  deserialize(input: any): this {
    Object.assign(this, input);
    return this;
  }
}
