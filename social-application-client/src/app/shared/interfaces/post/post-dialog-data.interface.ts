import { Post } from '../../../explore/state/post.model';

export interface PostDialogData {
    post: Post;
}
