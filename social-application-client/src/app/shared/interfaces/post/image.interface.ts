export interface Image {
  id: string;
  directLink: string;
  datetime: number;
  size: number;
}
