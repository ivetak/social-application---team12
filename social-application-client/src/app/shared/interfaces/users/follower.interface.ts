export interface Follower {
     id: string;
     username: string;
     avatar: string;
     isFollow: boolean;
}
