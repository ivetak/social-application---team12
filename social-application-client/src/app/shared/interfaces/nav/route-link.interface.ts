export interface RouteLink {
  label: string;
  link: string;
  icon: string;
  showIfLogged: boolean;
}
