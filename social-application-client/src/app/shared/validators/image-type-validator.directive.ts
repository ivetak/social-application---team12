import { AbstractControl, ValidatorFn } from '@angular/forms';

export function ImageTypeValidator(types: string[]): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    if (control.value) {
      return types.includes(control.value.type)
        ? null
        : {
            invalidType: {
              type: control.value.type,
              message: `Invalid image type. Allowed types[${types}]`,
            },
          };
    }
  };
}
