import { AbstractControl, ValidatorFn } from '@angular/forms';

export function ImageSizeValidator(size: number): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    if (control.value) {
      return control.value.size < size
        ? null
        : {
            invalidSize: {
              size: control.value.size,
              message: `Max size allowed is ${size / (1024 * 1024)}MB`,
            },
          };
    }
  };
}
