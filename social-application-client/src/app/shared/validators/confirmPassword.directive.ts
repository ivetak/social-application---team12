import { FormGroup, ValidationErrors } from '@angular/forms';

export const confirmPassword = (g: FormGroup): ValidationErrors | null => {
  return g.get('password').value === g.get('passwordConfirm').value
    ? null
    : {
        passwordNotConfirmed: true
      };
};
