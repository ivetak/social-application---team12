export const validImageTypes = ['image/jpeg', 'image/png'];
export const validImageSize = 2000000;
