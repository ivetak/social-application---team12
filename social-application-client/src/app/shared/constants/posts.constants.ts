export const explorePostsQueryStep = 5;
export const explorePostsQueryStepIncrement = 5;
export const explorePostsQueryDefaultLimit = 10;
export const explorePostsRequestSkip = 0;
export const explorePostsRequestSkipIncrement = 50;
export const explorePostsRequestSkipThreshold = 40;
export const explorePostsRequestDefaultLimit = 50;

export const profilePostsQueryStep = 5;
export const profilePostsQueryStepIncrement = 5;
export const profilePostsQueryDefaultLimit = 10;
export const profilePostsRequestSkip = 0;
export const profilePostsRequestSkipIncrement = 50;
export const profilePostsRequestSkipThreshold = 40;
export const profilePostsRequestDefaultLimit = 50;

export const dashboardPostsQueryStep = 5;
export const dashboardPostsQueryStepIncrement = 5;
export const dashboardPostsQueryDefaultLimit = 10;
export const dashboardPostsRequestSkip = 0;
export const dashboardPostsRequestSkipIncrement = 50;
export const dashboardPostsRequestSkipThreshold = 40;
export const dashboardPostsRequestDefaultLimit = 50;

export const profileNotificationsQueryStep = 5;
export const profileNotificationsQueryStepIncrement = 5;
export const profileNotificationsQueryDefaultLimit = 10;
export const profileNotificationsRequestSkip = 0;
export const profileNotificationsRequestSkipIncrement = 50;
export const profileNotificationsRequestSkipThreshold = 40;
export const profileNotificationsRequestDefaultLimit = 50;
