import { PostDialogComponent } from './../posts/post-dialog/post-dialog.component';
import { ProfileDetailsComponent } from './../users/profile-details/profile-details.component';
import { CreatePostComponent } from '../posts/create-post/create-post.component';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatCardModule,
  MatIconModule,
  MatListModule,
  MatFormFieldModule,
  MatButtonModule,
  MatInputModule,
  MatMenuModule,
  MatToolbarModule,
  MatGridListModule,
  MatTooltipModule,
  MatSnackBarModule,
  MatButtonToggleModule,
  MAT_SNACK_BAR_DEFAULT_OPTIONS,
  MatAutocompleteModule,
  MatBadgeModule,
} from '@angular/material';
import { MatTabsModule } from '@angular/material/tabs';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialogModule } from '@angular/material';
import { DragDropDirective } from './directives/attribute';
import { MatRippleModule } from '@angular/material/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { FlexLayoutModule } from '@angular/flex-layout';
import { PostsListComponent } from '../posts/posts-list/posts-list.component';
import { PostComponent } from '../posts/post/post.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { CommentComponent } from '../posts/comments/comment/comment.component';
import { CreateCommentComponent } from '../posts/comments/create-comment/create-comment.component';
import { LikePostComponent } from '../posts/like-post/like-post.component';
import { MatRadioModule } from '@angular/material/radio';
import { AngularCropperjsModule } from 'angular-cropperjs';
import { DateAgoPipe } from '../common/pipes/date-ago.pipe';
import { LayoutModule } from '@angular/cdk/layout';
import { NotificationsComponent } from '../notifications/notifications.component';

@NgModule({
  declarations: [
    DragDropDirective,
    DateAgoPipe,
    PostsListComponent,
    PostComponent,
    ProfileDetailsComponent,
    CommentComponent,
    CreateCommentComponent,
    LikePostComponent,
    CreatePostComponent,
    PostDialogComponent,
    NotificationsComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,

    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    InfiniteScrollModule,
    AngularCropperjsModule,
    LayoutModule,

    MatCardModule,
    MatIconModule,
    MatListModule,
    MatFormFieldModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatInputModule,
    MatMenuModule,
    MatToolbarModule,
    MatGridListModule,
    MatTabsModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatChipsModule,
    MatDialogModule,
    MatRippleModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    MatSlideToggleModule,
    MatSidenavModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatRadioModule,
  ],
  exports: [
    CommonModule,
    RouterModule,

    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    InfiniteScrollModule,
    AngularCropperjsModule,
    LayoutModule,

    MatCardModule,
    MatIconModule,
    MatListModule,
    MatFormFieldModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatInputModule,
    MatMenuModule,
    MatToolbarModule,
    MatGridListModule,
    MatTabsModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatChipsModule,
    MatDialogModule,
    MatRippleModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    MatSlideToggleModule,
    MatSidenavModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatRadioModule,

    DragDropDirective,
    DateAgoPipe,
    PostsListComponent,
    PostComponent,
    ProfileDetailsComponent,
    CommentComponent,
    CreateCommentComponent,
    LikePostComponent,
    CreatePostComponent,
    PostDialogComponent,
    NotificationsComponent,
  ],
  providers: [
    {
      provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
      useValue: {
        duration: 5000,
        verticalPosition: 'bottom',
        horizontalPosition: 'left',
      },
    },
  ],
  entryComponents: [PostComponent, PostsListComponent, PostDialogComponent],
})
export class SharedModule {}
