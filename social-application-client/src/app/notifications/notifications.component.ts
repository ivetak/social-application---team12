import { NotificationsService } from './state/notifications.service';
import { User } from './../users/state/user.model';
import { SessionQuery } from './../auth/state/session.query';
import { PostsQuery } from './../explore/state/posts.query';
import { PostsService } from './../explore/state/posts.service';
import { MatDialog } from '@angular/material';
import { first, filter, takeUntil } from 'rxjs/operators';
import { Notification } from './state/notification.model';
import { Observable, Subject } from 'rxjs';
import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy,
} from '@angular/core';
import { Post } from '../explore/state/post.model';
import { PostDialogComponent } from '../posts/post-dialog/post-dialog.component';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss'],
})
export class NotificationsComponent implements OnInit, OnDestroy {
  @Input() notifications: Notification[];
  @Input() portable: boolean;
  @Input() loading: boolean;
  @Input() notificationsRequestEnd: boolean;
  @Input() notificationsQueryEnd: boolean;

  public notificationsEnd: boolean;

  @Output() notificationsOnScroll = new EventEmitter<null>();

  public infiniteScrollDistance = 1;
  public infiniteScrollThrottle = 1000;

  public loggedUser$: Observable<User>;
  private closeDialog$: Subject<any>;
  constructor(
    private readonly dialog: MatDialog,
    private readonly postsService: PostsService,
    private readonly postsQuery: PostsQuery,
    private readonly sessionQuery: SessionQuery,
    private readonly notificationsService: NotificationsService,
  ) {
    this.closeDialog$ = new Subject<any>();
    this.loggedUser$ = this.sessionQuery.currentUser$;
  }

  ngOnInit() {
    this.notificationsEnd =
      this.notificationsQueryEnd && this.notificationsRequestEnd;
  }

  ngOnDestroy() {
    this.closeDialog$.next();
    this.closeDialog$.complete();
  }

  public onScroll() {
    if (
      !(this.notificationsRequestEnd && this.notificationsQueryEnd) &&
      !this.portable
    ) {
      this.notificationsOnScroll.emit();
    }
  }

  readNotification(notification: Notification) {
    if (notification.status === 'unread') {
      this.notificationsService.readNotification(String(notification.id));
    }
  }

  openDialog(postId: string): void {
    this.postsService.getPost(postId);
    this.postsQuery
      .selectActive()
      .pipe(
        filter(post => !!post && post.id === postId),
        takeUntil(this.closeDialog$),
      )
      .subscribe((activePost: Post) => {
        const dialogRef = this.dialog.open(PostDialogComponent, {
          width: '70vw',
          height: '90vh',
          data: { post: { ...activePost } },
          hasBackdrop: true,
        });

        dialogRef
          .backdropClick()
          .pipe(first())
          .subscribe(() => {
            dialogRef.close();
          });
        this.closeDialog$.next();
      });
  }
}
