import { delay } from 'rxjs/operators';
import { SocketService } from './../../core/sockets/websocket.service';
import { API_CONFIG } from './../../config';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ID } from '@datorama/akita';
import { HttpClient } from '@angular/common/http';
import { NotificationsStore } from './notifications.store';
import { Notification } from './notification.model';

@Injectable({ providedIn: 'root' })
export class NotificationsService {
  private readonly readNotification$: Observable<Notification>;
  constructor(
    private notificationsStore: NotificationsStore,
    private readonly http: HttpClient,
    private readonly socketService: SocketService,
  ) {
    this.readNotification$ = this.socketService.readNotificationSocket$;

    this.readNotification$.subscribe((readNotification: Notification) => {
      console.log(readNotification);

      this.notificationsStore.upsert(readNotification.id, readNotification);
    });
  }

  readNotification(id: string) {
    this.socketService.readNotification(id);
  }

  getNotifications(skip: number = 0, take: number = 50) {
    this.http
      .get<Notification[]>(
        `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_NOTIFICATIONS_PATH}?skip=${skip}&take=${take}`,
      )
      .pipe(delay(100))
      .subscribe(
        (notifications: Notification[]) => {
          if (notifications.length < take) {
            console.log('end');

            this.notificationsStore.setNotificationEnd();
          }
          this.notificationsStore.upsertMany(notifications);
        },
        error => {
          this.notificationsStore.setError(error);
        },
        () => {
          this.notificationsStore.setLoading(false);
        },
      );
  }

  get() {
    this.http
      .get<Notification[]>(
        `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_NOTIFICATIONS_PATH}/likes`,
      )
      .subscribe(likeNotifications => {
        this.notificationsStore.upsertMany(likeNotifications);
      });
  }

  add(notification: Notification) {
    this.notificationsStore.add(notification);
  }

  update(id, notification: Partial<Notification>) {
    this.notificationsStore.update(id, notification);
  }

  remove(id: ID) {
    this.notificationsStore.remove(id);
  }
}
