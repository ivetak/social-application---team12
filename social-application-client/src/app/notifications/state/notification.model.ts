import { Post } from './../../explore/state/post.model';
import { User } from './../../users/state/user.model';
import { ID } from '@datorama/akita';
import { NotificationType, NotificationStatus } from '../../shared/enum';

export interface Notification {
  id: ID;
  notificationAuthor: User;
  post: Post;
  type: NotificationType;
  status: NotificationStatus;
  createdOn: string;
}

/**
 * A factory function that creates Notifications
 */
export function createNotification(params: Partial<Notification>) {
  return {} as Notification;
}
