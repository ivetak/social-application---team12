import { tap } from 'rxjs/operators';
import { NotificationStatus } from './../../shared/enum/notification-status.enum';
import { Injectable } from '@angular/core';
import { QueryEntity, Order, QueryConfig } from '@datorama/akita';
import { NotificationsStore, NotificationsState } from './notifications.store';
import { Notification } from './notification.model';

@QueryConfig({
  sortBy: 'status',
  sortByOrder: Order.DESC,
})
@Injectable({
  providedIn: 'root',
})
export class NotificationsQuery extends QueryEntity<
  NotificationsState,
  Notification
> {
  public notifications$ = this.selectAll();
  public notificationsCount$ = this.selectCount();
  public unreadNotificationsCount$ = this.selectCount(
    (notification: Notification) =>
      notification.status === NotificationStatus.UNREAD,
  );
  public notificationsEnd$ = this.select(state => state.ui.notificationsEnd);

  constructor(protected store: NotificationsStore) {
    super(store);
  }
}
