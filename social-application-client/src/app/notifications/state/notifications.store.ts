import { avatarUrlFilter } from './../../common/store/avatar-url-filter.constant';
import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { Notification } from './notification.model';

export interface NotificationsState extends EntityState<Notification> {
  ui: {
    notificationsEnd: boolean;
  };
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'notifications' })
export class NotificationsStore extends EntityStore<
  NotificationsState,
  Notification
> {
  constructor() {
    super();
    this.update({ ui: { notificationsEnd: false } });
  }

  public setNotificationEnd() {
    this.update({ ui: { notificationsEnd: true } });
  }

  akitaPreAddEntity(notification: Notification) {
    return {
      ...notification,
      notificationAuthor: {
        ...notification.notificationAuthor,
        avatar: avatarUrlFilter(notification.notificationAuthor.avatar),
      },
    };
  }

  akitaPreUpdateEntity(_: Notification, newNotification: Notification) {
    return {
      ...newNotification,
      notificationAuthor: {
        ...newNotification.notificationAuthor,
        avatar: avatarUrlFilter(newNotification.notificationAuthor.avatar),
      },
    };
  }
}
