import { NotificationsService } from './state/notifications.service';
import { Notification } from './state/notification.model';
import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';
import { Observable } from 'rxjs';
import { map, first } from 'rxjs/operators';
import { NotificationsQuery } from './state/notifications.query';

@Injectable({ providedIn: 'root' })
export class NotificationResolver implements Resolve<Notification[]> {
  constructor(
    private readonly notificationsService: NotificationsService,
    private readonly notificationsQuery: NotificationsQuery,
  ) {}
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<Notification[]> | Promise<any> | any {
    this.notificationsService.get();
    return this.notificationsQuery.notifications$.pipe(
      map(notifications => notifications.slice(0, 5)),
      first(),
    );
  }
}
