export * from './theme.config';

const API_DOMAIN_NAME = 'http://localhost:3000';
const API_POSTS_PATH = '/posts';
const API_USERS_PATH = '/users';
const API_LOGGED_USER_PATH = '/user';
const DEFAULT_USER_AVATAR_IMAGE_PATH = '/assets/default-user.png';
const API_NOTIFICATIONS_PATH = '/notifications';

export const CONFIG = {
  DEFAULT_USER_AVATAR_IMAGE_PATH,
};

export const API_CONFIG = {
  API_DOMAIN_NAME,
  API_POSTS_PATH,
  API_USERS_PATH,
  API_LOGGED_USER_PATH,
  API_NOTIFICATIONS_PATH,
};
