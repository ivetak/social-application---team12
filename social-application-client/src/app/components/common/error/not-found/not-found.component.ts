import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.css'],
})
export class NotFoundComponent implements OnInit {
  public resource;

  private readonly unsubscribe$: Subject<void>;
  constructor(private readonly route: ActivatedRoute) {
    this.unsubscribe$ = new Subject<void>();
    this.route.queryParams
      .subscribe(params => {
        if (params.resource) {
          this.resource = params.resource;
        }
      });
  }

  ngOnInit() {}
}
