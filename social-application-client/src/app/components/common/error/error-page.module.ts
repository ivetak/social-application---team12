import { SharedModule } from './../../../shared/shared.module';
import { NotFoundComponent } from './not-found/not-found.component';
import { NgModule } from '@angular/core';
import { InternalServerErrorComponent } from './internal-server-error/internal-server-error.component';

@NgModule({
  declarations: [NotFoundComponent, InternalServerErrorComponent],
  exports: [NotFoundComponent, InternalServerErrorComponent],
  imports: [SharedModule],
})
export class ErrorPageModule {}
