import { NotificationsService } from './../../notifications/state/notifications.service';
import { NotificationsQuery } from './../../notifications/state/notifications.query';
import { RouteLink } from './../../shared/interfaces/nav';
import { User } from './../../users/state/user.model';
import { ThemeQuery } from './../../core/state/theme.query';
import { ThemeService } from './../../core/state/theme.service';
import { takeUntil, map } from 'rxjs/operators';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { SessionQuery } from '../../auth/state/session.query';
import { SessionService } from 'src/app/auth/state/session.service';
import { Notification } from '../../notifications/state/notification.model';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit, OnDestroy {
  public currentUser: User;
  public logged: boolean;

  public routeLinks: RouteLink[];

  public notificationsCount$: Observable<number>;
  public unreadNotificationsCount$: Observable<number>;
  public notifications$: Observable<Notification[]>;

  public currentSearchValue: string;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private readonly sessionQuery: SessionQuery,
    private readonly sessionService: SessionService,
    private readonly themeService: ThemeService,
    private readonly themeQuery: ThemeQuery,
    private readonly notificationsQuery: NotificationsQuery,
    private readonly notificationsService: NotificationsService,
  ) {
    this.notifications$ = this.notificationsQuery.notifications$.pipe(
      map((notifications: Notification[]) => notifications.slice(0, 5)),
    );
    this.unreadNotificationsCount$ = this.notificationsQuery.unreadNotificationsCount$;
    this.sessionQuery.isLoggedIn$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(logged => {
        this.logged = logged;
      });
    this.sessionQuery.currentUser$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((user: User) => {
        this.currentUser = user;
        if (user.id !== '0') {
          this.notificationsService.get();
        }
      });
    this.routeLinks = [
      {
        label: 'Explore',
        link: 'explore',
        icon: 'explore',
        showIfLogged: true,
      },
      {
        label: 'Dashboard',
        link: 'dashboard',
        icon: 'dashboard',
        showIfLogged: true,
      },
      {
        label: 'Login',
        link: 'auth',
        icon: 'vpn_key',
        showIfLogged: false,
      },
    ];
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public logout() {
    this.sessionService.logout();
  }

  public switchLight() {
    this.themeService.switchLight();
  }
  public filterList(searchParam: string) {
    this.currentSearchValue = searchParam;
  }
}
