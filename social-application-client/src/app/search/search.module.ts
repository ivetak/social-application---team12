import { SharedModule } from './../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchRoutingModule } from './search-routing.module';
import { SearchComponent } from './search.component';
import { SearchbarComponent } from './searchbar/searchbar.component';


@NgModule({
  declarations: [SearchComponent],
  imports: [
    CommonModule,
    SearchRoutingModule,
    // CoreModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  exports: [SearchComponent]
})
export class SearchModule { }
