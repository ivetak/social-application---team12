import { UserService } from './../users/state/user.service';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { User } from '../users/state/user.model';
import { UsersQuery } from '../users/state/user.query';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
  // public search = new FormControl();

  public filteredUsers: Observable<User[]>;
  public resolvedFilteredUsers: User[];
  public loading$: Observable<boolean>;

  public searching: Observable<boolean>;

  constructor(
    private readonly usersService: UserService,
    private readonly usersQuery: UsersQuery,
    private readonly router: Router,
  ) {}

  ngOnInit() {
    this.searching = this.usersQuery.searchTerm$;

    this.filteredUsers = this.usersQuery.searchUsers$;

    this.loading$ = this.usersQuery.selectLoading();
  }

  goToDetail(user: User): void {
    this.router.navigate([`/users`, user.username]);
  }
}
