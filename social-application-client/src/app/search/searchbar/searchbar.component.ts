import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { User } from '../../users/state/user.model';
import { UserService } from '../../users/state/user.service';
import { UsersQuery } from '../../users/state/user.query';
import { Router } from '@angular/router';
import { debounceTime, distinct } from 'rxjs/operators';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.scss'],
})
export class SearchbarComponent implements OnInit {
  public search = new FormControl();

  public filteredUsers$: Observable<User[]>;

  public loading$: Observable<boolean>;

  constructor(
    private readonly usersService: UserService,
    private readonly usersQuery: UsersQuery,
    private readonly router: Router,
  ) {}

  ngOnInit() {
    this.loading$ = this.usersQuery.selectLoading();

    this.filteredUsers$ = this.usersQuery.searchUsers$;

    this.search.valueChanges
      .pipe(debounceTime(300), distinct())
      .subscribe(term => {
        if (term && term.length) {
          this.usersService.get(term);
          this.usersService.updateSearchTerm(term);
        } else {
          this.usersService.updateSearchTerm(null);
        }
      });
  }

  goToSearch() {
    this.router.navigate(['search']);
  }
}
