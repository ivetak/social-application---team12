import { Notification } from './../../notifications/state/notification.model';
import { NotificationsService } from './../../notifications/state/notifications.service';
import { SessionQuery } from '../../auth/state/session.query';
import { Post } from '../../explore/state/post.model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Socket } from 'ngx-socket-io';

@Injectable({ providedIn: 'root' })
export class SocketService {
  public likesSocket$: Observable<Post>;
  public readNotificationSocket$: Observable<Notification>;

  constructor(
    private readonly ws: Socket,
    private readonly sessionQuery: SessionQuery,
  ) {
    this.likesSocket$ = this.ws.fromEvent('likes');
    this.readNotificationSocket$ = this.ws.fromEvent('readNotification');
  }

  public readNotification(id: string) {
    this.ws.emit('readNotification', id);
  }
}
