import { APP_THEMES } from './../../config/theme.config';
import { Injectable } from '@angular/core';
import { Query } from '@datorama/akita';
import { ThemeStore, ThemeState } from './theme.store';

@Injectable({ providedIn: 'root' })
export class ThemeQuery extends Query<ThemeState> {
  isDark$ = this.select(state => state.dark);
  currentTheme$ = this.select(state => state.themeName);
  availableThemes = Object.keys(APP_THEMES);

  constructor(protected store: ThemeStore) {
    super(store);
  }
}
