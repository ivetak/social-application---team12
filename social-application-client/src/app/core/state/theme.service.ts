import { SessionQuery } from 'src/app/auth/state/session.query';
import { SessionService } from '../../auth/state/session.service';
import { Injectable } from '@angular/core';
import { ThemeStore, ThemeState } from './theme.store';
import { APP_THEMES } from '../../config';

@Injectable({ providedIn: 'root' })
export class ThemeService {
  constructor(
    private themeStore: ThemeStore,
    private readonly sessionService: SessionService,
    private readonly sessionQuery: SessionQuery,
  ) {
    this.sessionQuery.currentUserTheme$.subscribe(
      (currentUserTheme: ThemeState) => {
        this.themeStore.update(currentUserTheme);
      },
    );
  }

  public changeTheme(themeName: APP_THEMES) {
    this.themeStore.update({ themeName });
    this.sessionService.changeTheme(themeName);
  }
  public switchLight() {
    this.themeStore.update({ dark: !this.themeStore.getValue().dark });
    this.sessionService.switchLight(this.themeStore.getValue().dark);
  }
}
