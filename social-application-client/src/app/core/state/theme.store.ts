import { SessionQuery } from 'src/app/auth/state/session.query';
import { APP_THEME, APP_THEMES } from './../../config';
import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';

export interface ThemeState {
  themeName: APP_THEMES;
  dark: boolean;
}

export const createInitialState = (session?: ThemeState): ThemeState =>
  session
    ? session
    : {
        themeName: APP_THEME.ORANGE_THEME,
        dark: false,
      };

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'theme' })
export class ThemeStore extends Store<ThemeState> {
  constructor(private readonly sessionQuery: SessionQuery) {
    super(createInitialState(sessionQuery.getValue().theme));
  }
}
