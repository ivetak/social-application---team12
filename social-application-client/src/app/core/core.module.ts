import { NotificationsQuery } from './../notifications/state/notifications.query';
import { NotificationsService } from './../notifications/state/notifications.service';
import { PostsQuery } from './../explore/state/posts.query';
import { PostsService } from './../explore/state/posts.service';
import { SocketService } from './sockets/websocket.service';
import { ToastrNotificationService } from '../common/notification/toastr-notification.service';
import { SearchModule } from './../search/search.module';
import { MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { ThemeService } from './state/theme.service';
import { SharedModule } from './../shared/shared.module';
import { ErrorPageModule } from './../components/common/error';
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { SessionService } from '../auth/state/session.service';
import { SessionQuery } from '../auth/state/session.query';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavbarComponent } from '../components/navbar/navbar.component';
import { httpInterceptorProviders } from '../http-interceptors';
import { ThemeQuery } from './state/theme.query';
import { SearchbarComponent } from '../search/searchbar/searchbar.component';
import { UsersQuery } from '../users/state/user.query';
import { UserService } from '../users/state/user.service';
import { SocketIoModule } from 'ngx-socket-io';
import { API_CONFIG } from '../config';

@NgModule({
  declarations: [NavbarComponent, SearchbarComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ErrorPageModule,
    SharedModule,
    SocketIoModule.forRoot({ url: API_CONFIG.API_DOMAIN_NAME, options: {} }),
  ],
  providers: [
    {
      provide: MAT_DIALOG_DEFAULT_OPTIONS,
      useValue: { hasBackdrop: false, closeOnNavigation: true, maxHeight: '' },
    },
    ThemeService,
    ThemeQuery,
    SessionService,
    SessionQuery,
    UserService,
    UsersQuery,
    ToastrNotificationService,
    SocketService,
    PostsService,
    PostsQuery,
    NotificationsService,
    NotificationsQuery,

    httpInterceptorProviders,
  ],
  exports: [NavbarComponent, SearchbarComponent],
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only',
      );
    }
  }
}
