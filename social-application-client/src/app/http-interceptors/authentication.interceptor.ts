import { Router } from '@angular/router';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpHeaders,
} from '@angular/common/http';
import { Observable, of, merge } from 'rxjs';
import { SessionQuery } from '../auth/state/session.query';
import { Injectable } from '@angular/core';
import { switchMap, filter, first } from 'rxjs/operators';
import { SessionService } from '../auth/state/session.service';

@Injectable()
export class AuthenticationInterceptor implements HttpInterceptor {
  constructor(
    private readonly session: SessionQuery,
    private readonly router: Router,
  ) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    return this.session.token$.pipe(
      first(),
      switchMap((token: string) => {
        if (token) {
          request = request.clone({
            headers: request.headers.set('Authorization', `Bearer ${token}`),
          });
        }
        return next.handle(request);
      }),
    );
  }
}
