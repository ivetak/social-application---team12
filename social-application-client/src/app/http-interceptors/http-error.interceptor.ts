import { ToastrNotificationService } from '../common/notification/toastr-notification.service';
import { Router } from '@angular/router';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  constructor(
    private readonly router: Router,
    private readonly toastrNotificationService: ToastrNotificationService,
  ) {}
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      retry(1),
      catchError((error: HttpErrorResponse) => {
        if (error.error instanceof ErrorEvent) {
          // client-side error
          return throwError(new Error('Client-side error'));
        } else if (error.status === 404) {
          const resource =
            error.error.message === 'User is not found' ? 'user' : '';
          this.router.navigate(['error', '404'], {
            queryParams: { resource },
          });
        } else if (error.status === 400) {
          // bad-request
          return throwError(error.error);
        } else if (error.status === 401) {
          this.toastrNotificationService.notAuthenticatedNotification();
        }
      }),
    );
  }
}
