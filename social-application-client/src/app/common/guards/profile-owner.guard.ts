import { ToastrNotificationService } from '../notification/toastr-notification.service';
import { User } from 'src/app/users/state/user.model';
import { map } from 'rxjs/operators';
import { SessionQuery } from './../../auth/state/session.query';
import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root',
})
export class ProfileOwnerGuard implements CanActivate {
  constructor(
    private readonly sessionQuery: SessionQuery,
    private readonly toastrNotificationService: ToastrNotificationService,
    private readonly router: Router,
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean | UrlTree> {
    const profileOwner = next.parent.params.username;
    return this.sessionQuery.currentUser$.pipe(
      map((loggedUser: User) => {
        const isProfileOwner = profileOwner === loggedUser.username;
        if (!isProfileOwner) {
          this.toastrNotificationService.notProfileOwnerNotification(
            next.url[next.url.length - 1].toString(),
          );
          return this.router.parseUrl(`/users/${profileOwner}`);
        }
        return profileOwner === loggedUser.username;
      }),
    );
  }
}
