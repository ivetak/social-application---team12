import { TestBed, async, inject } from '@angular/core/testing';

import { ProfileOwnerGuard } from './profile-owner.guard';

describe('ProfileOwnerGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProfileOwnerGuard]
    });
  });

  it('should ...', inject([ProfileOwnerGuard], (guard: ProfileOwnerGuard) => {
    expect(guard).toBeTruthy();
  }));
});
