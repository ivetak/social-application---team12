import { ToastrNotificationService } from '../notification/toastr-notification.service';
import { map } from 'rxjs/operators';
import { SessionQuery } from './../../auth/state/session.query';
import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root',
})
export class AuthenticatedGuard implements CanActivate {
  constructor(
    private readonly sessionQuery: SessionQuery,
    private readonly router: Router,
    private readonly toastrNotificationService: ToastrNotificationService,
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean | UrlTree> {
    return this.sessionQuery.isLoggedIn$.pipe(
      map((logged: boolean) => {
        if (!logged) {
          const snackBarRef = this.toastrNotificationService.notAuthenticatedNotification();
          // return this.router.parseUrl('/auth');
        }
        return logged;
      }),
    );
  }
}
