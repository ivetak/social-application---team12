export const avatarUrlFilter = (avatar: string) => {
  if (avatar === 'default_avatar') {
    return 'assets/avatars/default.jpg';
  }
  return avatar;
};
