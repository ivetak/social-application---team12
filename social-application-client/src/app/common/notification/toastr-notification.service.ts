import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarRef, SimpleSnackBar } from '@angular/material';
import { first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ToastrNotificationService {
  constructor(
    private readonly snackBar: MatSnackBar,
    private readonly router: Router,
  ) {}

  public createPostError(
    message: string = 'There was a problem creating post',
    action: string = '',
    retryFn,
    postToRetry,
    dismissWithAction: boolean = false,
  ): MatSnackBarRef<SimpleSnackBar> {
    let snackBarRef: MatSnackBarRef<SimpleSnackBar>;
    if (action) {
      snackBarRef = this.snackBar.open(message, action);
    } else {
      snackBarRef = this.snackBar.open(message);
    }
    snackBarRef
      .onAction()
      .pipe(first())
      .subscribe(() => {
        retryFn(postToRetry);
      });
    if (dismissWithAction) {
      snackBarRef.dismissWithAction();
    }
    return snackBarRef;
  }

  public createdPostNotification(
    message: string = 'Post successfully created',
    action: string = '',
    onAction: () => void = () => {},
    dismissWithAction: boolean = false,
  ): MatSnackBarRef<SimpleSnackBar> {
    let snackBarRef: MatSnackBarRef<SimpleSnackBar>;
    if (action) {
      snackBarRef = this.snackBar.open(message, action);
    } else {
      snackBarRef = this.snackBar.open(message);
    }
    snackBarRef
      .onAction()
      .pipe(first())
      .subscribe(onAction);
    if (dismissWithAction) {
      snackBarRef.dismissWithAction();
    }
    return snackBarRef;
  }

  public avatarChangedNotification(
    message: string = 'Avatar successfully changed',
    action: string = '',
    onAction: () => void = () => {},
    dismissWithAction: boolean = false,
  ): MatSnackBarRef<SimpleSnackBar> {
    let snackBarRef: MatSnackBarRef<SimpleSnackBar>;
    if (action) {
      snackBarRef = this.snackBar.open(message, action);
    } else {
      snackBarRef = this.snackBar.open(message);
    }
    snackBarRef
      .onAction()
      .pipe(first())
      .subscribe(onAction);
    if (dismissWithAction) {
      snackBarRef.dismissWithAction();
    }
    return snackBarRef;
  }

  public imageUploadError(
    message: string = 'Image upload failed',
    action: string = '',
    onAction: () => void = () => {},
    dismissWithAction: boolean = false,
  ): MatSnackBarRef<SimpleSnackBar> {
    let snackBarRef: MatSnackBarRef<SimpleSnackBar>;
    if (action) {
      snackBarRef = this.snackBar.open(message, action);
    } else {
      snackBarRef = this.snackBar.open(message);
    }
    snackBarRef
      .onAction()
      .pipe(first())
      .subscribe(onAction);
    if (dismissWithAction) {
      snackBarRef.dismissWithAction();
    }
    return snackBarRef;
  }

  public notAuthenticatedNotification(
    message: string = 'You need to login to do this',
    action: string = 'Login',
    onAction = () => {
      this.router.navigate(['/auth']);
    },
    dismissWithAction: boolean = false,
  ): MatSnackBarRef<SimpleSnackBar> {
    const snackBarRef = this.snackBar.open(message, action);
    snackBarRef
      .onAction()
      .pipe(first())
      .subscribe(onAction);
    if (dismissWithAction) {
      snackBarRef.dismissWithAction();
    }
    return snackBarRef;
  }

  public notProfileOwnerNotification(
    profilePage: string,
    message: string = '',
  ): MatSnackBarRef<SimpleSnackBar> {
    if (!message) {
      message = `Only profile owner can access this profile's ${profilePage} page`;
    }
    return this.snackBar.open(message);
  }
}
