import { CreatePostComponent } from './../posts/create-post/create-post.component';
import { User } from 'src/app/users/state/user.model';
import { SessionQuery } from './../auth/state/session.query';
import {
  dashboardPostsRequestSkipThreshold,
  dashboardPostsQueryStepIncrement,
  dashboardPostsRequestSkip,
  dashboardPostsQueryStep,
  dashboardPostsRequestDefaultLimit,
} from './../shared/constants/posts.constants';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Post } from '../explore/state/post.model';
import { PostsQuery } from '../explore/state/posts.query';
import { PostsService } from '../explore/state/posts.service';
import { takeUntil, map, switchMap } from 'rxjs/operators';
import {
  dashboardPostsQueryDefaultLimit,
  dashboardPostsRequestSkipIncrement,
} from '../shared/constants/posts.constants';
import { ID } from '@datorama/akita';
import { CreatePost } from '../shared/models/posts/create-post.model';
import { MatExpansionPanel } from '@angular/material';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit, OnDestroy {
  public posts$: Observable<Post[]>;
  public profileOwner: boolean;
  public skip = dashboardPostsRequestSkip; // request
  public step = dashboardPostsQueryStep; // query
  public requestLoading$: Observable<boolean>;
  public postRequestEnd$: Observable<boolean>;
  public postsQueryEnd$: Observable<boolean>;
  public loggedUser: Observable<User>;

  @ViewChild('createPostPanel', { static: false })
  createPostPanel: MatExpansionPanel;

  @ViewChild('createPostComponent', { static: false })
  createPostComponent: CreatePostComponent;

  private postsCount: number;
  private unsubscribe$: Subject<void>;

  constructor(
    private readonly postsQuery: PostsQuery,
    private readonly postsService: PostsService,
    private readonly sessionQuery: SessionQuery,
  ) {
    this.loggedUser = this.sessionQuery.currentUser$;
    this.profileOwner = true;
    this.unsubscribe$ = new Subject<void>();
  }

  ngOnInit() {
    this.requestPosts(this.skip);
    this.posts$ = this.queryPosts(this.step);

    this.postsQuery
      .selectCount(post => post.public)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(count => (this.postsCount = count));

    this.requestLoading$ = this.postsQuery.selectLoading();

    this.postsQueryEnd$ = this.postsQuery
      .selectCount((entity: Post) => entity.public)
      .pipe(map(postsCount => this.step > postsCount));

    this.postRequestEnd$ = this.postsQuery.postEnd$;
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public postsOnScroll() {
    this.step += dashboardPostsQueryStepIncrement;
    this.posts$ = this.queryPosts(this.step);
    if (
      !(this.step % dashboardPostsRequestSkipThreshold) ||
      this.step > this.postsCount
    ) {
      this.skip += dashboardPostsRequestSkipIncrement;
      this.requestPosts(this.skip);
    }
  }

  createPost(post: CreatePost) {
    this.postsService.createPost(post).subscribe((created: boolean) => {
      console.log('post created');
      this.posts$ = this.queryPosts(this.step);
      this.createPostPanel.close();
      this.createPostComponent.resetComponent();
    });
  }

  private queryPosts(
    step: number,
    limit: number = dashboardPostsQueryDefaultLimit,
  ): Observable<Post[]> {
    return this.loggedUser.pipe(
      switchMap((loggedUser: User) =>
        this.postsQuery.selectAll({
          filterBy: [
            (post: Post) => {
              const followedUsersUsernames: ID[] = loggedUser.following.map(
                (followedUser: User) => followedUser.id,
              );
              return [...followedUsersUsernames, loggedUser.id].includes(
                post.author.id,
              );
            },
          ],
          limitTo: step,
        }),
      ),
      map((posts: Post[]) => posts.slice(0, step)),
    );
  }

  private requestPosts(
    skip: number,
    limit: number = dashboardPostsRequestDefaultLimit,
  ) {
    this.postsService.getDashboardPosts(skip, limit);
  }
}
