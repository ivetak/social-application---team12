import { User } from 'src/app/users/state/user.model';
import { Observable, Subject } from 'rxjs';
import { CreateComment } from './../../shared/models/comments/create-comment.model';
import { PostsService } from './../../explore/state/posts.service';
import { Post, Like } from './../../explore/state/post.model';
import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy,
} from '@angular/core';
import { SessionQuery } from '../../auth/state/session.query';
import { map, takeUntil } from 'rxjs/operators';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss'],
})
export class PostComponent implements OnInit, OnDestroy {
  @Input() post: Post;
  @Output() openDialog: EventEmitter<Post>;

  public likedByLoggedUser: Observable<boolean>;
  public loggedUserIsAuthor: Observable<boolean>;
  public canOpenDialog: boolean;

  private unsubscribe$: Subject<any>;
  constructor(
    private readonly postsService: PostsService,
    private readonly sessionQuery: SessionQuery,
    private readonly bp$: BreakpointObserver,
  ) {
    this.unsubscribe$ = new Subject<any>();
    this.bp$
      .observe('(min-width: 1100px)')
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((bpState: BreakpointState) => {
        this.canOpenDialog = bpState.matches;
      });
    this.openDialog = new EventEmitter<Post>();
  }

  ngOnInit() {
    this.likedByLoggedUser = this.sessionQuery.currentUser$.pipe(
      map((loggedUser: User) =>
        this.post.likes
          ? !!this.post.likes.find(
              (like: Like) => like.votedBy.username === loggedUser.username,
            )
          : false,
      ),
    );

    this.loggedUserIsAuthor = this.sessionQuery.currentUser$.pipe(
      map(
        (loggedUser: User) => this.post.author.username === loggedUser.username,
      ),
    );
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public addComment(comment: CreateComment) {
    this.postsService.createComment(comment);
  }

  public likePost() {
    this.postsService.likePost(String(this.post.id));
  }

  public dislikePost() {
    this.postsService.dislikePost(String(this.post.id));
  }

  public removePost() {
    this.postsService.removePost(String(this.post.id));
  }

  public openPostDialog() {
    if (this.canOpenDialog) {
      this.openDialog.emit(this.post);
    }
  }
}
