import { ToastrNotificationService } from '../../common/notification/toastr-notification.service';
import { CreatePost } from './../../shared/models/posts/create-post.model';
import { User } from './../../users/state/user.model';
import { SessionQuery } from './../../auth/state/session.query';
import {
  validImageTypes,
  validImageSize,
} from './../../shared/constants/validation.constants';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  Component,
  OnInit,
  ViewChild,
  Output,
  EventEmitter,
  ElementRef,
  Input,
} from '@angular/core';
import {
  ImageTypeValidator,
  ImageSizeValidator,
} from '../../shared/validators';
import { Observable } from 'rxjs';
import { CropperComponent } from 'angular-cropperjs';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.scss'],
})
export class CreatePostComponent implements OnInit {
  @Input() loading: boolean;
  @Output() createdPost: EventEmitter<CreatePost>;

  public createPostForm: FormGroup;
  public imageCropperFile: File;
  public imageCropperUrl: string | ArrayBuffer;
  public loggedUser: Observable<User>;
  public cropperConfig;

  @ViewChild('angularCropper', { static: false })
  public angularCropper: CropperComponent;

  @ViewChild('fileInput', { static: false })
  public fileInput: ElementRef;

  constructor(
    private readonly fb: FormBuilder,
    private readonly sessionQuery: SessionQuery,
    private readonly toastrNotificationService: ToastrNotificationService,
  ) {
    this.createdPost = new EventEmitter<CreatePost>();
    this.createPostForm = this.fb.group({
      description: ['', Validators.required],
      privacy: [true, Validators.required],
      image: [
        null,
        Validators.compose([
          Validators.required,
          ImageTypeValidator(validImageTypes),
          ImageSizeValidator(validImageSize),
        ]),
      ],
    });

    this.loggedUser = this.sessionQuery.select('user');
  }

  ngOnInit() {
    this.cropperConfig = {
      center: true,
      guides: true,
      viewMode: 0,
      responsive: true,
      dragMode: 'move',
      aspectRatio: 1,
      initialAspectRatio: 1,
      scalable: false,
      minCanvasWidth: 200,
      minCanvasHeight: 200,
      minContainerHeight: 200,
      minContainerWidth: 200,
      minCropBoxWidth: 270,
      minCropBoxHeight: 270,
      movable: true,
      zoomable: true,
      modal: false,
      background: false,
      cropBoxMovable: false,
      cropBoxResizable: false,
      toggleDragModeOnDblclick: false,
      zoom: e => {},
      crop: e => {},
      cropstart: e => {},
      cropend: e => {},
      ready: e => {
        const container = this.angularCropper.cropper.getContainerData();
        this.angularCropper.cropper.setCropBoxData({
          height: container.height,
          width: container.width,
          left: 0,
          top: 0,
        });
      },
    };
  }

  createPost() {
    this.angularCropper.cropper.getCroppedCanvas().toBlob(blob => {
      const post: CreatePost = {
        description: this.createPostForm.controls.description.value,
        public: JSON.parse(this.createPostForm.controls.privacy.value),
        image: blob,
      };

      this.createdPost.emit(post);
      this.resetComponent();
    });
  }

  uploadedImage(image: File) {
    console.log(this.createPostForm.controls.image.errors);

    this.createPostForm.patchValue({ image });
    if (this.createPostForm.controls.image.valid) {
      this.imageCropperFile = null;
      const fr = new FileReader();
      fr.onloadend = e => {
        this.imageCropperFile = this.createPostForm.controls.image.value;
        this.imageCropperUrl = fr.result;
      };
      fr.readAsDataURL(image);
    } else {
      const invalidType = this.createPostForm.controls.image.getError(
        'invalidType',
      );
      const invalidSize = this.createPostForm.controls.image.getError(
        'invalidSize',
      );
      this.toastrNotificationService.imageUploadError(
        invalidType
          ? invalidType.message
          : invalidSize
          ? invalidSize.message
          : 'Upload failed',
        'Upload image',
        () => {
          (this.fileInput.nativeElement as HTMLInputElement).click();
        },
      );
    }
  }

  public resetComponent() {
    this.createPostForm.reset();
    this.createPostForm.controls.privacy.setValue(true);
    this.imageCropperFile = null;
    this.imageCropperUrl = null;
  }
}
