import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Post } from '../../explore/state/post.model';

@Component({
  selector: 'app-like-post',
  templateUrl: './like-post.component.html',
  styleUrls: ['./like-post.component.scss'],
})
export class LikePostComponent implements OnInit {
  @Input() public liked: boolean;

  @Input() public likesCount: number;

  @Output() likePost = new EventEmitter<null>();

  @Output() dislikePost = new EventEmitter<null>();

  constructor() {}

  ngOnInit() {}

  public like(): void {
    this.likePost.emit();
  }

  public dislike(): void {
    this.dislikePost.emit();
  }
}
