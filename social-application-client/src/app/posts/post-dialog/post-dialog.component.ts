import { PostsQuery } from './../../explore/state/posts.query';
import { Post, Like } from './../../explore/state/post.model';
import {
  Component,
  OnInit,
  Inject,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Observable } from 'rxjs';
import { map, tap, switchMap, take, filter } from 'rxjs/operators';
import { User } from '../../users/state/user.model';
import { CreateComment } from '../../shared/models/comments/create-comment.model';
import { PostsService } from '../../explore/state/posts.service';
import { SessionQuery } from '../../auth/state/session.query';
import { PostDialogData } from '../../shared/interfaces/post/post-dialog-data.interface';

@Component({
  selector: 'app-post-dialog',
  templateUrl: './post-dialog.component.html',
  styleUrls: ['./post-dialog.component.scss'],
})
export class PostDialogComponent implements OnInit {
  @ViewChild('createComment', { static: true })
  public createCommentChild: ElementRef;

  post: Post;

  public likedByLoggedUser: Observable<boolean>;

  constructor(
    public dialogRef: MatDialogRef<PostDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: PostDialogData,
    private readonly postsService: PostsService,
    private readonly sessionQuery: SessionQuery,
    private readonly postsQuery: PostsQuery,
  ) {
    this.dialogRef.disableClose = false;
  }

  ngOnInit() {
    this.post = this.data.post;

    this.likedByLoggedUser = this.postsQuery.selectEntity(this.post.id).pipe(
      switchMap(post => {
        if (post) {
          this.post = post;
        }
        return this.sessionQuery.currentUser$;
      }),
      map((loggedUser: User) =>
        this.post.likes
          ? !!this.post.likes.find(
              (like: Like) => like.votedBy.username === loggedUser.username,
            )
          : false,
      ),
    );
  }

  public addComment(comment: CreateComment) {
    this.postsService.createComment(comment);
  }

  public likePost() {
    this.postsService.likePost(String(this.post.id));
  }

  public dislikePost() {
    this.postsService.dislikePost(String(this.post.id));
  }

  public onNoClick() {
    this.dialogRef.close();
  }

  public commentsOverflow() {
    return this.post.comments.length <= 0 ? 'hidden' : 'scroll';
  }
}
