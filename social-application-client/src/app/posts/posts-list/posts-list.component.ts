import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Post } from '../../explore/state/post.model';
import { MatDialog } from '@angular/material';
import { PostComponent } from '../post/post.component';
import { PostDialogComponent } from '../post-dialog/post-dialog.component';

@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.scss'],
})
export class PostsListComponent implements OnInit {
  @Input() posts: Post[];
  @Input() loading: boolean;
  @Input() postEnd: boolean;

  @Output() postsOnScroll = new EventEmitter<null>();

  public infiniteScrollDistance = 1;
  public infiniteScrollThrottle = 1000;

  constructor(private readonly dialog: MatDialog) {}

  ngOnInit() {}

  public onScroll() {
    if (!this.postEnd) {
      this.postsOnScroll.emit();
    }
  }

  openDialog(post: Post): void {
    const dialogRef = this.dialog.open(PostDialogComponent, {
      width: '70vw',
      height: '90vh',
      data: { post },
      hasBackdrop: true,
    });

    dialogRef.backdropClick().subscribe(() => {
      dialogRef.close();
    });
  }
}
