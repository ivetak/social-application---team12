import { PostsService } from './../../../explore/state/posts.service';
import { FormControl } from '@angular/forms';
import { UpdateComment } from './../../../shared/models/comments/update-comment.model';
import {
  Component,
  OnInit,
  Input,
  EventEmitter,
  Output,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { Comment } from '../../../explore/state/post.model';
import { Observable } from 'rxjs';
import { SessionQuery } from '../../../auth/state/session.query';
import { User } from '../../../users/state/user.model';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss'],
})
export class CommentComponent implements OnInit {
  @Input() public comment: Comment;

  @Output() public readonly commentDeleted: EventEmitter<
    Comment
  > = new EventEmitter();

  @ViewChild('commentEditField', { static: false })
  public commentEditField: ElementRef;

  public commentEditMode = false;

  public commentContent;

  public loggedUserIsAuthor: Observable<boolean>;

  constructor(
    private readonly postsService: PostsService,
    private readonly sessionQuery: SessionQuery,
  ) {}

  ngOnInit() {
    this.loggedUserIsAuthor = this.sessionQuery.currentUser$.pipe(
      map(
        (loggedUser: User) =>
          this.comment.author.username === loggedUser.username,
      ),
    );
  }

  public editComment() {
    this.commentEditMode = true;
    this.commentContent = new FormControl(this.comment.content);
    setTimeout(() => this.commentEditField.nativeElement.focus());
  }

  public cancelEditComment() {
    this.commentEditMode = false;
  }

  public updateComment() {
    const editedComment: UpdateComment = new UpdateComment();
    editedComment.id = this.comment.id;
    if (
      this.commentContent.value !== this.comment.content ||
      this.commentContent.value.length <= 0
    ) {
      editedComment.content = this.commentContent.value;

      this.postsService.updateComment(editedComment);
    } else {
      this.commentEditMode = false;
    }
  }

  public onKeydown(event) {
    event.preventDefault();
  }

  public removeComment() {
    this.postsService.removeComment(String(this.comment.id));
  }
}
