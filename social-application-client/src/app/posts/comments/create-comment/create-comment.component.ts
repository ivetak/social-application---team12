import { CreateComment } from './../../../shared/models/comments/create-comment.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Comment } from '../../../explore/state/post.model';

@Component({
  selector: 'app-create-comment',
  templateUrl: './create-comment.component.html',
  styleUrls: ['./create-comment.component.scss'],
})
export class CreateCommentComponent implements OnInit {
  @Input() public postId: string;

  public commentForm: FormGroup;

  public comments: Comment[];

  @Output() public readonly commentCreated: EventEmitter<
    CreateComment
  > = new EventEmitter();

  constructor(private readonly fb: FormBuilder) {}

  ngOnInit() {
    this.commentForm = this.fb.group({
      content: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(1000),
        ]),
      ],
    });
  }

  public createComment(comment: string) {
    const commentToCreate: CreateComment = {
      postId: this.postId,
      content: comment,
    };

    this.commentCreated.emit(commentToCreate);
  }
}
