import { AuthComponent } from './auth.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AuthRoutingModule } from './auth-routing.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
    declarations: [AuthComponent, LoginComponent, RegisterComponent],
    imports: [AuthRoutingModule, SharedModule, CommonModule],
    providers: []
})
export class AuthModule { }

