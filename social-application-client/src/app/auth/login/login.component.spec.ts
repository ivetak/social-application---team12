import { catchError, throttle, toArray, first, tap, takeUntil } from 'rxjs/operators';
import { HttpClientModule } from '@angular/common/http';
import { ExploreComponent } from './../../explore/explore.component';
import { SessionService } from './../state/session.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { SharedModule } from '../../shared/';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormBuilder, Validators, FormGroup, FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SessionQuery } from '../state/session.query';
import { RouterTestingModule } from '@angular/router/testing';
import { Routes, Router } from '@angular/router';
import { of, throwError, interval } from 'rxjs';

describe('LoginComponent', () => {

  const routes: Routes = [
    { path: '', redirectTo: 'explore', pathMatch: 'full' },
    { path: 'explore', component: ExploreComponent },
  ];

  let formBuilder;
  // let activatedRoute;
  let router;
  let sessionQuery;
  let sessionService;
  let loginForm;


  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    jest.clearAllMocks();

    sessionService = {
      login() { }
    };

    sessionQuery = {

      get isLoggedIn$() {
        return of(true);
      }
    };

    formBuilder = {
      group() { }
    };

    router = {
      navigate() { }
    };


    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes(routes),
        SharedModule,
        BrowserAnimationsModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule

      ],
      declarations: [ExploreComponent, LoginComponent],
      providers: [
        SessionService,
        SessionQuery,
        FormBuilder,
      ]
    })
      .overrideProvider(SessionService, { useValue: sessionService })
      .overrideProvider(SessionQuery, { useValue: sessionQuery })
      .overrideProvider(FormBuilder, { useValue: formBuilder })
      .compileComponents();

    router = TestBed.get(Router);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    component.loginForm = new FormGroup({
      username: new FormControl(),
      password: new FormControl(),
      rememberMe: new FormControl(),
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('login should call sessionService.login with the user data', () => {

    const loginSpy = jest.spyOn(sessionService, 'login').mockImplementation(() => of(true));

    component.loginForm.controls.username.setValue('admin');
    component.loginForm.controls.password.setValue('Admin12345');
    component.loginForm.controls.rememberMe.setValue(true);

    component.onSubmit();

    fixture.detectChanges();

    expect(sessionService.login).toHaveBeenCalledWith({
      username: 'admin',
      password: 'Admin12345',

    },
      true);

  });

  it('login should call sessionService.login with the user data', () => {

    const loginSpy = jest.spyOn(sessionService, 'login').mockImplementation(() => of(true));

    component.loginForm.controls.username.setValue('admin');
    component.loginForm.controls.password.setValue('Admin12345');
    component.loginForm.controls.rememberMe.setValue(true);

    component.onSubmit();

    fixture.detectChanges();

    expect(sessionService.login).toHaveBeenCalledTimes(1);

  });

  it('successfull login should navigate to /explore', () => {

    const loginSpy = jest.spyOn(sessionService, 'login').mockImplementation(() => of(true));
    const navigateSpy = jest.spyOn(router, 'navigate');

    component.loginForm.controls.username.setValue('admin');
    component.loginForm.controls.password.setValue('Admin12345');
    component.loginForm.controls.rememberMe.setValue(true);

    component.onSubmit();

    fixture.detectChanges();

    expect(router.navigate).toHaveBeenCalledWith(['/']);

  });


  it('Unsuccessfull login should notify', () => {

    const error = {
      message: 'Wrong username or password'
    };

    const loginSpy = jest.spyOn(sessionService, 'login').mockImplementation(() => of(false));
    const navigateSpy = jest.spyOn(router, 'navigate');

    component.loginForm.controls.username.setValue(null);
    component.loginForm.controls.password.setValue(null);
    component.loginForm.controls.rememberMe.setValue(null);

    component.onSubmit();

    fixture.detectChanges();

    expect(sessionService.login).toHaveBeenCalledWith({
      username: null,
      password: null,
    },
      null);

  });

  it('Unsuccessfull login should throw', () => {

    const error = new Error('Wrong username or password');


    const loginSpy = jest.spyOn(sessionService, 'login').mockImplementation(() => throwError(error));
    const navigateSpy = jest.spyOn(router, 'navigate');

    component.loginForm.controls.username.setValue(null);
    component.loginForm.controls.password.setValue(null);
    component.loginForm.controls.rememberMe.setValue(null);

    component.onSubmit();

    fixture.detectChanges();

    expect(component.error).toBe('Wrong username or password');
  });

  it('ngOnDestroy', (done) => {

    const loginSpy = jest.spyOn(sessionQuery, 'isLoggedIn$', 'get').mockReturnValue(of(interval(1000)));
    const navigateSpy = jest.spyOn(router, 'navigate');

    component.loginForm.controls.username.setValue('admin');
    component.loginForm.controls.password.setValue('Admin12345');
    component.loginForm.controls.rememberMe.setValue(true);

    sessionQuery.isLoggedIn$.subscribe(() => {
      expect(router.navigate).toHaveBeenCalledTimes(0);

      done();
    });
    component.ngOnDestroy();

  });

  // it('ngOnDestroy should prevent memory leaks', (done) => {

  //   const loginSpy = jest.spyOn(sessionQuery, 'isLoggedIn$', 'get').mockReturnValue(of(0, 1, 2, 3, 4));
  //   const navigateSpy = jest.spyOn(router, 'navigate');

  //   component.loginForm.controls.username.setValue('admin');
  //   component.loginForm.controls.password.setValue('Admin12345');
  //   component.loginForm.controls.rememberMe.setValue(true);

  //   sessionQuery.isLoggedIn$.pipe(
  //     tap((counter: number) => {
  //       console.log(counter);

  //       if (counter === 0) {
  //         component.ngOnInit();
  //         console.log('ngOnInt');

  //       }

  //       if (counter === 2) {
  //         console.log('destroy');

  //         component.ngOnDestroy();
  //       }

  //     }))
  //     .subscribe((counter: number) => {

  //       if (counter === 4) {
  //         console.log(counter);

  //         expect(router.navigate).toHaveBeenCalledTimes(1);
  //         done();
  //       }

  //     });

  // });

});
