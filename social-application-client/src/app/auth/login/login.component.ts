import { takeUntil } from 'rxjs/operators';
import { Component, OnInit, OnDestroy } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SessionQuery } from '../state/session.query';
import { Subject } from 'rxjs';
import { SessionService } from '../state/session.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit, OnDestroy {
  public loginForm: FormGroup;
  public loading = false;
  public submitted = false;
  public error: string;

  private unsubscribe$: Subject<void>;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly sessionQuery: SessionQuery,
    private readonly sessionService: SessionService,
  ) {
    this.unsubscribe$ = new Subject<void>();
    this.loginForm = formBuilder.group({
      username: [
        '',
        Validators.compose([
          Validators.required,
          Validators.maxLength(20),
          Validators.minLength(2),
        ]),
      ],

      password: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(20),
          Validators.pattern(
            '^(?=.*[A-Z])(?=.*[a-z])(?=.*\\d)[A-Za-z\\d!$%@#£€*?&]{8,}$',
          ),
        ]),
      ],
      rememberMe: [false],
    });
  }

  ngOnInit() {
    // redirect to home if already logged in
    this.sessionQuery.isLoggedIn$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((logged: boolean) => {
        if (logged) {
          this.router.navigate(['/explore']);
        }
      });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  // convenience getter for easy access to form fields
  public get usernameControl(): AbstractControl {
    return this.loginForm.controls.username;
  }

  public get emailControl(): AbstractControl {
    return this.loginForm.controls.email;
  }

  public get passwordControl(): AbstractControl {
    return this.loginForm.controls.password;
  }

  public get rememberMeControl(): AbstractControl {
    return this.loginForm.controls.rememberMe;
  }

  public onFocusUsername(): void {
    this.usernameControl.markAsTouched();
  }

  public onFocusEmail(): void {
    this.emailControl.markAsTouched();
  }

  public onFocusPassword(): void {
    this.passwordControl.markAsTouched();
  }

  onSubmit() {
    console.log('logging');

    this.submitted = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    this.loading = true;
    this.sessionService
      .login(
        {
          username: this.usernameControl.value,
          password: this.passwordControl.value,
        },
        this.rememberMeControl.value,
      )
      .subscribe(
        _ => {
          this.router.navigate(['/explore']);
        },
        error => {
          this.error = error.message;
        },
        () => {
          this.loading = false;
        },
      );
  }
}
