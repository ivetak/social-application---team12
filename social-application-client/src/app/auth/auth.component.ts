import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css'],
})
export class AuthComponent implements OnInit {
  public authComponents: { link: string; label: string }[];

  constructor() {
    this.authComponents = [
      {
        link: 'login',
        label: 'Login',
      },
      {
        link: 'register',
        label: 'Register',
      },
    ];
  }

  ngOnInit() {}
}
