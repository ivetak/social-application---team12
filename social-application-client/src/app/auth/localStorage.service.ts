import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class LocalStorageService {
    public setLocalStorage(...pairs: { key: string; value: any }[]): void {
        pairs.map(pair => {
            if (Array.isArray(pair.value)) {
                localStorage.setItem(pair.key, JSON.stringify(pair.value));
            } else if (pair.value instanceof Object) {
                localStorage.setItem(pair.key, JSON.stringify(pair.value));
            } else {
                localStorage.setItem(pair.key, pair.value);
            }
        });
    }

    public getLocalStorage(...keys: string[]) {
        if (keys) {
            let pairs = [];
            keys.forEach(key => {
                try {
                    pairs = [...pairs, JSON.parse(localStorage.getItem(key))];
                } catch (error) {
                    pairs = [...pairs, localStorage.getItem(key)];
                }
            });

            return pairs;
        }
        return Array(localStorage.length).map((_, i) => {
            const pair = {
                key: localStorage.key(i),
                value: localStorage.getItem(localStorage.key(i))
            };
            return pair;
        });
    }

    public getLocalStorageByKey(key: string) {
        try {
            return JSON.parse(localStorage.getItem(key));
        } catch (error) {
            return localStorage.getItem(key);
        }
    }

    public clearLocalStorage(...keys: string[]) {
        if (keys) {
            let removedPairs = [];
            keys.forEach(key => {
                try {
                    removedPairs = [
                        ...removedPairs,
                        JSON.parse(localStorage.getItem(key))
                    ];
                    localStorage.removeItem(key);
                } catch (error) {
                    removedPairs = [...removedPairs, localStorage.getItem(key)];
                    localStorage.removeItem(key);
                }
            });
            return removedPairs;
        }
        return Array(localStorage.length).map((_, i) => {
            try {
                const pair = {
                    key: localStorage.key(i),
                    value: JSON.parse(localStorage.getItem(localStorage.key(i)))
                };
                localStorage.removeItem(localStorage.key(i));
                return pair;
            } catch (error) {
                const pair = {
                    key: localStorage.key(i),
                    value: localStorage.getItem(localStorage.key(i))
                };
                localStorage.removeItem(localStorage.key(i));
                return pair;
            }
        });
    }
}
