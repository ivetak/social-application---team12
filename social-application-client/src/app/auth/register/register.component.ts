import { RegisterCredentials } from './../../shared/models/users';
import { SessionService } from '../state/session.service';
import { SessionQuery } from '../state/session.query';
import { confirmPassword } from '../../shared/validators/confirmPassword.directive';
import { Component, OnInit, OnDestroy } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit, OnDestroy {
  public regForm: FormGroup;
  public loading = false;
  public submitted = false;
  public error: string;
  public returnUrl: string;

  private readonly unsubscribe$: Subject<void>;

  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private sessionQuery: SessionQuery,
    private sessionService: SessionService
  ) {
    this.unsubscribe$ = new Subject<void>();
    this.regForm = formBuilder.group(
      {
        username: [
          '',
          Validators.compose([
            Validators.required,
            Validators.maxLength(20),
            Validators.minLength(2),
          ]),
        ],
        email: [
          '',
          Validators.compose([
            Validators.required,
            Validators.maxLength(20),
            Validators.minLength(6),
            Validators.pattern(
              '^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$'
            ),
          ]),
        ],
        password: [
          '',
          Validators.compose([
            Validators.required,
            Validators.minLength(8),
            Validators.maxLength(20),
            Validators.pattern(
              '^(?=.*[A-Z])(?=.*[a-z])(?=.*\\d)[A-Za-z\\d!$%@#£€*?&]{8,}$'
            ),
          ]),
        ],
        passwordConfirm: ['', Validators.compose([Validators.required])],
      },
      { validators: confirmPassword }
    );
  }

  ngOnInit() {
    this.returnUrl = this.activatedRoute.snapshot.queryParams.returnUrl || '/';

    // redirect to home if already logged in
    this.sessionQuery.isLoggedIn$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((logged) => {
        if (logged) {
          this.router.navigate(['/explore']);
        }
      });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  // convenience getter for easy access to form fields
  public get usernameControl(): AbstractControl {
    return this.regForm.controls.username;
  }

  public get emailControl(): AbstractControl {
    return this.regForm.controls.email;
  }

  public get passwordControl(): AbstractControl {
    return this.regForm.controls.password;
  }

  public get passwordConfirmControl(): AbstractControl {
    return this.regForm.controls.passwordConfirm;
  }

  public onFocusUsername(): void {
    this.usernameControl.markAsTouched();
  }

  public onFocusEmail(): void {
    this.emailControl.markAsTouched();
  }

  public onFocusPassword(): void {
    this.passwordControl.markAsTouched();
  }

  public onFocusPasswordConfirm(): void {
    this.passwordConfirmControl.markAsTouched();
  }

  public onSubmit(user: RegisterCredentials) {
    this.submitted = true;

    // stop here if form is invalid
    if (this.regForm.invalid) {
      return;
    }

    this.loading = true;
    this.sessionService
      .register({
        username: this.usernameControl.value,
        email: this.emailControl.value,
        password: this.passwordControl.value,
      })
      .subscribe(
        () => {
          this.router.navigate(['/auth']);
        },
        (error) => {
          this.error = error;
        },
        () => {
          this.loading = false;
        }
      );
  }
}
