import { confirmPassword } from './../../shared/validators/confirmPassword.directive';
import { LoginComponent } from './../login/login.component';
import { AuthComponent } from './../auth.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterComponent } from './register.component';
import { of, throwError } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '../../shared';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Routes, Router } from '@angular/router';
import { SessionService } from '../state/session.service';
import { SessionQuery } from '../state/session.query';

describe('RegisterComponent', () => {

  // const routes: Routes = [
  //   { path: 'auth', redirectTo: 'login', pathMatch: 'full' },
  //   { path: 'login', component: LoginComponent },

  // ];

  const routes: Routes = [
    {
      path: 'auth',
      component: AuthComponent,
      children: [
        { path: '', redirectTo: 'login', pathMatch: 'full' },
        { path: 'login', component: LoginComponent },
        { path: 'register', component: RegisterComponent },
      ],
    },
  ];

  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;

  let formBuilder;
  let activatedRoute;
  let router;
  let sessionQuery;
  let sessionService;

  beforeEach(async(() => {
    jest.clearAllMocks();

    sessionService = {
      register() { }
    };

    sessionQuery = {

      get isLoggedIn$() {
        return of(true);
      }
    };

    formBuilder = {
      group() { }
    };

    router = {
      navigate() { }
    };


    TestBed.configureTestingModule({
      declarations: [RegisterComponent, LoginComponent, AuthComponent],
      imports: [
        RouterTestingModule.withRoutes(routes),
        SharedModule,
        BrowserAnimationsModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule

      ],
      providers: [
        SessionService,
        SessionQuery,
        FormBuilder,
      ],
    })
      .overrideProvider(SessionService, { useValue: sessionService })
      .overrideProvider(SessionQuery, { useValue: sessionQuery })
      .overrideProvider(FormBuilder, { useValue: formBuilder })
      .compileComponents();

    router = TestBed.get(Router);

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    component.regForm = new FormGroup({
      username: new FormControl(),
      email: new FormControl(),
      password: new FormControl(),
      passwordConfirm: new FormControl(),

    });

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('register should call sessionService.register with the user data', () => {

    const user = {
      username: 'admin',
      email: 'admin@admin.bg',
      password: 'Admin12345',
    };

    const loginSpy = jest.spyOn(sessionService, 'register').mockImplementation(() => of(true));

    component.regForm.controls.username.setValue('admin');
    component.regForm.controls.email.setValue('admin@admin.bg');
    component.regForm.controls.password.setValue('Admin12345');

    component.onSubmit(user);

    fixture.detectChanges();

    expect(sessionService.register).toHaveBeenCalledWith({
      username: 'admin',
      email: 'admin@admin.bg',
      password: 'Admin12345',
    });

  });

  it('register should call sessionService.register', () => {

    const user = {
      username: 'admin',
      email: 'admin@admin.bg',
      password: 'Admin12345',
    };

    const loginSpy = jest.spyOn(sessionService, 'register').mockImplementation(() => of(true));

    component.regForm.controls.username.setValue('admin');
    component.regForm.controls.email.setValue('admin@admin.bg');
    component.regForm.controls.password.setValue('Admin12345');

    component.onSubmit(user);

    fixture.detectChanges();

    expect(sessionService.register).toHaveBeenCalledTimes(1);

  });

  it('register should should navigate to /auth/login', () => {

    const user = {
      username: 'admin',
      email: 'admin@admin.bg',
      password: 'Admin12345',
    };

    const navigateSpy = jest.spyOn(router, 'navigate');
    const loginSpy = jest.spyOn(sessionService, 'register').mockImplementation(() => of(true));

    component.regForm.controls.username.setValue('admin');
    component.regForm.controls.email.setValue('admin@admin.bg');
    component.regForm.controls.password.setValue('Admin12345');

    component.onSubmit(user);

    fixture.detectChanges();

    expect(router.navigate).toHaveBeenCalledWith(['auth']);

  });

  it('Unsuccessfull register should throw', () => {

    const error = new Error('Wrong username or password');

    const loginSpy = jest.spyOn(sessionService, 'register').mockImplementation(() => throwError(error.message));
    const navigateSpy = jest.spyOn(router, 'navigate');

    component.regForm.controls.username.setValue(null);
    component.regForm.controls.email.setValue(null);
    component.regForm.controls.password.setValue(null);

    component.onSubmit(null);

    fixture.detectChanges();

    expect(component.error).toBe('Wrong username or password');

  });


});
