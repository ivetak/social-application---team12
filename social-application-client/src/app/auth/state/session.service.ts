import { ThemeState } from './../../core/state/theme.store';
import { SessionStore, UserDataState, SessionState } from './session.store';
import {
  LoginCredentials,
  RegisterCredentials,
} from '../../shared/models/users';
import { LocalStorage } from '../../shared/enum';
import { APIToken } from '../../shared/models/token';
import { LocalStorageService } from '../localStorage.service';
import { API_CONFIG, APP_THEMES } from '../../config';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import * as moment from 'moment';
import { User } from 'src/app/users/state/user.model';
import { toBoolean } from '@datorama/akita';

@Injectable({
  providedIn: 'root',
})
export class SessionService {
  private jwtService = new JwtHelperService();

  public constructor(
    private readonly http: HttpClient,
    private readonly localStorage: LocalStorageService,
    private readonly router: Router,
    private readonly store: SessionStore,
  ) {
    this.getLoggedUserData(this.store.getValue().user.username);
  }

  public tokenNotExpired(token: string): boolean {
    const expiresAt: Date = this.jwtService.getTokenExpirationDate(token);
    return moment().isBefore(expiresAt);
  }

  public register(credentials: RegisterCredentials): Observable<User> {
    return this.http
      .post<User>(`${API_CONFIG.API_DOMAIN_NAME}/auth/register`, {
        ...credentials,
      })
      .pipe(
        tap((createdUser: User) => {
          this.router.navigate(['/auth']);
        }),
      );
  }

  public login(
    credentials: LoginCredentials,
    remember: boolean,
  ): Observable<User> {
    return this.http
      .post<APIToken>(`${API_CONFIG.API_DOMAIN_NAME}/auth/login`, credentials)
      .pipe(map(token => this.setSession(token, remember)));
  }

  public logout() {
    this.http
      .delete<any>(`${API_CONFIG.API_DOMAIN_NAME}/auth/logout`)
      .subscribe(() => {
        this.clearSession();
      });
  }

  public changeTheme(themeName: APP_THEMES) {
    this.store.update((state: SessionState) => ({
      ...state,
      theme: { ...state.theme, themeName },
    }));
    this.setPersistantSessionState({
      user: this.store.getValue().user,
      token: this.store.getValue().token,
      theme: { ...this.store.getValue().theme, themeName },
    });
  }

  public switchLight(dark: boolean) {
    this.store.update((state: SessionState) => ({
      ...state,
      theme: { ...state.theme, dark },
    }));
    this.setPersistantSessionState({
      user: this.store.getValue().user,
      token: this.store.getValue().token,
      theme: { ...this.store.getValue().theme, dark },
    });
  }

  private setPersistantSessionState(state: UserDataState) {
    this.store.update({
      oldSessions: [
        ...this.store
          .getValue()
          .oldSessions.map((oldSession: UserDataState) =>
            oldSession.user.username === state.user.username
              ? state
              : oldSession,
          ),
      ],
    });
  }

  private setSession(token: APIToken, remember: boolean): User {
    console.log('setting session');

    const decodedAccessTokenObj: User = this.jwtService.decodeToken(
      token.token,
    );
    const sessionTheme = this.store
      .getValue()
      .oldSessions.find(
        (oldSession: UserDataState) =>
          oldSession.user.username === decodedAccessTokenObj.username,
      );
    const userData: UserDataState = {
      user: { ...decodedAccessTokenObj },
      token: { ...token },
      theme: sessionTheme
        ? sessionTheme.theme
        : { themeName: APP_THEMES.WORKWISE_DEFAULT, dark: false },
    };

    this.store.login(userData);
    if (remember) {
      this.store.rememberSession(userData);
    }
    this.getLoggedUserData(userData.user.username);

    this.localStorage.setLocalStorage(
      { key: LocalStorage.token, value: token.token },
      { key: LocalStorage.loggedUser, value: decodedAccessTokenObj },
    );
    return decodedAccessTokenObj;
  }

  private clearSession(): void {
    this.localStorage.clearLocalStorage(
      LocalStorage.token,
      LocalStorage.loggedUser,
    );
    this.store.logout();
    this.router.navigate(['/auth']);
  }

  private getLoggedUserData(username: string) {
    if (
      toBoolean(this.store.getValue().user) &&
      this.store.getValue().token &&
      this.tokenNotExpired(this.store.getValue().token.token)
    ) {
      this.http
        .get<User>(
          `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/${username}`,
        )
        .subscribe(user => {
          this.updateLoggedUser(user);
        });
    }
  }

  private updateLoggedUser(user: Partial<User>) {
    this.store.update((state: SessionState) => {
      return { ...state, user: { ...state.user, ...user } };
    });
  }
}
