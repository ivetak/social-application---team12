import { ThemeState } from './../../core/state/theme.store';
import { avatarUrlFilter } from './../../common';
import { User, createUser } from './../../users/state/user.model';
import { UserRole } from '../../shared/enum';
import { APIToken } from 'src/app/shared/models';
import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';
import { APP_THEMES } from 'src/app/config';

export interface UserDataState {
  user: User;
  token: APIToken;
  theme: ThemeState;
}

export interface SessionState {
  user: User;
  token: APIToken;
  theme: ThemeState;
  oldSessions: UserDataState[];
}

function createSession(user: User, token: APIToken): Partial<SessionState> {
  return {
    user: { ...user },
    token: { ...token },
  };
}

function createInitialState(): SessionState {
  return {
    user: { ...guestUser },
    token: null,
    oldSessions: [],
    theme: null,
  };
}

export const guestUser: User = createUser({
  id: '0',
  username: 'Guest',
  roles: [UserRole.Guest],
  followers: [],
  following: [],
  followersCount: 0,
});

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'session' })
export class SessionStore extends Store<SessionState> {
  constructor() {
    super(createInitialState());
  }

  public login(session: UserDataState) {
    const { user, token } = createSession(session.user, session.token);
    const persistentSession = this.getPersistentSession(session.user.username);
    this.update({
      user,
      token,
      theme: persistentSession ? persistentSession.theme : null,
    });
  }

  public logout() {
    this.update({ user: guestUser, token: null });
  }

  public rememberSession(session: UserDataState) {
    const oldSessions: UserDataState[] = this.getValue().oldSessions;
    let updated = false;
    let updatedSessions = oldSessions.map(state => {
      if (state.user.username === session.user.username) {
        updated = true;
        return session;
      }
      return state;
    });
    if (oldSessions.length === updatedSessions.length && !updated) {
      updatedSessions = [...updatedSessions, session];
    }
    this.update({ oldSessions: [...updatedSessions] });
  }

  public akitaPreUpdate(_, nextState: SessionState) {
    const filteredAvatarNextState = {
      ...nextState,
      user: {
        ...nextState.user,
        avatar: avatarUrlFilter(nextState.user.avatar),
      },
    };
    return filteredAvatarNextState;
  }

  private getPersistentSession(username: string) {
    return this.getValue().oldSessions.find(
      (oldSession: UserDataState) => oldSession.user.username === username,
    );
  }
}
