import { JwtHelperService } from '@auth0/angular-jwt';
import { Injectable } from '@angular/core';
import { Query, toBoolean } from '@datorama/akita';
import { SessionStore, SessionState } from './session.store';

@Injectable({ providedIn: 'root' })
export class SessionQuery extends Query<SessionState> {
  public isLoggedIn$ = this.select(
    ({ user, token }) =>
      toBoolean(user) && token && !this.jwt.isTokenExpired(token.token),
  );

  public token$ = this.select(({ token }) => (token ? token.token : null));

  public currentUser$ = this.select(({ user }) => user);

  public currentUserTheme$ = this.select(({ theme }) => theme);

  private readonly jwt = new JwtHelperService();

  constructor(protected store: SessionStore) {
    super(store);
  }
}
