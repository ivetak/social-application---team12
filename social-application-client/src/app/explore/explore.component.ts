import {
  explorePostsRequestSkip,
  explorePostsQueryStep,
  explorePostsQueryStepIncrement,
  explorePostsRequestSkipIncrement,
  explorePostsRequestSkipThreshold,
  explorePostsQueryDefaultLimit,
  explorePostsRequestDefaultLimit,
} from './../shared/constants/posts.constants';
import { takeUntil, map, delay, filter } from 'rxjs/operators';
import { PostsService } from './state/posts.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { PostsQuery } from './state/posts.query';
import { Subject, Observable } from 'rxjs';
import { Post } from './state/post.model';
import { Order, sortByOptions } from '@datorama/akita';

@Component({
  selector: 'app-explore',
  templateUrl: './explore.component.html',
  styleUrls: ['./explore.component.css'],
})
export class ExploreComponent implements OnInit, OnDestroy {
  public posts$: Observable<Post[]>;
  public skip = explorePostsRequestSkip; // request
  public step = explorePostsQueryStep; // query
  public requestLoading$: Observable<boolean>;
  public postRequestEnd$: Observable<boolean>;
  public postsQueryEnd$: Observable<boolean>;

  private postsCount: number;
  private unsubscribe$: Subject<void>;

  constructor(
    private readonly postsQuery: PostsQuery,
    private readonly postsService: PostsService,
  ) {
    this.unsubscribe$ = new Subject<void>();
  }

  ngOnInit() {
    this.requestPosts(this.skip);
    this.posts$ = this.queryPosts(this.step);

    this.postsQuery
      .selectCount(post => post.public)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(count => (this.postsCount = count));

    this.requestLoading$ = this.postsQuery.selectLoading();

    this.postsQueryEnd$ = this.postsQuery
      .selectCount((entity: Post) => entity.public)
      .pipe(map(postsCount => this.step > postsCount));

    this.postRequestEnd$ = this.postsQuery.postEnd$;
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public postsOnScroll() {
    this.step += explorePostsQueryStepIncrement;
    this.posts$ = this.queryPosts(this.step);
    if (
      !(this.step % explorePostsRequestSkipThreshold) ||
      this.step > this.postsCount
    ) {
      this.skip += explorePostsRequestSkipIncrement;
      this.requestPosts(this.skip);
    }
  }

  private queryPosts(
    step: number,
    limit: number = explorePostsQueryDefaultLimit,
  ): Observable<Post[]> {
    return this.postsQuery
      .selectAll({
        filterBy: [entity => entity.public],
        limitTo: step,
      })
      .pipe(map((posts: Post[]) => posts.slice(0, step)));
  }

  private requestPosts(
    skip: number,
    limit: number = explorePostsRequestDefaultLimit,
  ) {
    this.postsService.getPublicPosts(skip, limit);
  }
}
