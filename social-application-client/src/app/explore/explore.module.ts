import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';

import { ExploreRoutingModule } from './explore-routing.module';
import { ExploreComponent } from './explore.component';

@NgModule({
  declarations: [ExploreComponent],
  imports: [SharedModule, ExploreRoutingModule]
})
export class ExploreModule {}
