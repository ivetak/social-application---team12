import { Injectable } from '@angular/core';
import { QueryEntity, QueryConfig, Order } from '@datorama/akita';
import { PostsStore, PostsState } from './posts.store';
import { Post } from './post.model';

@Injectable({
  providedIn: 'root',
})
@QueryConfig({
  sortBy: 'createdOn',
  sortByOrder: Order.DESC // Order.DESC
})
export class PostsQuery extends QueryEntity<PostsState, Post> {
  public postEnd$ = this.select(state => state.ui.postEnd);

  constructor(protected store: PostsStore) {
    super(store);
  }
}
