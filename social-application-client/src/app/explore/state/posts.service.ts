import { SocketService } from './../../core/sockets/websocket.service';
import { UpdateComment } from './../../shared/models/comments/update-comment.model';
import { CreateComment } from './../../shared/models/comments/create-comment.model';
import { CreatePost } from './../../shared/models/posts/create-post.model';
import { Injectable } from '@angular/core';
import { ID, arrayUpdate } from '@datorama/akita';
import { HttpClient } from '@angular/common/http';
import { PostsStore } from './posts.store';
import { Post, Comment } from './post.model';
import { API_CONFIG } from '../../config';
import { avatarUrlFilter } from '../../common';
import { delay, map, catchError, tap } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class PostsService {
  constructor(
    private postsStore: PostsStore,
    private http: HttpClient,
    private readonly ws: SocketService,
  ) {}

  get() {
    this.postsStore.setLoading(true);
    this.http.get<Post[]>(`${API_CONFIG.API_DOMAIN_NAME}/posts`).subscribe(
      (entities: Post[]) => {
        this.postsStore.set(entities);
      },
      error => this.postsStore.setError(error),
      () => this.postsStore.setLoading(false),
    );
  }

  getPost(postId: string) {
    this.postsStore.setLoading(true);
    this.http
      .get<Post>(
        `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_POSTS_PATH}/${postId}`,
      )
      .subscribe((post: Post) => {
        this.postsStore.upsert(post.id, post);
        this.postsStore.setActive(post.id);
      });
  }

  getPublicPosts(skip: number = 0, take: number = 50) {
    this.postsStore.setLoading(true);
    this.http
      .get<Post[]>(
        `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_POSTS_PATH}?public=true&skip=${skip}&take=${take}`,
      )
      .pipe(delay(100))
      .subscribe(
        (entities: Post[]) => {
          if (entities.length < take) {
            this.postsStore.setPostEnd();
          }
          this.postsStore.upsertMany(entities);
        },
        error => this.postsStore.setError(error),
        () => this.postsStore.setLoading(false),
      );
  }

  getDashboardPosts(skip: number = 0, take: number = 50) {
    this.postsStore.setLoading(true);
    this.http
      .get<Post[]>(
        `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_POSTS_PATH}/dashboard?skip=${skip}&take=${take}`,
      )
      .pipe(delay(100))
      .subscribe(
        (entities: Post[]) => {
          if (entities.length < take) {
            this.postsStore.setPostEnd();
          }
          this.postsStore.upsertMany(entities);
        },
        error => this.postsStore.setError(error),
        () => this.postsStore.setLoading(false),
      );
  }

  getUserPosts(userId: string, skip: number = 0, take: number = 50) {
    return this.http
      .get<Post[]>(
        `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/${userId}/posts?skip=${skip}&take=${take}`,
      )
      .pipe(
        map((posts: Post[]) => {
          if (posts.length < take) {
            this.postsStore.setPostEnd();
          }
          this.postsStore.upsertMany(posts);

          return true;
        }),
        catchError(error => {
          this.postsStore.setError(error);
          return throwError(error);
        }),
        tap(() => this.postsStore.setLoading(false)),
      );
  }

  createPost(post: CreatePost) {
    const formData = new FormData();
    formData.append('description', post.description);
    formData.append('public', post.public.toString());
    formData.append('image', post.image);
    return this.http
      .post<Post>(
        `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_POSTS_PATH}`,
        formData,
      )
      .pipe(
        map((createdPost: Post) => {
          this.postsStore.add(createdPost);
          return true;
        }),
        delay(300),
      );
  }

  likePost(postId: string) {
    this.http
      .post<Post>(
        `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_POSTS_PATH}/${postId}/votes`,
        {},
      )
      .subscribe((post: Post) => {
        this.postsStore.upsert(post.id, post);
      });
  }

  dislikePost(postId: string) {
    this.http
      .delete<Post>(
        `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_POSTS_PATH}/${postId}/votes`,
        {},
      )
      .subscribe((post: Post) => {
        this.postsStore.upsert(post.id, post);
      });
  }

  add(post: Post) {
    this.postsStore.add(post);
  }

  update(id, post: Partial<Post>) {
    this.postsStore.update(id, post);
  }

  removePost(postId: string) {
    this.http
      .delete<Post>(
        `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_POSTS_PATH}/${postId}`,
      )
      .subscribe((deletedPost: Post) => {
        this.postsStore.remove(deletedPost.id);
      });
  }

  remove(id: ID) {
    this.postsStore.remove(id);
  }

  public createComment(comment: CreateComment) {
    this.http
      .post<Post>(
        `${API_CONFIG.API_DOMAIN_NAME}/comments/${comment.postId}`,
        comment.content,
      )
      .subscribe(
        (post: Post) => {
          this.postsStore.upsert(post.id, { comments: post.comments });
        },
        error => {
          this.postsStore.setError(error);
        },
      );
  }

  public updateComment(comment: UpdateComment) {
    this.http
      .patch<Comment>(
        `${API_CONFIG.API_DOMAIN_NAME}/comments/${comment.id}`,
        comment,
      )
      .subscribe((newComment: Comment) => {
        this.postsStore.update(newComment.post.id, newPost => ({
          comments: arrayUpdate(newPost.comments, comment.id, {
            content: newComment.content,
          }),
        }));
      });
  }

  public removeComment(commentId: string) {
    this.http
      .delete<Post>(`${API_CONFIG.API_DOMAIN_NAME}/comments/${commentId}`)
      .subscribe((updatedPost: Post) => {
        this.postsStore.update(updatedPost.id, updatedPost);
      });
  }
}
