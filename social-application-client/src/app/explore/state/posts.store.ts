import { avatarUrlFilter } from './../../common/store/avatar-url-filter.constant';
import { Injectable } from '@angular/core';
import {
  EntityState,
  EntityStore,
  StoreConfig,
  ActiveState,
} from '@datorama/akita';
import { Post, Comment } from './post.model';

export interface PostsState extends EntityState<Post>, ActiveState {
  ui: {
    postEnd: boolean;
  };
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'posts' })
export class PostsStore extends EntityStore<PostsState, Post> {
  constructor() {
    super();
    this.update({ ui: { postEnd: false } });
  }

  public setPostEnd() {
    this.update({ ui: { postEnd: true } });
  }

  akitaPreAddEntity(post: Post) {
    return {
      ...post,
      author: { ...post.author, avatar: avatarUrlFilter(post.author.avatar) },
      comments: post.comments
        ? post.comments.map((comment: Comment) => ({
            ...comment,
            author: {
              ...comment.author,
              avatar: avatarUrlFilter(comment.author.avatar),
            },
          }))
        : [],
    };
  }

  akitaPreUpdateEntity(post: Post, newPost: Post) {
    return {
      ...newPost,
      author: {
        ...newPost.author,
        avatar: avatarUrlFilter(newPost.author.avatar),
      },
      comments: newPost.comments
        ? newPost.comments.map((comment: Comment) => ({
            ...comment,
            author: {
              ...comment.author,
              avatar: avatarUrlFilter(comment.author.avatar),
            },
          }))
        : [],
    };
  }
}
