import { User } from './../../users/state/user.model';
import { Image } from '../../shared/interfaces/post/image.interface';
import { ID } from '@datorama/akita';

export interface Post {
  id: ID;
  description: string;
  createdOn: number;
  updatedOn: number;
  public: boolean;
  image: Image;
  author: User;
  comments: Comment[];
  commentsCount: number;
  likes: Like[];
  likesCount: number;
}

export interface Comment {
  id: ID;
  createdOn: string;
  updatedOn: string;
  content: string;
  author: User;
  post: Post;
}

export interface Like {
  id: ID;
  votedBy: User;
}

/**
 * A factory function that creates Posts
 */
export function createPost(params: Partial<Post>) {
  return {} as Post;
}
