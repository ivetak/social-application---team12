import { first, delay } from 'rxjs/operators';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { PostsService } from './posts.service';
import { async, TestBed } from '@angular/core/testing';
import { PostsStore } from './posts.store';
import { of, throwError } from 'rxjs';
import { API_CONFIG } from '../../config';
import * as akita from '@datorama/akita';
import { error } from 'protractor';

describe('PostsService', () => {

    jest.mock('@datorama/akita');

    let getPostService: () => PostsService;

    let postsStore;
    let http;
    let postsQuery;

    beforeEach(async(() => {
        jest.clearAllMocks();

        postsQuery = {
            selectAll() { }
        };

        postsStore = {
            setLoading() { },
            set() { },
            setError() { },
            setPostEnd() { },
            upsertMany() { },
            add() { },
            upsert() { },
            update() { },
            remove() { },
        };

        http = {
            get() { return of(); },
            post() { },
            delete() { },
            patch() { },
        };


        TestBed.configureTestingModule({
            declarations: [],
            imports: [
                HttpClientModule
            ],
            providers: [
                PostsService,
                HttpClient,
                PostsStore
            ],
        })
            .overrideProvider(PostsStore, { useValue: postsStore })
            .overrideProvider(HttpClient, { useValue: http });

        getPostService = () => TestBed.get(PostsService);


    }));

    afterEach(async () => {
        jest.clearAllMocks();

    });

    it('should create', () => {
        expect(getPostService()).toBeTruthy();
    });


    it('get() should call http.get', () => {

        const url = `${API_CONFIG.API_DOMAIN_NAME}/posts`;

        const mockedPostData = 'mockedPostData';

        const spyPostsStore = jest.spyOn(postsStore, 'setLoading').mockImplementation(() => of(true));
        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => of(mockedPostData));
        const spyPostsStoreSet = jest.spyOn(postsStore, 'set').mockImplementation(() => of(mockedPostData));

        getPostService().get();

        expect(postsStore.set).toHaveBeenCalledTimes(1);
        expect(postsStore.set).toHaveBeenCalledWith(mockedPostData);
        expect(http.get).toHaveBeenCalledWith(url);
    });

    it('get() should call postsStore.setLoading', () => {

        const url = `${API_CONFIG.API_DOMAIN_NAME}/posts`;

        const mockedPostData = 'mockedPostData';

        const spyPostsStore = jest.spyOn(postsStore, 'setLoading').mockImplementation(() => of(true));
        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => of(mockedPostData));
        const spyPostsStoreSet = jest.spyOn(postsStore, 'set').mockImplementation(() => of(mockedPostData));

        getPostService().get();

        expect(postsStore.setLoading).toHaveBeenCalledWith(true);

    });

    it('get() should notify', () => {

        const url = `${API_CONFIG.API_DOMAIN_NAME}/posts`;


        const error = new Error('error');

        const mockedPostData = 'mockedPostData';

        const spyPostsStore = jest.spyOn(postsStore, 'setLoading').mockImplementation(() => of(true));
        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => throwError(error.message));
        const spyPostsStoreSet = jest.spyOn(postsStore, 'set').mockImplementation(() => of(mockedPostData));
        const spyOnError = jest.spyOn(postsStore, 'setError');

        getPostService().get();

        expect(postsStore.setError).toHaveBeenCalledWith(error.message);
        expect(postsStore.setError).toHaveBeenCalledTimes(1);
    });

    it('getPublicPosts() should call postStore.setLoading', () => {

        const url = `${API_CONFIG.API_DOMAIN_NAME}/posts?public=true&skip=0&take=50`;
        const mockedPostData = 'mockedPostData';

        const spyPostsStore = jest.spyOn(postsStore, 'setLoading').mockImplementation(() => of(true));
        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => of(mockedPostData));
        const spyPostsStoreSet = jest.spyOn(postsStore, 'setPostEnd').mockImplementation(() => of(mockedPostData));
        const spyPostsUpsertMany = jest.spyOn(postsStore, 'upsertMany').mockImplementation(() => { });

        const spyOnError = jest.spyOn(postsStore, 'setError');

        getPostService().getPublicPosts();

        expect(postsStore.setLoading).toHaveBeenCalledWith(true);
        expect(postsStore.setLoading).toHaveBeenCalledTimes(1);

    });

    it('getPublicPosts() should call http.get', () => {

        const url = `${API_CONFIG.API_DOMAIN_NAME}/posts?public=true&skip=0&take=50`;
        const take = 50;
        const mockedPostData = 'mockedPostData';

        const spyPostsStore = jest.spyOn(postsStore, 'setLoading').mockImplementation(() => of(true));
        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => of(mockedPostData));
        // const spyOnHttpGet = jest.spyOn(http, 'get').mockReturnValue(of(mockedPostData));
        const spyPostsStoreSet = jest.spyOn(postsStore, 'setPostEnd').mockImplementation(() => of(mockedPostData));
        const spyPostsUpsertMany = jest.spyOn(postsStore, 'upsertMany').mockImplementation(() => { });

        const spyOnError = jest.spyOn(postsStore, 'setError');

        getPostService().getPublicPosts();

        expect(http.get).toHaveBeenCalledWith(url);
        // expect(http.get).toEqual(mockedPostData);

    });


    it('getPublicPosts() should call postsStore.setPostEnd()', () => {

        const url = `${API_CONFIG.API_DOMAIN_NAME}/posts?public=true&skip=0&take=50`;
        const take = 50;
        const mockedPostData = 'mockedPostData';

        const spyPostsStore = jest.spyOn(postsStore, 'setLoading').mockImplementation(() => of(true));
        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => of(mockedPostData));
        const spyPostsStoreSet = jest.spyOn(postsStore, 'setPostEnd').mockImplementation(() => of(mockedPostData));
        const spyPostsUpsertMany = jest.spyOn(postsStore, 'upsertMany').mockImplementation(() => { });

        const spyOnError = jest.spyOn(postsStore, 'setError');

        getPostService().getPublicPosts();

        if (mockedPostData.length < take) {
            expect(postsStore.setPostEnd).toHaveBeenCalledTimes(0);
        }

        if (mockedPostData.length > take) {
            expect(postsStore.setPostEnd).toHaveBeenCalledTimes(1);
        }

    });

    it('getPublicPosts() should call postsStore.upsertMany()', (done) => {

        const url = `${API_CONFIG.API_DOMAIN_NAME}/posts?public=true&skip=0&take=50`;

        const mockedPostData = 'mockedPostData';

        const spyPostsStore = jest.spyOn(postsStore, 'setLoading').mockImplementation(() => of(true));
        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => of(mockedPostData));
        const spyPostsStoreSet = jest.spyOn(postsStore, 'setPostEnd').mockImplementation(() => of(mockedPostData));
        const spyPostsUpsertMany = jest.spyOn(postsStore, 'upsertMany').mockImplementation(() => { });

        const spyOnError = jest.spyOn(postsStore, 'setError');

        getPostService().getPublicPosts();

        http.get().pipe(delay(200)).subscribe(() => {
            expect(postsStore.upsertMany).toHaveBeenCalledWith(mockedPostData);
            done();

        });

    });

    it('Unsuccessful getPublicPosts() should notify', () => {

        const url = `${API_CONFIG.API_DOMAIN_NAME}/posts?public=true&skip=0&take=50`;

        const error = new Error('error');

        const mockedPostData = 'mockedPostData';

        const spyPostsStore = jest.spyOn(postsStore, 'setLoading').mockImplementation(() => of(true));
        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => throwError(error.message));
        const spyPostsStoreSet = jest.spyOn(postsStore, 'upsertMany').mockImplementation(() => { });
        const spyOnError = jest.spyOn(postsStore, 'setError');

        getPostService().getPublicPosts();

        expect(postsStore.setError).toHaveBeenCalledWith(error.message);
        expect(postsStore.setError).toHaveBeenCalledTimes(1);

    });

    it('Successful method getDashboardPosts() should should call postsStore.setLoading', () => {

        const url = `${API_CONFIG.API_DOMAIN_NAME}/posts/dashboard?skip=0&take=50`;
        const mockedPostData = 'mockedPostData';

        const take = 10;

        const spyPostsStore = jest.spyOn(postsStore, 'setLoading').mockImplementation(() => of(true));
        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => of(mockedPostData));
        const spyPostsStoreSet = jest.spyOn(postsStore, 'setPostEnd').mockImplementation(() => of(mockedPostData));
        const spyPostsUpsertMany = jest.spyOn(postsStore, 'upsertMany').mockImplementation(() => { });

        const spyOnError = jest.spyOn(postsStore, 'setError');

        getPostService().getDashboardPosts();

        expect(postsStore.setLoading).toHaveBeenCalledWith(true);
        expect(postsStore.setLoading).toHaveBeenCalledTimes(1);

    });

    it('Successful method getDashboardPosts() should should call postsStore.setPOstEnd if length of posts[] < take', () => {

        const url = `${API_CONFIG.API_DOMAIN_NAME}/posts/dashboard?skip=0&take=50`;
        const mockedPostData = 'mockedPostData';

        const take = 10;

        const spyPostsStore = jest.spyOn(postsStore, 'setLoading').mockImplementation(() => of(true));
        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => of(mockedPostData));
        const spyPostsStoreSet = jest.spyOn(postsStore, 'setPostEnd').mockImplementation(() => of(mockedPostData));
        const spyPostsUpsertMany = jest.spyOn(postsStore, 'upsertMany').mockImplementation(() => { });

        const spyOnError = jest.spyOn(postsStore, 'setError');

        getPostService().getDashboardPosts();

        if (mockedPostData.length < take) {
            expect(postsStore.setPostEnd).toHaveBeenCalledTimes(1);
        } else {
            expect(postsStore.setPostEnd).toHaveBeenCalledTimes(0);
        }

    });

    it('Successful method getDashboardPosts() should should call postsStore.upsertMany', (done) => {

        const url = `${API_CONFIG.API_DOMAIN_NAME}/posts/dashboard?skip=0&take=50`;
        const mockedPostData = 'mockedPostData';

        const take = 10;

        const spyPostsStore = jest.spyOn(postsStore, 'setLoading').mockImplementation(() => of(true));
        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => of(mockedPostData));
        const spyPostsStoreSet = jest.spyOn(postsStore, 'setPostEnd').mockImplementation(() => of(mockedPostData));
        const spyPostsUpsertMany = jest.spyOn(postsStore, 'upsertMany').mockImplementation(() => { });

        const spyOnError = jest.spyOn(postsStore, 'setError');

        getPostService().getDashboardPosts();

        http.get().pipe(delay(200)).subscribe(() => {
            expect(postsStore.upsertMany).toHaveBeenCalledWith(mockedPostData);
            expect(http.get).toHaveBeenCalledWith(url);
            done();

        });


    });

    it('Unsuccessful getDashboardPosts() should notify and should call method postsStore.setError', () => {

        const url = `${API_CONFIG.API_DOMAIN_NAME}/posts?public=true&skip=0&take=50`;

        const error = new Error('error');

        const mockedPostData = 'mockedPostData';

        const spyPostsStore = jest.spyOn(postsStore, 'setLoading').mockImplementation(() => of(true));
        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => throwError(error.message));
        const spyPostsStoreSet = jest.spyOn(postsStore, 'upsertMany').mockImplementation(() => { });
        const spyOnError = jest.spyOn(postsStore, 'setError');

        getPostService().getDashboardPosts();


        expect(postsStore.setError).toHaveBeenCalledWith(error.message);
        expect(postsStore.setError).toHaveBeenCalledTimes(1);

    });

    it('Successful method getUserPosts() should should call postsStore.get', () => {
        const userId = '1';
        const skip = 0;
        const take = 5;
        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_USERS_PATH}/${userId}/posts?skip=${skip}&take=${take}`;
        const mockedPostsData = 'mockedPostData';

        const spyPostsStore = jest.spyOn(postsStore, 'setLoading').mockImplementation(() => of(true));
        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => of(mockedPostsData));
        const spyOnUsersUpsertMany = jest.spyOn(postsStore, 'upsertMany').mockImplementation(() => { });
        const spyOnError = jest.spyOn(postsStore, 'setError');

        getPostService().getUserPosts(userId, skip, take);

        expect(http.get).toHaveBeenCalledWith(url);

    });

    it('Successful method getUserPosts() should should call postsStore.upsertMany', (done) => {
        const userId = '1';
        const skip = 0;
        const take = 5;
        const url = `${API_CONFIG.API_DOMAIN_NAME}/users/${userId}/posts`;
        const mockedPostsData = 'mockedPostData';

        const spyPostsStore = jest.spyOn(postsStore, 'setLoading').mockImplementation(() => of(true));
        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => of(mockedPostsData));
        const spyOnUsersUpsertMany = jest.spyOn(postsStore, 'upsertMany').mockImplementation(() => { });
        const spyOnError = jest.spyOn(postsStore, 'setError');

        getPostService().getUserPosts(userId, skip, take).subscribe(
            (success: boolean) => {
                expect(success).toBe(true);
                expect(postsStore.upsertMany).toHaveBeenCalledWith(mockedPostsData);
                done();

            });
    });

    it('Successful method getUserPosts() should should call postsStore.setLoading', (done) => {
        const userId = '1';
        const skip = 0;
        const take = 5;
        const url = `${API_CONFIG.API_DOMAIN_NAME}/users/${userId}/posts`;
        const mockedPostData = 'mockedPostData';

        const spyPostsStore = jest.spyOn(postsStore, 'setLoading').mockImplementation(() => of(false));
        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => of(mockedPostData));
        const spyPostsStoreSet = jest.spyOn(postsStore, 'setPostEnd').mockImplementation(() => of(mockedPostData));
        const spyPostsUpsertMany = jest.spyOn(postsStore, 'upsertMany').mockImplementation(() => { });

        const spyOnError = jest.spyOn(postsStore, 'setError');

        getPostService().getUserPosts(userId, skip, take).subscribe(() => {
            expect(postsStore.upsertMany).toHaveBeenCalledWith(mockedPostData);
            expect(postsStore.upsertMany).toHaveBeenCalledTimes(1);
            expect(postsStore.setLoading).toHaveBeenCalledWith(false);
            expect(postsStore.setLoading).toHaveBeenCalledTimes(1);
            done();
        },
            (newError) => {
                done();
            },
        );
    });

    it('Unsuccessful method getUserPosts() should notify', (done) => {
        const userId = '1';
        const skip = 0;
        const take = 5;
        const url = `${API_CONFIG.API_DOMAIN_NAME}/users/${userId}/posts`;
        const mockedPostsData = 'mockedPostData';
        const error = new Error('error');

        const spyPostsStore = jest.spyOn(postsStore, 'setLoading').mockImplementation(() => of(true));
        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => throwError(error.message));
        const spyOnUsersUpsertMany = jest.spyOn(postsStore, 'upsertMany').mockImplementation(() => { });
        const spyOnError = jest.spyOn(postsStore, 'setError');

        getPostService().getUserPosts(userId, skip, take).pipe(first()).subscribe(() => { },
            (newError) => {
                expect(postsStore.setError).toHaveBeenCalledWith(error.message);
                expect(postsStore.setError).toHaveBeenCalledTimes(1);
                done();
            },
        );
    });

    it('Unsuccessful method getUserPosts() should notify', (done) => {
        const userId = '1';
        const skip = 0;
        const take = 5;
        const url = `${API_CONFIG.API_DOMAIN_NAME}/users/${userId}/posts`;
        const mockedPostsData = 'mockedPostData';
        const error = new Error('error');

        const spyPostsStore = jest.spyOn(postsStore, 'setLoading').mockImplementation(() => of(true));
        const spyOnHttp = jest.spyOn(http, 'get').mockImplementation(() => throwError(error.message));
        const spyOnUsersUpsertMany = jest.spyOn(postsStore, 'upsertMany').mockImplementation(() => { });
        const spyOnError = jest.spyOn(postsStore, 'setError');

        getPostService().getUserPosts(userId, skip, take).pipe(first()).subscribe(() => { },
            (newError) => {
                expect(postsStore.upsertMany).toHaveBeenCalledTimes(0);
                done();
            },
        );
    });

    it('Successful method createPost() should should call http.post', () => {
        const userId = '1';
        const skip = 0;
        const take = 5;
        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_POSTS_PATH}`;
        const createPostData = {
            description: 'string',
            public: true,
            image: {
                size: 2,
                type: 'png',
                slice() { return new Blob(); },
            },
        };

        const mockedPostsData = new FormData();
        mockedPostsData.append('description', 'string');
        mockedPostsData.append('public', 'true');
        mockedPostsData.append('image', {
            size: 2,
            type: 'png',
            slice() { return new Blob(); },
        });

        const spyOnHttp = jest.spyOn(http, 'post').mockImplementation(() => of(mockedPostsData));
        const spyOnUsersUpsertMany = jest.spyOn(postsStore, 'upsertMany').mockImplementation(() => { });
        const spyOnError = jest.spyOn(postsStore, 'setError');

        getPostService().createPost(createPostData).pipe(delay(200)).subscribe(() => {
            expect(http.post).toHaveBeenCalledWith(url, mockedPostsData);
            expect(postsStore.upsertMany).toHaveBeenCalledTimes(1);

        });

    });


    it('Successful method createPost() should should call postsStore.add', () => {
        const userId = '1';
        const skip = 0;
        const take = 5;
        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_POSTS_PATH}`;
        const createPostData = {
            description: 'string',
            public: true,
            image: {
                size: 2,
                type: 'png',
                slice() { return new Blob(); },
            },
        };

        const mockedPostsData = new FormData();
        mockedPostsData.append('description', 'string');
        mockedPostsData.append('public', 'true');
        mockedPostsData.append('image', {
            size: 2,
            type: 'png',
            slice() { return new Blob(); },
        });

        const spyOnHttp = jest.spyOn(http, 'post').mockImplementation(() => of(mockedPostsData));
        const spyOnUsersUpsertMany = jest.spyOn(postsStore, 'add').mockImplementation(() => { });
        const spyOnError = jest.spyOn(postsStore, 'setError');

        getPostService().createPost(createPostData).subscribe(() => {
            expect(postsStore.add).toHaveBeenCalledWith(mockedPostsData);
        });

    });

    it('Successful method createPost() should should call postsStore.add should emit true', () => {
        const userId = '1';
        const skip = 0;
        const take = 5;
        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_POSTS_PATH}`;
        const createPostData = {
            description: 'string',
            public: true,
            image: {
                size: 2,
                type: 'png',
                slice() { return new Blob(); },
            },
        };

        const mockedPostsData = new FormData();
        mockedPostsData.append('description', 'string');
        mockedPostsData.append('public', 'true');
        mockedPostsData.append('image', {
            size: 2,
            type: 'png',
            slice() { return new Blob(); },
        });

        const spyOnHttp = jest.spyOn(http, 'post').mockImplementation(() => of(mockedPostsData));
        const spyOnUsersUpsertMany = jest.spyOn(postsStore, 'add').mockImplementation(() => { });
        const spyOnError = jest.spyOn(postsStore, 'setError');

        getPostService().createPost(createPostData).subscribe((success: boolean) => {
            expect(success).toBe(true);
        });

    });


    it('Successful method likePost() should should call http.post', () => {
        const postId = '2';

        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_POSTS_PATH}/${postId}/votes`;

        const mockedPostData = {
            id: '2',
            description: 'string',
            public: true,
            image: {
                size: 2,
                type: 'png',
                slice() { return new Blob(); },
            },
        };
        const spyOnHttp = jest.spyOn(http, 'post').mockImplementation(() => of(mockedPostData));
        const spyOnUsersUpsertMany = jest.spyOn(postsStore, 'upsert').mockImplementation(() => { });

        getPostService().likePost(postId);


        expect(http.post).toHaveBeenCalledWith(url, {});
        expect(http.post).toHaveBeenCalledTimes(1);

    });

    it('Successful method likePost() should should call postStore.upsert', () => {
        const postId = '2';

        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_POSTS_PATH}/${postId}/votes`;

        const mockedPostData = {
            id: '2',
            description: 'string',
            public: true,
            image: {
                size: 2,
                type: 'png',
                slice() { return new Blob(); },
            },
        };
        const spyOnHttp = jest.spyOn(http, 'post').mockImplementation(() => of(mockedPostData));
        const spyOnUsersUpsertMany = jest.spyOn(postsStore, 'upsert').mockImplementation(() => { });

        getPostService().likePost(postId);


        expect(postsStore.upsert).toHaveBeenCalledWith(mockedPostData.id, mockedPostData);
        expect(postsStore.upsert).toHaveBeenCalledTimes(1);

    });


    it('Successful method dislikePost() should should call http.delete', () => {
        const postId = '2';

        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_POSTS_PATH}/${postId}/votes`;

        const mockedPostData = {
            id: '2',
            description: 'string',
            public: true,
            image: {
                size: 2,
                type: 'png',
                slice() { return new Blob(); },
            },
        };
        const spyOnHttp = jest.spyOn(http, 'delete').mockImplementation(() => of(mockedPostData));
        const spyOnUsersUpsertMany = jest.spyOn(postsStore, 'upsert').mockImplementation(() => { });

        getPostService().dislikePost(postId);


        expect(http.delete).toHaveBeenCalledWith(url, {});
        expect(http.delete).toHaveBeenCalledTimes(1);

    });

    it('Successful method dislikePost() should should call postStore.upsert', () => {
        const postId = '2';

        const url = `${API_CONFIG.API_DOMAIN_NAME}${API_CONFIG.API_POSTS_PATH}/${postId}/votes`;

        const mockedPostData = {
            id: '2',
            description: 'string',
            public: true,
            image: {
                size: 2,
                type: 'png',
                slice() { return new Blob(); },
            },
        };
        const spyOnHttp = jest.spyOn(http, 'delete').mockImplementation(() => of(mockedPostData));
        const spyOnUsersUpsertMany = jest.spyOn(postsStore, 'upsert').mockImplementation(() => { });

        getPostService().dislikePost(postId);


        expect(postsStore.upsert).toHaveBeenCalledWith(mockedPostData.id, mockedPostData);
        expect(postsStore.upsert).toHaveBeenCalledTimes(1);

    });


    it('Successful method createComment() should should call http.post', () => {

        const mockedCommentToCreate = {

            postId: 'string',
            content: 'string',
        };

        const url = `${API_CONFIG.API_DOMAIN_NAME}/comments/${mockedCommentToCreate.postId}`;

        const mockedCommentData = {
            id: '2',
            description: 'string',
            public: true,
            image: {
                size: 2,
                type: 'png',
                slice() { return new Blob(); },
            },
        };
        const spyOnHttp = jest.spyOn(http, 'post').mockImplementation(() => of(mockedCommentData));

        getPostService().createComment(mockedCommentToCreate);

        expect(http.post).toHaveBeenCalledWith(url, mockedCommentToCreate.content);
        expect(http.post).toHaveBeenCalledTimes(1);

    });


    it('Successful method createComment() should should call postStore.upsert', () => {

        const mockedCommentToCreate = {

            postId: 'string',
            content: 'string',
        };

        const url = `${API_CONFIG.API_DOMAIN_NAME}/comments/${mockedCommentToCreate.postId}`;

        const mockedPostData = {
            id: '2',
            description: 'string',
            public: true,
            image: {
                size: 2,
                type: 'png',
                slice() { return new Blob(); },
            },
            comments: [mockedCommentToCreate],
        };
        const spyOnHttp = jest.spyOn(http, 'post').mockImplementation(() => of(mockedPostData));
        const spyOnUsersUpsertMany = jest.spyOn(postsStore, 'upsert').mockImplementation(() => { });

        getPostService().createComment(mockedCommentToCreate);

        expect(postsStore.upsert).toHaveBeenCalledWith(mockedPostData.id, { comments: mockedPostData.comments });
        expect(postsStore.upsert).toHaveBeenCalledTimes(1);

    });


    it('Unsuccessful response of method createComment() should should not call postStore.upsert', () => {

        const mockedCommentToCreate = {

            postId: 'string',
            content: 'string',
        };

        const url = `${API_CONFIG.API_DOMAIN_NAME}/comments/${mockedCommentToCreate.postId}`;

        const mockedPostData = {
            id: '2',
            description: 'string',
            public: true,
            image: {
                size: 2,
                type: 'png',
                slice() { return new Blob(); },
            },
            comments: [mockedCommentToCreate],
        };

        const spyOnHttp = jest.spyOn(http, 'post').mockImplementation(() => throwError('error'));
        const spyOnUsersUpsertMany = jest.spyOn(postsStore, 'upsert').mockImplementation(() => { });
        const spyOnSetError = jest.spyOn(postsStore, 'setError').mockImplementation(() => { });

        getPostService().createComment(mockedCommentToCreate);

        expect(postsStore.upsert).toHaveBeenCalledTimes(0);
        expect(postsStore.setError).toHaveBeenCalledWith('error');
        expect(postsStore.setError).toHaveBeenCalledTimes(1);

    });

    it('Successful method updateComment() should should call http.patch', (done) => {

        const mockedCommentToUpdate = {

            id: 'string',
            content: 'string',
        };

        const url = `${API_CONFIG.API_DOMAIN_NAME}/comments/${mockedCommentToUpdate.id}`;

        const mockedUpdatedComment = {
            id: '2',
            content: 'string',
            post: {
                id: '3',
            }

        };
        const spyOnHttp = jest.spyOn(http, 'patch').mockImplementation(() => of(mockedUpdatedComment));

        getPostService().updateComment(mockedCommentToUpdate);

        http.patch().subscribe(() => {

            expect(http.patch).toHaveBeenCalledWith(url, mockedCommentToUpdate);
            // expect(http.patch).toHaveBeenCalledTimes(1);
            // spyOnHttp.mockReset();
            done();
        });

    });

    it('Successful method updateComment() should should call postStore.update', (done) => {

        const mockedCommentToUpdate = {
            id: 'string',
            content: 'string',
        };

        const url = `${API_CONFIG.API_DOMAIN_NAME}/comments/${mockedCommentToUpdate.id}`;

        const mockedUpdatedComment = {
            id: '2',
            content: 'string',
            post: {
                id: '2',
            }
        };

        const mockedArrayUpdateData = undefined;

        const spyOnHttp = jest.spyOn(http, 'patch').mockImplementation(() => of(mockedUpdatedComment));
        const spyOnUpdate = jest.spyOn(postsStore, 'update').mockImplementation(() => { });

        getPostService().updateComment(mockedCommentToUpdate);
        http.patch().subscribe(() => {
            expect(postsStore.update).toHaveBeenCalledWith(mockedUpdatedComment.post.id,
                expect.any(Function),
            );
            expect(postsStore.update).toHaveBeenCalledTimes(1);
            done();
        });

    });

    it('Successful method removeComment() should should call http.delete', () => {
        const commentId = '2';

        const url = `${API_CONFIG.API_DOMAIN_NAME}/comments/${commentId}`;

        const mockedPostData = {};

        const spyOnHttp = jest.spyOn(http, 'delete').mockImplementation(() => of(mockedPostData));

        getPostService().removeComment(commentId);

        expect(http.delete).toHaveBeenCalledWith(url);
        expect(http.delete).toHaveBeenCalledTimes(1);

    });

    it('Successful method removeComment() should should call postStore.update', (done) => {

        const commentId = '2';

        const post = {
            id: '5',
        };

        const url = `${API_CONFIG.API_DOMAIN_NAME}/comments/${commentId}`;

        const spyOnHttp = jest.spyOn(http, 'delete').mockImplementation(() => of(post));
        const spyOnUpdate = jest.spyOn(postsStore, 'update').mockImplementation(() => { });

        getPostService().removeComment(commentId);
        http.delete().subscribe(() => {
            expect(postsStore.update).toHaveBeenCalledWith(post.id, post);
            expect(postsStore.update).toHaveBeenCalledTimes(1);
            done();
        });
    });




});

