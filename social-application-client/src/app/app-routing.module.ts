import { InternalServerErrorComponent } from './components/common/error/internal-server-error/internal-server-error.component';
import { NotFoundComponent } from './components/common/error/not-found/not-found.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule),
  },
  {
    path: 'explore',
    loadChildren: () =>
      import('./explore/explore.module').then(m => m.ExploreModule),
  },
  {
    path: 'dashboard',
    loadChildren: () =>
      import('./dashboard/dashboard.module').then(m => m.DashboardModule),
  },
  {
    path: 'search',
    loadChildren: () =>
      import('./search/search.module').then(m => m.SearchModule),
  },
  {
    path: 'users',
    loadChildren: () => import('./users/users.module').then(m => m.UsersModule),
  },
  {
    path: 'error',
    children: [
      {
        path: '404',
        component: NotFoundComponent,
      },
      {
        path: '500',
        component: InternalServerErrorComponent,
      },
    ],
  },
  { path: '', redirectTo: 'explore', pathMatch: 'full' },
  { path: '**', redirectTo: 'error/404' },
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule],
})
export class AppRoutingModule {}
