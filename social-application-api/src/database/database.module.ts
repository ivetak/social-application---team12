import { CONFIG_PATH } from './../common/constants/config.constants';
import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '../config';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
	imports: [
		TypeOrmModule.forRootAsync({
			inject: [ConfigService],
			useFactory: async (configService: ConfigService) => ({
				type: configService.dbType as any,
				host: configService.dbHost,
				port: configService.dbPort,
				username: configService.dbUsername,
				password: configService.dbPassword,
				database: configService.dbName,
				entities: [__dirname + '/**/*.entity{.ts,.js}'],
				synchronize: true,
				migrations: ['migration/*.js'],
				cli: {
					migrationsDir: 'migration',
				},
			}),
		}),
	],
})
export class DatabaseModule {}
