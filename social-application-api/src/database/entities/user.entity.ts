import { Notification } from './notification.entity';
import { Comment } from './comment.entity';
import { Post } from './post.entity';
import { Role } from './role.entity';
import { Entity, Column, ManyToMany, JoinTable, RelationCount, OneToMany } from 'typeorm';
import { BaseEntity } from './base.entity';
import { Vote } from './vote.entity';

@Entity('users')
export class User extends BaseEntity {
  @Column('nvarchar', { unique: true, nullable: false })
  public username: string;

  @Column('nvarchar', { unique: true, nullable: false })
  public email: string;

  @Column('nvarchar', { nullable: false })
  public password: string;

  @Column('nvarchar', { nullable: false, default: 'default_avatar' })
  public avatar: string;

  @Column({ type: 'boolean', default: false })
  public isDeleted: boolean;

  @OneToMany(type => Post, post => post.author)
  public posts: Post[];

  @RelationCount((user: User) => user.posts)
  postsCount: number;

  @ManyToMany(type => Role, { eager: true })
  @JoinTable({
    name: 'users_roles',
    joinColumn: { referencedColumnName: 'username', name: 'user' },
  })
  public roles: Role[];

  @ManyToMany(
    type => User,
    user => user.following,
  )
  @JoinTable()
  followers: User[];

  @ManyToMany(
    type => User,
    user => user.followers,
  )
  following: User[];

  @RelationCount((user: User) => user.followers)
  followersCount: number;

  @RelationCount((user: User) => user.following)
  followingCount: number;

  @OneToMany(type => Comment, comment => comment.author)
  public comments: Promise<Comment[]>;

  @RelationCount((user: User) => user.comments)
  public commentsCount: number;

  @OneToMany(type => Vote, vote => vote.votedBy)
  public postLikes: Promise<Vote[]>;

  @RelationCount((user: User) => user.postLikes)
  public postLikesCount: number;
}
