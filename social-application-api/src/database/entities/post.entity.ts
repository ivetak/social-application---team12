import { Comment } from './comment.entity';
import { User } from './user.entity';
import { Image } from './image.entity';
import { Entity, Column, OneToOne, JoinColumn, ManyToOne, OneToMany, RelationCount } from 'typeorm';
import { BaseEntity } from './';
import { Vote } from './vote.entity';

@Entity('posts')
export class Post extends BaseEntity {
  @Column()
  public description: string;

  @Column()
  public public: boolean;

  @OneToOne(
    type => Image,
    image => image.post,
    { eager: true },
  )
  @JoinColumn()
  public image: Image;

  @ManyToOne(
    type => User,
    user => user.posts,
    { eager: true },
  )
  public author: User;

  @OneToMany(type => Comment, comment => comment.post)
  public comments: Promise<Comment[]>;

  // // @RelationCount((post: Post) => post.comments)
  // public commentsCount: number;

  @OneToMany(type => Vote, vote => vote.post)
  public likes: Promise<Vote[]>;

  @RelationCount((post: Post) => post.likes)
  public likesCount: number;
}
