import { Entity, Column, ManyToOne, JoinColumn } from 'typeorm';
import { BaseEntity } from './base.entity';
import { User } from './user.entity';
import { Post } from './post.entity';

@Entity('comments')
export class Comment extends BaseEntity {
    @Column({ nullable: false, length: 1000 })
    public content: string;

    @ManyToOne(type => User, author => author.comments, { eager: true })
    @JoinColumn()
    public author: User;

    @ManyToOne(type => Post, post => post.comments)
    @JoinColumn()
    public post: Promise<Post>;

}