import { User } from './user.entity';
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { Post } from './post.entity';
import { BaseEntity } from './base.entity';

@Entity('votes')
export class Vote extends BaseEntity {

    @ManyToOne(type => User, user => user.postLikes, { eager: true })
    public votedBy: User;

    @ManyToOne(type => Post, post => post.likes)
    public post: Promise<Post>;
}
