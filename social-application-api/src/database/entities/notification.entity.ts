import { NotificationStatus } from './../../common/enums/notification-status.enum';
import { User } from './user.entity';
import { BaseEntity } from './base.entity';
import { ManyToOne, Column } from 'typeorm';
import { NotificationType } from '../../common/enums/notification-type.enum';

export abstract class Notification extends BaseEntity {
  @Column({ type: 'enum', enum: NotificationType })
  public type: NotificationType;

  @Column({
    type: 'enum',
    enum: NotificationStatus,
    default: NotificationStatus.UNREAD,
  })
  public status: NotificationStatus;

  @ManyToOne(type => User, { eager: true })
  public author: User;

  @ManyToOne(type => User, { eager: true })
  public notificationOwner: User;
}
