import { Post } from './post.entity';
import { Entity, Column, OneToOne, JoinColumn } from 'typeorm';
import { BaseEntity } from './';

@Entity('images')
export class Image extends BaseEntity {
  @Column()
  public datetime: number;

  @Column()
  public size: number;

  @Column({ nullable: false, default: '' })
  public deleteHash: string;

  @Column()
  public directLink: string;

  @OneToOne(
    type => Post,
    post => post.image,
  )
  public post: Post;
}
