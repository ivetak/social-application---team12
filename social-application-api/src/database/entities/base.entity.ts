import {
	PrimaryGeneratedColumn,
	CreateDateColumn,
	Column,
	UpdateDateColumn,
} from 'typeorm';

export abstract class BaseEntity {
	@PrimaryGeneratedColumn()
	id: string;

	@CreateDateColumn()
	createDate: Date;

	@UpdateDateColumn()
	lastChangedDate: Date;

	@Column({ type: 'boolean', default: false })
	isDeleted: boolean;
}
