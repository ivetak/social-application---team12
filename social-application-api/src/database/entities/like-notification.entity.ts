import { Post } from './post.entity';
import { Entity, ManyToOne } from 'typeorm';
import { Notification } from './notification.entity';

@Entity('like_notifications')
export class LikeNotification extends Notification {
  @ManyToOne(type => Post, { eager: true })
  public post: Post;
}
