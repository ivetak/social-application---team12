import { UserRole } from './../../common/enums/user-role.enum';
import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('roles')
export class Role {
	@PrimaryGeneratedColumn('increment')
	public id: string;

	@Column({
		type: 'enum',
		enum: UserRole,
		nullable: false,
		unique: true,
	})
	public name: UserRole;
}
