import { createConnection, Like } from 'typeorm';
import axios from 'axios';
import { User } from '../../entities/user.entity';
import { Image } from '../../entities/image.entity';
import { Post } from './../../entities/post.entity';

const main = async () => {
  let postAmount = 10;
  const profileArg = process.argv.slice(2).find(el => el.includes('--posts='));
  if (profileArg) {
    postAmount = Number(profileArg.split('=')[1]);
  }
  const connection = await createConnection();
  const userRepo = connection.getRepository(User);
  const postRepo = connection.getRepository(Post);
  const imageRepo = connection.getRepository(Image);

  const {
    data: { data },
  } = await axios.get<{ data: Array<{ link: string }> }>(
    'https://api.imgur.com/3/gallery/random/random',
    { headers: { Authorization: 'Client-ID 81c12001fb5b62f' } },
  );
  const imgurLinks = data
    .map(image => image.link)
    .filter(
      link =>
        link.includes('i.imgur') && !link.includes('.gif', link.length - 5),
    );

  const seededUsers: User[] = await userRepo.find({
    where: { avatar: Like('%api.adorable.io%') },
  });

  const posts: Array<Promise<Post>> = Array.from(Array(postAmount)).map(
    async (post: Post) => {
      const image = new Image();
      const link = imgurLinks[(Math.random() * imgurLinks.length).toFixed()];
      image.directLink = link;
      image.datetime = new Date().getMilliseconds();
      image.size = 128000;
      const savedImage = await imageRepo.save(image);

      post = new Post();
      post.description =
        'Spicy jalapeno bacon ipsum dolor amet ex do lorem dolor eu swine. Aliqua tri-tip incididunt nisi ball tip. Labore pork adipisicing, voluptate cupidatat in alcatra rump capicola doner sunt deserunt et occaecat meatloaf.';
      post.public = Math.random() < 0.5;
      post.image = image;
      post.author = seededUsers[(Math.random() * seededUsers.length).toFixed()];
      return post;
    },
  );

  await postRepo.save(await Promise.all(posts));

  await connection.close();

  console.log(`Data seeded successfully`);
};

main().catch(console.log);
