import { passwordHash } from './../../../common/constants/auth.constants';
import { createConnection } from 'typeorm';
import { Role } from '../../entities/role.entity';
import { User } from '../../entities/user.entity';
import { seedNames } from '../seed-names/names';

const generateUsername = () => {
  const ranName = Math.floor(Math.random() * seedNames.names.length);
  const ranAdj = Math.floor(Math.random() * seedNames.adjectives.length);
  const ranSuffix = Math.floor(Math.random() * 1000);
  return `${seedNames.adjectives[ranAdj]}-${seedNames.names[ranName]}${ranSuffix}`;
};

const main = async () => {
  let profileAmount = 20;
  const profileArg = process.argv
    .slice(2)
    .find(el => el.includes('--profiles='));
  if (profileArg) {
    profileAmount = Number(profileArg.split('=')[1]);
  }
  const connection = await createConnection();
  const roleRepo = connection.getRepository(Role);
  const userRepo = connection.getRepository(User);

  const userRole: Role = await roleRepo.findOne(2);

  const users: User[] = Array.from(Array(profileAmount)).map(_ => {
    const username = generateUsername();
    const email = `${generateUsername()}@mail.com`;
    const profile = new User();
    profile.username = username;
    profile.email = email;
    profile.password = passwordHash('User12345');
    profile.avatar = `https://api.adorable.io/avatars/${username}`;
    profile.roles = [userRole];
    // console.log(profile);
    return userRepo.create(profile);
  });
  await userRepo.save(users);
  // console.log(users);
  await connection.close();

  console.log(`Data seeded successfully`);
};

main().catch(console.log);
