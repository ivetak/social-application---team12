import { createConnection } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { Role } from '../../entities/role.entity';
import { User } from '../../entities/user.entity';
import { UserRole } from '../../../common/enums/user-role.enum';

const main = async () => {
  const connection = await createConnection();
  const roleRepo = connection.getRepository(Role);
  const userRepo = connection.getRepository(User);

  const userRole: Role = await roleRepo.findOne({ name: UserRole.User });
  const adminRole: Role = await roleRepo.findOne({ name: UserRole.Admin });

  const userEntity: User = await userRepo.findOne({ username: 'user' });
  const adminEntity: User = await userRepo.findOne({ username: 'admin' });

  let admin: Role;
  let user: Role;
  if (!adminRole) {
    admin = await roleRepo.save({
      name: UserRole.Admin,
    });
  } else {
    admin = adminRole;
  }
  if (!userRole) {
    user = await roleRepo.save({
      name: UserRole.User,
    });
  } else {
    user = userRole;
  }

  if (!adminEntity) {
    const firstAdmin = new User();
    (firstAdmin.username = 'admin'),
      (firstAdmin.email = 'admin@abv.bg'),
      (firstAdmin.password = await bcrypt.hash('Admin12345', 10));
    firstAdmin.roles = [admin];

    await userRepo.save(firstAdmin);
  }

  if (!userEntity) {
    const firstUser = new User();
    (firstUser.username = 'user'),
      (firstUser.email = 'user@abv.bg'),
      (firstUser.password = await bcrypt.hash('User12345', 10));
    firstUser.roles = [user];

    await userRepo.save(firstUser);
  }

  await connection.close();

  console.log(`Data seeded successfully`);
};

main().catch(console.log);
