import { NotificationDTO } from './../models/notifications/notification.dto';
import { plainToClass } from 'class-transformer';
import { NotificationsService } from './notifications.service';
import { AuthGuardWithBlacklisting } from './../common/guards/blacklist.guard';
import { UseGuards, Injectable } from '@nestjs/common';
import {
  WebSocketGateway,
  WebSocketServer,
  SubscribeMessage,
  OnGatewayConnection,
  OnGatewayDisconnect,
  MessageBody,
  ConnectedSocket,
  WsResponse,
} from '@nestjs/websockets';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { Socket } from 'socket.io';

// @Injectable()
// @UseGuards(AuthGuardWithBlacklisting)
@WebSocketGateway()
export class NotificationsGateway
  implements OnGatewayConnection, OnGatewayDisconnect {
  public pool = [];
  @WebSocketServer()
  server;

  constructor(private readonly notificationsService: NotificationsService) {}

  async handleConnection(@MessageBody() message, @ConnectedSocket() user) {
    this.pool = [...this.pool, user];
    return 'connected';
  }

  async handleDisconnect(user) {
    this.pool = [...this.pool, user];
  }

  @SubscribeMessage('readNotification')
  async onReadNotification(client: Socket, data: any) {
    const readNotification = await this.notificationsService.readNotification(
      data,
    );
    if (readNotification) {
      client.emit(
        'readNotification',
        plainToClass(
          NotificationDTO,
          { ...readNotification },
          { excludeExtraneousValues: true },
        ),
      );
    }
  }

  @SubscribeMessage('like')
  async onLike(client, post) {
    console.log('hit');
    client.send('liked');
    return 'liked';
  }

  @SubscribeMessage('comment')
  async onComment(client, comment) {
    client.broadcast.emit('comment', comment);
  }

  @SubscribeMessage('followed')
  async onFollowed(client, followedBy) {
    client.broadcast.emit('followed', followedBy);
  }

  @SubscribeMessage('post')
  async onPost(client, post) {
    client.broadcast.emit('post', post);
  }

  private validate(client: WebSocket, message: MessageEvent) {
    // try {
    //   const { token } = JSON.parse(message.data) as any;
    //   // const user = (this.jwt.verify(token) && this.jwt.decode(token));
    //   if (!user) {
    //     throw new Error(`Invalid credentials!`);
    //   }
    //   // this.socketService.add(user.id, client);
    //   // unregister the message event once the user is authenticated
    //   client.onmessage = () => { /* empty */ };
    // } catch (e) {
    //   // your error handling here
    // }
  }
}
