import { User } from './../database/entities/user.entity';
import { NotificationStatus } from './../common/enums/notification-status.enum';
import { plainToClass } from 'class-transformer';
import { LikeNotification } from './../database/entities/like-notification.entity';
import { NotificationDTO } from './../models/notifications/notification.dto';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class NotificationsService {
  constructor(
    @InjectRepository(LikeNotification)
    private readonly likeNotificationRepo: Repository<LikeNotification>,
    @InjectRepository(User)
    private readonly usersRepo: Repository<User>,
  ) {}

  public async getNotifications(
    username: string,
    skip: number = 0,
    take: number = 10,
  ): Promise<NotificationDTO[]> {
    const foundUser = await this.usersRepo.findOne({
      where: { username },
    });
    if (!foundUser) {
      throw new UnauthorizedException();
    }
    const foundLikeNotifications = await this.likeNotificationRepo.find({
      where: { notificationOwner: foundUser },
      order: { createDate: 'DESC', status: 'DESC' },
      skip,
      take,
    });

    return plainToClass(NotificationDTO, foundLikeNotifications, {
      excludeExtraneousValues: true,
      version: 0,
    });
  }

  public async readNotification(id: string) {
    const foundNotification = await this.likeNotificationRepo.findOne({
      where: { id },
    });
    if (foundNotification) {
      foundNotification.status = NotificationStatus.READ;
      const savedNotification = await this.likeNotificationRepo.save(
        foundNotification,
      );
      return savedNotification;
    }
  }
}
