import { NotificationsService } from './notifications.service';
import { ShowUserDTO } from './../models/users/show-user.dto';
import { AuthGuardWithBlacklisting } from './../common/guards/blacklist.guard';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { Controller, UseGuards, Get, Query } from '@nestjs/common';
import { userDecorator } from '../common/decorators/user.decorator';

@UseGuards(AuthGuardWithBlacklisting)
@ApiBearerAuth()
@ApiUseTags('notifications')
@Controller('notifications')
export class NotificationsController {
  constructor(private readonly notificationsService: NotificationsService) {}

  @UseGuards(AuthGuardWithBlacklisting)
  @Get('/')
  public async getNotifications(
    @userDecorator() user: ShowUserDTO,
    @Query('skip') skip: number = 0,
    @Query('take') take: number = 10,
  ) {
    return await this.notificationsService.getNotifications(
      user.username,
      skip,
      take,
    );
  }

  @UseGuards(AuthGuardWithBlacklisting)
  @Get('/likes')
  public async getLikeNotifications(
    @userDecorator() user: ShowUserDTO,
    @Query('skip') skip: number = 0,
    @Query('take') take: number = 10,
  ) {
    return await this.notificationsService.getNotifications(
      user.username,
      skip,
      take,
    );
  }
}
