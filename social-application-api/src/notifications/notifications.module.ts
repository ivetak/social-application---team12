import { LikeNotification } from './../database/entities/like-notification.entity';
import { User } from './../database/entities/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NotificationsController } from './notifications.controller';
import { NotificationsService } from './notifications.service';
import { NotificationsGateway } from './notifications.gateway';
import { Module } from '@nestjs/common';

@Module({
  imports: [TypeOrmModule.forFeature([User, LikeNotification])],
  controllers: [NotificationsController],
  providers: [NotificationsGateway, NotificationsService],
})
export class NotificationsModule {}
