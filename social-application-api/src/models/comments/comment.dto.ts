import { parseDate } from './../../common';
import { ShowUserDTO } from './../users/show-user.dto';
import { PostDTO } from './../post/post.dto';
import { Expose, Transform, plainToClass } from 'class-transformer';

export class CommentDTO {
  @Expose()
  public id: string;

  @Expose()
  public content: string;

  @Expose({ name: 'createDate' })
  @Transform(parseDate)
  public createdOn: string;

  @Expose({ name: 'lastChangedDate' })
  @Transform(parseDate)
  public updatedOn: string;

  @Expose({ name: '__author__' })
  @Transform((_, obj) =>
    plainToClass(
      ShowUserDTO,
      { ...obj.__author__ },
      {
        excludeExtraneousValues: true,
      },
    ),
  )
  public author: ShowUserDTO;

  @Expose({ name: '__post__' })
  @Transform((_, obj) =>
    plainToClass(
      PostDTO,
      { ...obj.__post__ },
      {
        excludeExtraneousValues: true,
      },
    ),
  )
  public post: PostDTO;
}
