import { ApiModelPropertyOptional } from '@nestjs/swagger';
import { IsString, IsOptional } from 'class-validator';

export class UpdateCommentDTO {
    @ApiModelPropertyOptional()
    @IsString()
    @IsOptional()
    public content: string;
}