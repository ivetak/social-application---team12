import { parseDate } from './../../common';
import { Expose, Transform, plainToClass } from 'class-transformer';
import { ShowUserDTO } from '../users/show-user.dto';

export class ShowCommentDTO {
  @Expose()
  public id: string;

  @Expose()
  public content: string;

  @Expose({ name: 'createDate' })
  @Transform(parseDate)
  public createdOn: string;

  @Expose({ name: 'lastChangedDate' })
  @Transform(parseDate)
  public updatedOn: string;

  @Expose({ name: 'author' })
  @Transform((_, obj) =>
    plainToClass(
      ShowUserDTO,
      { ...obj.author },
      {
        excludeExtraneousValues: true,
      },
    ),
  )
  public author: ShowUserDTO;
}
