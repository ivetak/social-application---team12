import { ApiModelProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty } from 'class-validator';

export class CreateCommentDTO {
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    public content: string;
}