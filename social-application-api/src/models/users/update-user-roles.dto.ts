import { IsArray } from 'class-validator';
import { UserRole } from '../../common/enums/user-role.enum';
import { ApiModelProperty } from '@nestjs/swagger';
import { Expose } from 'class-transformer';

export class UpdateUserRolesDTO {
    @Expose()
    @IsArray()
    public roles: UserRole[];
}