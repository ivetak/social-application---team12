import { ShowUserDTO } from './show-user.dto';
import { Expose, Transform, plainToClass } from 'class-transformer';
import { ApiModelProperty } from '@nestjs/swagger';

export class UserDTO {
  @ApiModelProperty()
  @Expose()
  public id: string;

  @ApiModelProperty()
  @Expose()
  public username: string;

  @ApiModelProperty()
  @Expose()
  public email: string;

  @ApiModelProperty()
  @Expose({ name: 'avatar' })
  public avatar: string;

  @ApiModelProperty()
  @Expose()
  public isDeleted: boolean;

  @ApiModelProperty()
  @Expose()
  public posts: [];

  @ApiModelProperty()
  @Expose()
  public postsCount: number;

  @ApiModelProperty()
  @Expose()
  @Transform((_, obj) =>
    obj.roles ? obj.roles.map((x: any) => x.roleName) : [],
  )
  public roles: string[];

  @ApiModelProperty()
  @Expose()
  @Transform((_, obj) =>
    plainToClass(UserDTO, obj.followers, {
      excludeExtraneousValues: true,
    }),
  )
  public followers: UserDTO[];

  @ApiModelProperty()
  @Expose()
  @Transform((_, obj) =>
    plainToClass(UserDTO, obj.following, {
      excludeExtraneousValues: true,
    }),
  )
  public following: UserDTO[];

  @ApiModelProperty()
  @Expose()
  followersCount: number;

  @ApiModelProperty()
  @Expose()
  followingCount: number;
}
