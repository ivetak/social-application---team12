import { ApiModelProperty } from '@nestjs/swagger';
import { Length, Matches, IsNotEmpty } from 'class-validator';

export class RegisterUserDTO {
  @ApiModelProperty({ required: true, example: 'newUser' })
  @Length(2, 20)
  @Matches(/[^' " / & @ #,]/, { message: 'No special characters allowed' })
  public username: string;

  @ApiModelProperty({ required: true, example: 'user@email.com' })
  @IsNotEmpty()
  @Matches(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/, {
    message: 'Must be a valid email',
  })
  public email: string;

  @ApiModelProperty({ required: true, example: 'newUserPassword1' })
  @Matches(/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)[A-Za-z\d!$%@#£€*?&]{8,}$/, {
    message:
      'The password must be minimum eight characters, at least one lowercase letter, one uppercase letter and one number',
  })
  public password: string;
}
