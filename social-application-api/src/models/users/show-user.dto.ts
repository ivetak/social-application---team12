import { Expose, Transform } from 'class-transformer';
import { ApiResponseModelProperty } from '@nestjs/swagger';
import { Role } from '../../database/entities';

export class ShowUserDTO {
  @ApiResponseModelProperty({ example: '666' })
  @Expose({ name: 'id' })
  public id: string;

  @ApiResponseModelProperty({ example: 'new-user' })
  @Expose({ name: 'username' })
  public username: string;

  @Expose({ name: 'avatar' })
  public avatar: string;

  @ApiResponseModelProperty({ example: ['User'] })
  @Expose({ name: 'roles' })
  @Transform((_, obj) =>
    obj.roles ? obj.roles.map((x: Role) => x.name) : [],
  )
  public roles: string[];
}
