import { ApiModelProperty } from '@nestjs/swagger';

import { IsString, IsNotEmpty, Length, Matches } from 'class-validator';

export class LoginUserDTO {
    @ApiModelProperty({ required: true, example: 'admin' })
    @IsString()
    @IsNotEmpty()
    @Length(2, 20)
    public username: string;

    @ApiModelProperty({ required: true, example: 'admin@email.com' })
    @Matches(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/, {
        message: 'Must be a valid email',
    })
    public email: string;

    @ApiModelProperty({ required: true, example: 'Admin12345' })
    @IsNotEmpty()
    @Matches(/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)[A-Za-z\d!$%@#£€*?&]{8,}$/, {
        message:
            'The password must be minimum eight characters, at least one lowercase letter, one uppercase letter and one number',
    })
    public password: string;
}