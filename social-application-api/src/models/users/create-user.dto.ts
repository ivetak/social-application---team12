import { ApiModelProperty } from '@nestjs/swagger';
import { Length, Matches, IsNotEmpty } from 'class-validator';
import { UserRole } from '../../common/enums/user-role.enum';

export class CreateUserDTO {
    @ApiModelProperty({ required: true, example: 'admin' })
    @Length(2, 20)
    public username: string;

    @ApiModelProperty({ required: true, example: 'user@email.com' })
    @IsNotEmpty()
    @Matches(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/, {
        message: 'Must be a valid email',
    })
    public email: string;

    @ApiModelProperty({ required: true, example: 'admin' })
    @Matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{5,}$/, {
        message:
            'The password must be minimum five characters, at least one letter and one number',
    })
    public password: string;

    @ApiModelProperty({
        required: false,
        type: UserRole,
        isArray: true,
        enum: UserRole,
        example: [UserRole.User, UserRole.Admin],
    })
    public roles: UserRole[];
}
