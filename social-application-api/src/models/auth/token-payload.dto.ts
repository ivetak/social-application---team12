import { Expose } from 'class-transformer';
import { UserRole } from '../../common/enums/user-role.enum';

export class TokenPayloadDTO {
    @Expose()
    public id: string;
    @Expose()
    public username: string;
    @Expose()
    public roles: UserRole[];
    @Expose()
    public iat: string;
    @Expose()
    public exp: string;
}