import { parseDate } from './../../common';
import { LikeNotification } from './../../database/entities/like-notification.entity';
import { PostDTO } from './../post/post.dto';
import { ShowUserDTO } from './../users/show-user.dto';
import { Notification } from './../../database/entities/notification.entity';
import { NotificationType } from '../../common/enums/notification-type.enum';
import { NotificationStatus } from '../../common/enums/notification-status.enum';
import { Expose, Transform, plainToClass } from 'class-transformer';
export class NotificationDTO {
  @Expose()
  public id: string;

  @Expose()
  public type: NotificationType;

  @Expose()
  public status: NotificationStatus;
  
  @Expose({name: 'createDate'})
  @Transform(parseDate)
  public createdOn: string;

  @Expose({name:'author'})
  @Transform((_, obj: Notification) =>
    plainToClass(ShowUserDTO, obj.author, { excludeExtraneousValues: true }),
  )
  public notificationAuthor: ShowUserDTO;

  @Expose({ until: 0.1 })
  @Transform((_, obj: LikeNotification) =>
    plainToClass(PostDTO, obj.post, { excludeExtraneousValues: true }),
  )
  public post: PostDTO;
}
