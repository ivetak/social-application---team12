import { Expose, Transform, plainToClass } from 'class-transformer';
import { ShowUserDTO } from '../users';

export class VoteDTO {
    @Expose()
    public id: string;

    @Expose({ name: 'votedBy' })
    @Transform((_, obj) =>
        plainToClass(
            ShowUserDTO,
            { ...obj.votedBy },
            { excludeExtraneousValues: true },
        ),
    )
    public votedBy: ShowUserDTO;
}
