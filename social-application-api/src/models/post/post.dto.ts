import { VoteDTO } from './vote.dto';
import { CommentDTO } from './../comments/comment.dto';
import { Comment } from './../../database/entities/comment.entity';
import { parseDate } from './../../common';
import { ShowImageDTO } from './../image/show-image.dto';
import { ShowUserDTO } from './../users/show-user.dto';
import { Post } from './../../database/entities/post.entity';
import { ImageModel } from '../image';
import { Expose, Transform, plainToClass } from 'class-transformer';
import { ShowCommentDTO } from '../comments/show-comment.dto';
import { ApiModelProperty } from '@nestjs/swagger';

export class PostDTO {
  @Expose()
  public id: string;

  @Expose({ name: 'createDate' })
  @Transform(parseDate)
  public createdOn: string;

  @Expose({ name: 'lastChangedDate' })
  @Transform(parseDate)
  public updatedOn: string;

  @Expose()
  public isDeleted: boolean;

  @Expose()
  public description: string;

  @Expose()
  public public: boolean;

  @Expose({ name: 'image' })
  @Transform((_, obj: Post) =>
    plainToClass(ShowImageDTO, obj.image, { excludeExtraneousValues: true }),
  )
  public image: ImageModel;

  @Expose({ name: 'author' })
  @Transform((_, obj) =>
    plainToClass(ShowUserDTO, obj.author, { excludeExtraneousValues: true }),
  )
  public author: ShowUserDTO;

  @Expose({ name: '__comments__' })
  @Transform((_, obj) => {
    const comments = obj.__comments__
      ? obj.__comments__.filter((comment: Comment) => !comment.isDeleted)
      : obj.__comments__;

    return plainToClass(ShowCommentDTO, comments, {
      excludeExtraneousValues: true,
    });
  })
  public comments: ShowCommentDTO[];

  @ApiModelProperty()
  @Expose()
  @Transform((_, obj) => {
    const comments = obj.__comments__
      ? obj.__comments__.filter((comment: Comment) => !comment.isDeleted).length
      : 0;

    return plainToClass(ShowCommentDTO, comments, {
      excludeExtraneousValues: true,
    });
  })
  public commentsCount: number;

  @Expose({ name: '__likes__' })
  @Transform((_, obj) =>
    plainToClass(VoteDTO, obj.__likes__, {
      excludeExtraneousValues: true,
    }),
  )
  public likes: VoteDTO[];

  @ApiModelProperty()
  @Expose()
  public likesCount: number;
}
