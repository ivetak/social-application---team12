import { Expose } from 'class-transformer';

export class ShowImageDTO {
  @Expose()
  public id: string;

  @Expose()
  public directLink: string;

  @Expose()
  public datetime: number;

  @Expose()
  public size: number;
}
