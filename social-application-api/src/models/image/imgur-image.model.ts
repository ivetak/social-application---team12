export class ImgurImageResponse {
	public id: string;

	public datetime: number;

	public mimeType: string;

	public width: number;

	public height: number;

	public size: number;

	public deleteHash: string;

	public directLink: string;
}
