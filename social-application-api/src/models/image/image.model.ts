import { Expose } from 'class-transformer';
export class ImageModel {
  @Expose()
  public id: string;

  @Expose({ name: 'link' })
  public directLink: string;

  @Expose()
  public datetime: number;

  @Expose()
  public size: number;

  @Expose({ name: 'deletehash' })
  public deleteHash: string;
}
