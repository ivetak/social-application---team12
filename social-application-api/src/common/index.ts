import * as moment from 'moment';

export const parseDate = (date: string | Date) => {
  return moment(date)
    .utc()
    .unix();
};
