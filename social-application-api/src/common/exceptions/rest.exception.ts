import { HttpException, HttpStatus } from '@nestjs/common';
import { RestExceptionResponse } from '../interfaces/exception';

export class RestException extends HttpException {
	constructor(
		response: RestExceptionResponse,
		status: number = HttpStatus.BAD_REQUEST,
	) {
		super(response, status);
	}
}
