export const CONFIG_OPTIONS = 'CONFIG_OPTIONS';
export const ENV_CONFIG = 'ENV_CONFIG';
export const CONFIG_PATH = './.env';

export const NODE_ENV_DEVELOPMENT = 'development';
export const NODE_ENV_PRODUCTION = 'production';
export const NODE_ENV_TEST = 'test';
export const NODE_ENV_PROVISION = 'provision';
