export const IMGUR_AUTHORIZATION_HEADER = {
  Authorization: `Client-ID `,
  'Content-Type': 'multipart/form-data',
};
export const IMGUR_DELETE_IMAGE_PATH = 'image/';
export const IMGUR_UPLOAD_IMAGE_PATH = 'upload/';
