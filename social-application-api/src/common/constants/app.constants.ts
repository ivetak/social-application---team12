export const API_TITLE = 'Social Application';
export const API_DESCRIPTION = 'The API Documentation';
export const API_VERSION = '1.0';
export const API_MAIN_TAG = 'SocialApp';
export const API_PATH = 'swag-api';
