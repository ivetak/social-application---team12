import * as bcrypt from 'bcrypt';

const SALT_ROUNDS = 10;

export const passwordHash = (password: string): string => {
    return bcrypt.hashSync(password, SALT_ROUNDS);
};

export const passwordCompare = async (
    password: string,
    hashPassword: string,
): Promise<boolean> => {
    return await bcrypt.compare(password, hashPassword);
};