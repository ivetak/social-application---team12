import {
	PipeTransform,
	Injectable,
	ArgumentMetadata,
	HttpStatus,
	Type,
} from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import { RestException } from '../exceptions';

@Injectable()
export class ImgurImageResponsePipe implements PipeTransform {
	async transform(value: any, { metatype }: ArgumentMetadata) {
		if (!metatype || !this.toValidate(metatype)) {
			return value;
		}
		const image = plainToClass(metatype, value);
		const errors = await validate(image);
		if (errors.length > 0) {
			throw new RestException(
				{
					error: 'Validation failed',
					message: `Failed to validate ${value}. Following problem/s occured: ${errors}`,
					condition: 400,
				},
				HttpStatus.BAD_REQUEST,
			);
		}
		return value;
	}

	private toValidate(metatype: Type<any>): boolean {
		const types: any[] = [String, Boolean, Number, Array, Object];
		return !types.includes(metatype);
	}
}
