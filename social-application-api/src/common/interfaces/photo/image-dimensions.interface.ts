export type ThumbnailType = 's' | 'b' | 't' | 'm' | 'l' | 'h';

export interface ImageDimensions {
  square: string;
  bigSquare: string;
}
