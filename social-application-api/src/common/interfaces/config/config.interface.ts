export interface Config {
	appRootPath: string;
	isProduction: boolean;
	session: {
		domain: string;
		secret: string;
		timeout: number;
		refresh: {
			secret: string;
			timeout: number;
		};
	};
}
