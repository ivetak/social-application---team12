import { ApiResponseModelProperty } from "@nestjs/swagger";

export interface RestExceptionResponse {
	error: string;
	message: string;
	condition: number;
}
