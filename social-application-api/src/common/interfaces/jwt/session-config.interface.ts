import { RefreshConfig } from './refresh-config.interface';
export interface SessionConfig {
	domain: string;
	secret: string;
	timeout: number;
	refresh: RefreshConfig;
}
