export interface RefreshConfig {
	secret: string;
	timeout: number;
}
