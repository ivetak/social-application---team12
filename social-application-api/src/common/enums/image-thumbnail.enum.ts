export enum ImageThumbnail {
	's' = 0,
	'b' = 1,
	't' = 2,
	'm' = 3,
	'l' = 4,
	'h' = 5,
}
