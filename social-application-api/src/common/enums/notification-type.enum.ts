export enum NotificationType {
  LIKE = 'like',
  POST = 'post',
  COMMENT = 'comment',
  FOLLOW = 'follow',
}
