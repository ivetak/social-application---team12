export enum UserEntityRelations {
    followers = 'followers',
    following = 'following',
}
