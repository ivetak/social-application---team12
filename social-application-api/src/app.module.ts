import { CoreModule } from './core';
import { Module } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { PostModule } from './post/post.module';
import { CommentsModule } from './comments/comments.module';

@Module({
	imports: [
		CoreModule,
		UsersModule,
		PostModule,
		CommentsModule,
	],
})
export class AppModule { }
