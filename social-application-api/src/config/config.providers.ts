import { ENV_CONFIG } from './../common/constants/config.constants';
import { Config } from '../common/interfaces/config';

export const config: Config = {
	appRootPath: `${__dirname}/../`,
	isProduction: process.env.NODE_ENV === 'production',
	session: {
		domain: 'domain',
		secret: process.env.TOKEN_SECRET_KEY,
		timeout: parseInt(process.env.TOKEN_TIMEOUT, 10),
		refresh: {
			secret: process.env.REFRESH_SECRET_KEY,
			timeout: parseInt(process.env.REFRESH_TIMEOUT, 10),
		},
	},
};

export const configProviders = [
	{
		provide: ENV_CONFIG,
		useValue: config,
	},
];
