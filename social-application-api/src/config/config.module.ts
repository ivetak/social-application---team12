import { Module, DynamicModule } from '@nestjs/common';
import { ConfigService } from './config.service';
import { ConfigOptions } from '../common/interfaces/config';
import { CONFIG_OPTIONS } from '../common/constants/config.constants';
import { configProviders } from './config.providers';

@Module({})
export class ConfigModule {
	static register(options?: ConfigOptions): DynamicModule {
		return {
			module: ConfigModule,
			providers: [
				{
					provide: CONFIG_OPTIONS,
					useValue: options,
				},
				ConfigService,
				...configProviders,
			],
			exports: [ConfigService],
		};
	}
}
