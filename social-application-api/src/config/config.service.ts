import * as Joi from '@hapi/joi';
import * as dotenv from 'dotenv';
import { Injectable, Inject } from '@nestjs/common';
import { DatabaseType } from 'typeorm';
import { ConfigOptions, EnvConfig } from '../common/interfaces/config';
import {
  CONFIG_OPTIONS,
  NODE_ENV_DEVELOPMENT,
  NODE_ENV_PRODUCTION,
  NODE_ENV_TEST,
  NODE_ENV_PROVISION,
} from '../common/constants/config.constants';

@Injectable()
export class ConfigService {
  private readonly envConfig: EnvConfig;

  public constructor(@Inject(CONFIG_OPTIONS) private options: ConfigOptions) {
    const config = dotenv.config({ path: this.options.folder }).parsed;
    this.envConfig = this.validateInput(config);
  }

  private validateInput(envConfig: EnvConfig): EnvConfig {
    const envVarsSchema: Joi.ObjectSchema = Joi.object({
      NODE_ENV: Joi.string()
        .valid(
          NODE_ENV_DEVELOPMENT,
          NODE_ENV_PRODUCTION,
          NODE_ENV_TEST,
          NODE_ENV_PROVISION,
        )
        .default(NODE_ENV_DEVELOPMENT),
      APP_PORT: Joi.number().default(3000),
      DB_TYPE: Joi.string(),
      DB_HOST: Joi.string(),
      DB_PORT: Joi.number(),
      DB_USERNAME: Joi.string(),
      DB_PASSWORD: Joi.string(),
      DB_DATABASE_NAME: Joi.string(),
      TOKEN_SECRET_KEY: Joi.string(),
      REFRESH_TOKEN_SECRET_KEY: Joi.string(),
      TOKEN_TIMEOUT: Joi.string(),
      REFRESH_TIMEOUT: Joi.string(),
      IMGUR_API_ROUTE: Joi.string(),
      IMGUR_API_CLIENT_ID: Joi.string(),
    });

    const { error, value: validatedEnvConfig } = envVarsSchema.validate(
      envConfig,
    );

    if (error) {
      throw new Error(`Config validation error: ${error.message}`);
    }

    return validatedEnvConfig;
  }

  public get port(): number {
    return +this.envConfig.APP_PORT;
  }

  public get dbHost(): string {
    return this.envConfig.DB_HOST;
  }

  public get dbPort(): number {
    return +this.envConfig.DB_PORT;
  }

  public get dbUsername(): string {
    return this.envConfig.DB_USERNAME;
  }

  public get dbPassword(): string {
    return this.envConfig.DB_PASSWORD;
  }

  public get dbName(): string {
    return this.envConfig.DB_DATABASE_NAME;
  }

  public get dbType(): DatabaseType {
    return this.envConfig.DB_TYPE as DatabaseType;
  }

  public get tokenSecret(): string {
    return this.envConfig.TOKEN_SECRET_KEY;
  }

  public get refreshTokenSecret(): string {
    return this.envConfig.REFRESH_TOKEN_SECRET_KEY;
  }

  public get tokenTimeout(): number {
    return +this.envConfig.TOKEN_TIMEOUT;
  }

  public get refreshTimeout(): number {
    return +this.envConfig.REFRESH_TIMEOUT;
  }

  public get ImgurAPIRoute(): string {
    return this.envConfig.IMGUR_API_ROUTE;
  }

  public get ImgurAPIClientId(): string {
    return this.envConfig.IMGUR_API_CLIENT_ID;
  }
}
