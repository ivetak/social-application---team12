import { ImgurService } from './imgur.service';
import { CoreModule } from './../core/core.module';
import { Module } from '@nestjs/common';

@Module({
  imports: [],
  providers: [ImgurService],
  exports: [ImgurService],
})
export class ImgurModule {}
