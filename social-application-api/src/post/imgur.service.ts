import {
  IMGUR_DELETE_IMAGE_PATH,
  IMGUR_UPLOAD_IMAGE_PATH,
  IMGUR_AUTHORIZATION_HEADER,
} from '../common/constants/imgur.constants';
import { ConfigService } from '../config/config.service';
import { Injectable, HttpService } from '@nestjs/common';
import { ImageModel, ImgurImageResponse } from '../models/image';
import { plainToClass } from 'class-transformer';

@Injectable()
export class ImgurService {
  private readonly headers;

  constructor(
    private readonly http: HttpService,
    private readonly config: ConfigService,
  ) {
    const auth = IMGUR_AUTHORIZATION_HEADER;
    auth.Authorization += this.config.ImgurAPIClientId;
    this.headers = {
      ...auth,
    };
  }

  public async deleteImage(hash: string): Promise<void> {
    const successful = await this.http
      .delete<{ success: boolean }>(
        `${this.config.ImgurAPIRoute}${IMGUR_DELETE_IMAGE_PATH}${hash}`,
      )
      .toPromise();
  }

  public async uploadImage(image): Promise<ImageModel> {
    const {
      data: { data: innerData },
    } = await this.http
      .post<any>(
        `${this.config.ImgurAPIRoute}${IMGUR_UPLOAD_IMAGE_PATH}`,
        image.buffer,
        { headers: this.headers },
      )
      .toPromise();
    return plainToClass(ImageModel, innerData, {
      excludeExtraneousValues: true,
    });
  }
}
