import { UsersService } from './../users/users.service';
import { LikeNotification } from './../database/entities/like-notification.entity';
import { NotificationType } from './../common/enums/notification-type.enum';
import { NotificationStatus } from '../common/enums/notification-status.enum';
import { Notification } from './../database/entities/notification.entity';
import { PostDTO } from './../models/post/post.dto';
import { Vote } from './../database/entities/vote.entity';
import { RestException } from './../common/exceptions/rest.exception';
import { CreatePostDTO } from './../models/post';
import { Injectable, HttpStatus, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, In } from 'typeorm';
import { Post } from '../database/entities/post.entity';
import { Image } from '../database/entities/image.entity';
import { User } from '../database/entities/user.entity';
import { plainToClass } from 'class-transformer';
import * as moment from 'moment';

@Injectable()
export class PostService {
  constructor(
    private readonly usersService: UsersService,
    @InjectRepository(Image) private readonly imageRepo: Repository<Image>,
    @InjectRepository(Post) private readonly postRepo: Repository<Post>,
    @InjectRepository(User) private readonly userRepo: Repository<User>,
    @InjectRepository(Vote) private readonly voteRepo: Repository<Vote>,
    @InjectRepository(LikeNotification)
    private readonly likeNotificationRepo: Repository<LikeNotification>,
  ) {}

  public async getPublicPosts(
    skip: number = 0,
    take: number = 10,
  ): Promise<PostDTO[]> {
    const foundPosts = await this.postRepo.find({
      where: { public: true },
      order: { createDate: 'DESC' },
      skip,
      take,
      relations: [
        'author',
        'comments',
        'comments.author',
        'likes',
        'likes.votedBy',
      ],
    });
    return plainToClass(PostDTO, foundPosts, { excludeExtraneousValues: true });
  }

  public async getDashboardPosts(
    username: string,
    skip: number = 0,
    take: number = 10,
  ): Promise<PostDTO[]> {
    const foundUser = await this.userRepo.findOne({
      where: { username },
      relations: ['following'],
    });
    if (!foundUser) {
      throw new UnauthorizedException();
    }
    const foundPosts = await this.postRepo.find({
      where: {
        author: {
          id: In([
            ...foundUser.following.map((followedUser: User) => followedUser.id),
            foundUser.id,
          ]),
        },
      },
      order: { createDate: 'DESC' },
      skip,
      take,
      relations: [
        'author',
        'comments',
        'comments.author',
        'likes',
        'likes.votedBy',
      ],
    });

    return plainToClass(PostDTO, foundPosts, { excludeExtraneousValues: true });
  }

  public async getPostById(id: string): Promise<PostDTO> {
    const foundPost: Post = await this.postRepo.findOne({
      where: { id, isDeleted: false },
      relations: [
        'author',
        'comments',
        'comments.author',
        'likes',
        'likes.votedBy',
      ],
    });
    if (!foundPost) {
      throw new RestException(
        {
          error: `Not found`,
          message: `No post with id:${id} found`,
          condition: 404,
        },
        404,
      );
    }
    return plainToClass(PostDTO, foundPost, { excludeExtraneousValues: true });
  }

  public async createPost(
    post: CreatePostDTO,
    userIdentifier: string,
  ): Promise<PostDTO> {
    const foundUser = await this.usersService.findUserEntity(userIdentifier);
    if (!foundUser) {
      throw new RestException(
        {
          error: 'Bad request',
          message: 'You are not allowed to do this',
          condition: 400,
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    const createdPost = this.postRepo.create({
      description: post.description,
      public: JSON.parse(post.public.toString()),
    });
    const { image } = post;
    const createdImage = this.imageRepo.create({
      datetime: image.datetime,
      size: image.size,
      deleteHash: image.deleteHash,
      directLink: image.directLink,
    });
    const savedImage = await this.imageRepo.save(createdImage);
    createdPost.image = savedImage;
    createdPost.author = foundUser;
    const savedPost = await this.postRepo.save(createdPost);

    return plainToClass(PostDTO, savedPost, { excludeExtraneousValues: true });
  }

  async deletePost(username: string, postId: string): Promise<PostDTO> {
    const foundUser = await this.userRepo.findOne({ where: { username } });
    if (!foundUser) {
      throw new UnauthorizedException();
    }
    const foundPost = await this.postRepo.findOne({ where: { id: postId } });
    foundPost.isDeleted = true;
    const deletedPost = await this.postRepo.save(foundPost);

    return plainToClass(PostDTO, deletedPost, {
      excludeExtraneousValues: true,
    });
  }

  async vote(postId: string, username: string): Promise<PostDTO> {
    const foundUser: User = await this.userRepo.findOne({ username });
    const foundPost: Post = await this.postRepo.findOne({
      where: { id: postId },
      relations: ['likes', 'likes.votedBy'],
    });

    if (!foundUser) {
      throw new UnauthorizedException();
    }
    if (!foundPost) {
      throw new RestException(
        {
          error: 'Bad request',
          message: 'Post not found',
          condition: 400,
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    const likeByUser = (await foundPost.likes).find(
      (like: Vote) => like.votedBy.id === foundUser.id,
    );

    let updatedPost: Post;

    if (likeByUser) {
      updatedPost = await this.postRepo.findOne({
        where: { id: postId },
        relations: [
          'author',
          'comments',
          'comments.author',
          'likes',
          'likes.votedBy',
        ],
      });

      return plainToClass(PostDTO, updatedPost, {
        excludeExtraneousValues: true,
      });
    }

    const createdLike = this.voteRepo.create();

    createdLike.votedBy = foundUser;
    createdLike.post = Promise.resolve(foundPost);

    const savedLike = await this.voteRepo.save(createdLike);

    updatedPost = await this.postRepo.findOne({
      where: { id: postId },
      relations: [
        'author',
        'comments',
        'comments.author',
        'likes',
        'likes.votedBy',
      ],
    });
    const foundNotification = await this.likeNotificationRepo.findOne({
      where: { post: { id: foundPost.id }, isDeleted: false },
    });
    if (
      updatedPost.author.username !== foundUser.username &&
      !foundNotification
    ) {
      const likeNotification = await this.likeNotificationRepo.save({
        type: NotificationType.LIKE,
        author: foundUser,
        notificationOwner: updatedPost.author,
        post: updatedPost,
      });
    }

    return plainToClass(PostDTO, updatedPost, {
      excludeExtraneousValues: true,
    });
  }

  public async deleteLike(postId: string, username: string) {
    const foundUser: User = await this.userRepo.findOne({ username });
    const foundPost: Post = await this.postRepo.findOne({
      where: { id: postId },
      relations: ['likes', 'likes.votedBy'],
    });
    if (!foundUser) {
      throw new UnauthorizedException();
    }
    if (!foundPost) {
      throw new RestException(
        {
          error: 'Bad request',
          message: 'Post not found',
          condition: 400,
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    const likeByUser = (await foundPost.likes).find(
      (like: Vote) => like.votedBy.id === foundUser.id,
    );

    let updatedPost: Post;

    if (likeByUser) {
      const deletedLike = await this.voteRepo.remove(likeByUser);

      const foundLikeNotification = await this.likeNotificationRepo.findOne({
        where: { post: { id: foundPost.id, isDeleted: false } },
      });

      if (
        foundLikeNotification &&
        moment()
          .hours(-24)
          .isAfter(foundLikeNotification.createDate)
      ) {
        foundLikeNotification.isDeleted = true;
        const deletedNotification = await this.likeNotificationRepo.save(
          foundLikeNotification,
        );
      }

      updatedPost = await this.postRepo.findOne({
        where: { id: postId },
        relations: [
          'author',
          'comments',
          'comments.author',
          'likes',
          'likes.votedBy',
        ],
      });

      return plainToClass(PostDTO, updatedPost, {
        excludeExtraneousValues: true,
      });
    }

    updatedPost = await this.postRepo.findOne({
      where: { id: postId },
      relations: [
        'author',
        'comments',
        'comments.author',
        'likes',
        'likes.votedBy',
      ],
    });

    return plainToClass(PostDTO, updatedPost, {
      excludeExtraneousValues: true,
    });
  }

  private async getUserById(
    id: string,
    relations: string[] = [],
  ): Promise<User> {
    return await this.userRepo.findOne({ where: id, relations });
  }
}
