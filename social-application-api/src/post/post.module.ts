import { UsersModule } from './../users/users.module';
import { LikeNotification } from './../database/entities/like-notification.entity';
import { ImgurModule } from './imgur.module';
import { Vote } from './../database/entities/vote.entity';
import { Module } from '@nestjs/common';
import { PostService } from './post.service';
import { PostController } from './post.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Image } from '../database/entities/image.entity';
import { Post } from '../database/entities/post.entity';
import { User } from '../database/entities/user.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Post, Image, User, Vote, LikeNotification]),
    ImgurModule,
    UsersModule,
  ],
  controllers: [PostController],
  providers: [PostService],
  exports: [PostService],
})
export class PostModule { }
