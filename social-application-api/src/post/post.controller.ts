import { PostDTO } from './../models/post/post.dto';
import { ShowUserDTO } from './../models/users/show-user.dto';
import { AuthGuardWithBlacklisting } from './../common/guards/blacklist.guard';
import { ApiUseTags, ApiConsumes, ApiBearerAuth } from '@nestjs/swagger';
import { plainToClass } from 'class-transformer';
import { PostService } from './post.service';
import { ImgurService } from './imgur.service';
import {
  Controller,
  Get,
  Param,
  Post,
  UseInterceptors,
  UploadedFile,
  Body,
  ValidationPipe,
  UseGuards,
  Query,
  Put,
  Request,
  Delete,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiImplicitFormData, Token } from '../common/decorators';
import { CreatePostDTO } from '../models/post';
import { userDecorator } from '../common/decorators/user.decorator';

@ApiBearerAuth()
@ApiUseTags('posts')
@Controller('posts')
export class PostController {
  constructor(
    private readonly imgur: ImgurService,
    private readonly postService: PostService,
  ) {}

  @Get('/')
  public async getPosts(
    @Query('skip') skip: number = 0,
    @Query('take') take: number = 10,
  ): Promise<PostDTO[]> {
    return await this.postService.getPublicPosts(skip, take);
  }

  @UseGuards(AuthGuardWithBlacklisting)
  @Get('dashboard')
  public async getDashboardPosts(
    @Query('skip') skip: number = 0,
    @Query('take') take: number = 10,
    @userDecorator() user: ShowUserDTO,
  ): Promise<PostDTO[]> {
    return await this.postService.getDashboardPosts(user.username, skip, take);
  }

  @Get(':id')
  public async getPostById(@Param('id') postId: string): Promise<PostDTO> {
    return await this.postService.getPostById(postId);
  }

  @Post()
  @ApiConsumes('multipart/form-data')
  @ApiImplicitFormData({ name: 'image', required: true, type: 'file' })
  @ApiImplicitFormData({ name: 'public', required: true, type: 'boolean' })
  @ApiImplicitFormData({ name: 'description', required: true, type: 'string' })
  @UseInterceptors(FileInterceptor('image'))
  @ApiBearerAuth()
  @UseGuards(AuthGuardWithBlacklisting)
  public async createPost(
    @UploadedFile() image,
    @Body(new ValidationPipe({ transform: true }))
    post: CreatePostDTO,
    @userDecorator() user: ShowUserDTO,
  ): Promise<PostDTO> {
    const uploadedImage = await this.imgur.uploadImage(image);
    const createdPost = await this.postService.createPost(
      {
        ...post,
        image: uploadedImage,
      },
      user.id,
    );
    return plainToClass(PostDTO, createdPost);
  }

  @UseGuards(AuthGuardWithBlacklisting)
  @Delete(':postId')
  public async deletePost(
    @Param('postId') postId: string,
    @userDecorator() user: ShowUserDTO,
  ): Promise<PostDTO> {
    return await this.postService.deletePost(user.username, postId);
  }

  @UseGuards(AuthGuardWithBlacklisting)
  @Post(':postId/votes')
  async votePost(
    @Param('postId') postId: string,
    @userDecorator('user') user: ShowUserDTO,
  ): Promise<PostDTO> {
    return await this.postService.vote(postId, user.username);
  }

  @UseGuards(AuthGuardWithBlacklisting)
  @Delete(':postId/votes')
  async deleteVote(
    @Param('postId') postId: string,
    @userDecorator('user') user: ShowUserDTO,
  ): Promise<PostDTO> {
    return await this.postService.deleteLike(postId, user.username);
  }
}
