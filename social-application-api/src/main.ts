import {
	API_TITLE,
	API_DESCRIPTION,
	API_VERSION,
	API_MAIN_TAG,
	API_PATH,
} from './common/constants/app.constants';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import * as helmet from 'helmet';
import { ConfigService } from './config';
import { HttpExceptionFilter } from './common/filters';
import * as bodyParser from 'body-parser';

async function bootstrap() {
	const app = await NestFactory.create(AppModule);
	const configService: ConfigService = app.get(ConfigService);
	const options = new DocumentBuilder()
		.setTitle(API_TITLE)
		.setDescription(API_DESCRIPTION)
		.setVersion(API_VERSION)
		.addTag(API_MAIN_TAG)
		.addBearerAuth()
		.build();
	const document = SwaggerModule.createDocument(app, options);
	SwaggerModule.setup(API_PATH, app, document);

	app.use(bodyParser.json({limit: '20mb'}));
	app.use(bodyParser.urlencoded({limit: '20mb', extended: true}));
	app.enableCors();
	app.use(helmet());
	app.useGlobalFilters(new HttpExceptionFilter());
	await app.listen(process.env.PORT || configService.port);
}
bootstrap();
