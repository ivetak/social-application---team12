import { UsersService } from './../users/users.service';
import { AuthController } from "./auth.controller";
import { TestingModule, Test } from "@nestjs/testing";
import { AuthService } from "./auth.service";

describe('AuthController', () => {
    let controller: AuthController;

    let authService: any;
    let usersService: any;

    beforeEach(async () => {
        authService = {
            login() {
                /* empty */
            },
            blacklistToken() {
                /* empty */
            },
        };

        usersService = {
            create() {
                /* empty */
            },
        };

        const module: TestingModule = await Test.createTestingModule({
            controllers: [AuthController],
            providers: [
                {
                    provide: AuthService,
                    useValue: authService,
                },
                {
                    provide: UsersService,
                    useValue: usersService,
                },
            ],
        }).compile();

        controller = module.get<AuthController>(AuthController);
    });

    it('should be defined', () => {
        // Arrange & Act & Assert
        expect(controller).toBeDefined();
    });
});