import { ShowUserDTO, RegisterUserDTO, LoginUserDTO } from './../models/users';
import { UsersService } from './../users/users.service';
import {
  Controller,
  Body,
  Delete,
  UseGuards,
  Post,
  Get,
  ValidationPipe,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthGuard } from '@nestjs/passport';
import { Token } from '../common/decorators/token.decorator';
import {
  ApiUseTags,
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiBadRequestResponse,
} from '@nestjs/swagger';
import { UserRole } from '../common/enums/user-role.enum';
import { AuthGuardWithBlacklisting } from '../common/guards/blacklist.guard';

@Controller('auth')
@ApiUseTags('auth')
@ApiBearerAuth()
export class AuthController {
  public constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
  ) {}

  @Get('auth-test')
  @UseGuards(AuthGuard('accessJwt'))
  tempAuth() {
    return { auth: 'works' };
  }

  @Post('login')
  public async loginUser(@Body() user: LoginUserDTO) {
    return await this.authService.login(user);
  }

  @ApiCreatedResponse({
    description: 'Successfuly registered',
    type: ShowUserDTO,
  })
  @ApiBadRequestResponse({
    description: 'User with such username already exists',
  })
  @Post('register')
  public async register(
    @Body(new ValidationPipe({ whitelist: true, transform: true }))
    userDTO: RegisterUserDTO,
  ): Promise<ShowUserDTO> {
    const { username, email, password } = userDTO;
    const user: ShowUserDTO = await this.usersService.create(
      {
        username,
        email,
        password,
      },
      UserRole.User,
    );
    return user;
  }

  @Delete('logout')
  @UseGuards(AuthGuardWithBlacklisting)
  public async logoutUser(@Token() token: string) {
    this.authService.blacklistToken(token);

    return {
      msg: 'Successful logout!',
    };
  }
}
