import { plainToClass } from 'class-transformer';
import { RestException } from './../common/exceptions/rest.exception';
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../users/users.service';
import { LoginUserDTO, UserDTO, ShowUserDTO } from '../models/users';
import { User } from '../database/entities';

@Injectable()
export class AuthService {
  private readonly blacklist: string[] = [];

  public constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) { }

  public async login(user: LoginUserDTO): Promise<any> {
    const foundUser: User = await this.usersService.findUserEntity(user.username, []);

    if (!foundUser) {
      throw new RestException(
        {
          error: 'Bad request',
          message: 'Wrong username or password',
          condition: 400,
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    if (!(await this.usersService.validatePassword(user))) {
      throw new RestException(
        {
          error: 'Bad request',
          message: 'Wrong username or password',
          condition: 400,
        },
        HttpStatus.BAD_REQUEST,
      );
    }

    const payload: ShowUserDTO = { ...plainToClass(ShowUserDTO, foundUser, { excludeExtraneousValues: true }) };

    return {
      token: await this.jwtService.signAsync(payload),
    };
  }

  public async validateUserToken(username: string): Promise<ShowUserDTO> {
    return await this.usersService.getUser(username);
  }

  public async validateUser(userDTO: LoginUserDTO): Promise<ShowUserDTO> {
    if (await this.usersService.validatePassword(userDTO)) {
      return await this.usersService.getUser(userDTO.username);
    } else {
      throw new HttpException('Invalid credentials', HttpStatus.UNAUTHORIZED);
    }
  }

  public blacklistToken(token: string): void {
    this.blacklist.push(token);
  }

  public isTokenBlacklisted(token: string): boolean {
    return this.blacklist.includes(token);
  }
}
