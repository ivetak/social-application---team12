import { TokenPayloadDTO } from './../../models/auth/token-payload.dto';
import { AuthService } from './../auth.service';
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy, VerifiedCallback } from 'passport-jwt';
import { ConfigService } from '../../config';
import { plainToClass } from 'class-transformer';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'accessJwt') {
    constructor(
        private readonly authService: AuthService,
        private readonly configService: ConfigService,
    ) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: configService.tokenSecret,
            ignoreExpiration: false,
        });
    }

    public async validate(payload: any, done: VerifiedCallback) {
        const tokenPayload: TokenPayloadDTO = plainToClass(
            TokenPayloadDTO,
            payload,
            { excludeExtraneousValues: true },
        );

        const user = await this.authService.validateUserToken(tokenPayload.username);

        if (!user) {
            return done(
                new HttpException('Unauthorized access', HttpStatus.UNAUTHORIZED),
                false,
            );
        }
        return done(false, { ...user, exp: tokenPayload.exp })
    }
}
