import { Injectable } from '@nestjs/common';
import { User } from '../database/entities/user.entity';

@Injectable()
export class SocketService {
  private pool: Array<[string, WebSocket]> = [];

  constructor(
  ) {}

  async add(id: string, socket: WebSocket) {
    this.pool.push([id, socket]);

    await this.pushUnread(id, socket);
  }

  async remove(socket: WebSocket) {
    this.pool = this.pool.filter(([, s]) => s !== socket);
  }

  async pushNew(user: User, notification: Notification) {
    const toNotify = this.pool.filter(([id]) => id === user.id);

    toNotify.forEach(([, s]) => s.send(JSON.stringify([notification])));
  }

  async pushUnread(id: string, socket: WebSocket) {
    // const unread = await this.notificationRepo.find({
    //   where: {
    //     to: id,
    //     read: false,
    //   },
    // });

    // socket.send(JSON.stringify(unread));
  }

}
