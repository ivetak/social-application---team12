import { NotificationsModule } from './../notifications/notifications.module';
import { CONFIG_PATH } from './../common/constants/config.constants';
import { ConfigModule } from './../config/config.module';
import { AuthModule } from './../auth/auth.module';
import { Module, Global, HttpModule } from '@nestjs/common';
import { CommonModule } from '../common/common.module';

@Global()
@Module({
  imports: [
    ConfigModule.register({ folder: CONFIG_PATH }),
    CommonModule,
    AuthModule,
    HttpModule,
    NotificationsModule,
  ],
  exports: [
    ConfigModule,
    CommonModule,
    AuthModule,
    HttpModule,
    NotificationsModule,
  ],
})
export class CoreModule {}
