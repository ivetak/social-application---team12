import { PostDTO } from './../models/post/post.dto';
import { AuthGuardWithBlacklisting } from './../common/guards/blacklist.guard';
import { ShowUserDTO } from './../models/users/show-user.dto';
import { CreateCommentDTO } from './../models/comments/create-comment.dto';
import { CommentsService } from './comments.service';
import { Controller, UseGuards, Get, Param, Post, Body, ValidationPipe, Delete, Patch } from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags, ApiImplicitParam } from '@nestjs/swagger';
import { CommentDTO } from '../models/comments/comment.dto';
import { userDecorator } from '../common/decorators/user.decorator';
import { UpdateCommentDTO } from '../models/comments/update-comment.dto';


@ApiBearerAuth()
@ApiUseTags('comments')
@Controller('comments')
export class CommentsController {
    constructor(private readonly commentsService: CommentsService) { }

    @UseGuards(AuthGuardWithBlacklisting)
    @ApiImplicitParam({ name: 'commentId', required: false })
    @Get('/:commentId')
    async getPostComments(
        @Param('commentId') id: string,
    ): Promise<CommentDTO | CommentDTO[]> {
        if (id) {
            return await this.commentsService.findCommentById(id);
        }

        return await this.commentsService.getPostComments();
    }

    @UseGuards(AuthGuardWithBlacklisting)
    @Post('/:postId')
    async writeComment(
        @Body(new ValidationPipe({ transform: true, whitelist: true }))
        comment: CreateCommentDTO,
        @userDecorator('user') user: ShowUserDTO,
        @Param('postId') postId: string,
    ): Promise<PostDTO> {
        return await this.commentsService.createComment(
            comment,
            user.username,
            postId,
        )
    }

    @UseGuards(AuthGuardWithBlacklisting)
    @Delete(':id')
    async deleteComment(
        @Param('id') id: string,
        @userDecorator() user: ShowUserDTO): Promise<PostDTO> {
        return await this.commentsService.deleteComment(id, user.username);
    }

    @UseGuards(AuthGuardWithBlacklisting)
    @Patch('/:id')
    async updateComment(
        @Param('id') commentId: string,
        @Body(new ValidationPipe({ whitelist: true, transform: true }))
        comment: UpdateCommentDTO,
        @userDecorator('user') user: ShowUserDTO,
    ): Promise<CommentDTO> {
        return await this.commentsService.updateComment(
            commentId,
            comment,
            user.username,
        )
    }
}
