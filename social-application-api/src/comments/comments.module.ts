import { User } from './../database/entities/user.entity';
import { Comment } from './../database/entities/comment.entity';
import { CommentsController } from './comments.controller';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { CommentsService } from './comments.service';
import { Post } from '../database/entities/post.entity';

@Module({
    imports: [
        TypeOrmModule.forFeature([Comment, User, Post]),
    ],
    controllers: [CommentsController],
    providers: [CommentsService],
    exports: [CommentsService],
})
export class CommentsModule { }

