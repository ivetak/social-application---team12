import { PostDTO } from './../models/post/post.dto';
import { CreateCommentDTO } from './../models/comments/create-comment.dto';
import { CommentDTO } from './../models/comments/comment.dto';
import { plainToClass } from 'class-transformer';
import { Post } from './../database/entities/post.entity';
import { User } from './../database/entities/user.entity';
import { Comment } from './../database/entities/comment.entity';
import { Injectable, HttpStatus, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, In } from 'typeorm';
import { RestException } from '../common/exceptions/rest.exception';
import { UpdateCommentDTO } from '../models/comments/update-comment.dto';
import { ShowCommentDTO } from '../models/comments/show-comment.dto';

@Injectable()
export class CommentsService {
    constructor(
        @InjectRepository(Comment) private readonly commentsRepo: Repository<Comment>,
        @InjectRepository(User) private readonly userRepo: Repository<User>,
        @InjectRepository(Post) private readonly postRepo: Repository<Post>,
    ) { }

    async getPostComments(): Promise<CommentDTO[]> {
        return plainToClass(
            CommentDTO,
            await this.commentsRepo.find({
                where: { isDeleted: false },
                relations: ['author', 'post'],
            }),
            { excludeExtraneousValues: true },
        )

    }

    async findCommentById(id: string): Promise<CommentDTO> {
        const foundComment: Comment = await this.commentsRepo.findOne({
            where: { id },
            relations: ['author', 'post'],
        });

        if (foundComment === undefined || foundComment.isDeleted) {
            throw new RestException(
                {
                    error: 'Not found',
                    message: 'Comment is not found',
                    condition: 404,
                },
                HttpStatus.NOT_FOUND,
            );
        }
        return plainToClass(CommentDTO, foundComment, {
            excludeExtraneousValues: true,
        });
    }

    async createComment(
        comment: CreateCommentDTO,
        username: string,
        postId: string,
    ): Promise<PostDTO> {

        const foundUser: User = await this.userRepo.findOne({ username });
        const foundPost: Post = await this.postRepo.findOne({ id: postId });

        if (!foundUser || foundUser === undefined || foundUser.isDeleted) {
            throw new RestException(
                {
                    error: 'Bad request',
                    message: 'User is not found',
                    condition: 400,
                },
                HttpStatus.BAD_REQUEST,
            );
        }

        if (!foundPost || foundPost === undefined || foundPost.isDeleted) {
            throw new RestException(
                {
                    error: 'Bad request',
                    message: 'Post is not found',
                    condition: 400,
                },
                HttpStatus.BAD_REQUEST,
            );
        }

        const createdComment: Comment = this.commentsRepo.create(comment);

        createdComment.author = foundUser;
        createdComment.post = Promise.resolve(foundPost);

        const savedComment: Comment = await this.commentsRepo.save(createdComment);

        const postEntity: Post = await this.postRepo.findOne({
            where: { id: postId },
            relations: ['author', 'comments', 'comments.author'],
        })

        return plainToClass(
            PostDTO,
            { ...postEntity },
            { excludeExtraneousValues: true },
        );

    }

    async deleteComment(id: string, username: string): Promise<PostDTO> {
        const foundComment: Comment = await this.commentsRepo.findOne({ id });
        const foundUser: User = await this.userRepo.findOne({
            where: { username },
        })

        if (!foundComment) {
            throw new RestException(
                {
                    error: 'Bad request',
                    message: 'Comment is not found',
                    condition: 404,
                },
                HttpStatus.BAD_REQUEST,
            );
        }

        if (!foundUser) {
            throw new UnauthorizedException();
        }

        if (!(foundComment.author.username === foundUser.username)) {
            throw new RestException(
                {
                    error: 'Forbidden',
                    message: 'User is not author',
                    condition: 403,
                },
                HttpStatus.FORBIDDEN,
            );
        }

        const deletedComment: Comment = await this.commentsRepo.save({
            ...foundComment,
            isDeleted: true,
            post: null,
        });

        const foundPost: Post = await this.postRepo.findOne({
            where: { id: (await foundComment.post).id },
            relations: ['comments', 'comments.author'],
        });

        return plainToClass(PostDTO, foundPost,
            { excludeExtraneousValues: true });
    }

    async updateComment(
        commentId: string,
        comment: UpdateCommentDTO,
        username: string,
    ): Promise<CommentDTO> {

        const foundUser: User = await this.userRepo.findOne({
            where: { username },
        });

        const foundComment: Comment = await this.commentsRepo.findOne({
            where: { id: commentId },
            relations: ['author', 'post'],
        });

        if (foundUser && foundComment) {
            const userIsAuthor = (await foundComment.author).id === foundUser.id;
            if (userIsAuthor) {
                if (comment.content) {
                    foundComment.content = comment.content;
                }
                const savedComment: Comment = await this.commentsRepo.save(foundComment);

                return plainToClass(CommentDTO, savedComment, {
                    excludeExtraneousValues: true,
                });
            }
        }

    }

    async getUserComments(username: string): Promise<ShowCommentDTO[]> {
        const foundUser: User = await this.userRepo.findOne({
            where: { username },
            relations: ['comments'],
        });

        if (foundUser) {
            const existingCommentsInUser: Comment[] = (await foundUser.comments)
                .filter((comment: Comment) => !comment.isDeleted);

            const commentsEntities: Comment[] = await this.commentsRepo.find({
                where: {
                    id: In(existingCommentsInUser.map((comment: Comment) => comment.id)),
                    isDeleted: false,
                },
                relations: ['author', 'post'],
            });
            return plainToClass(ShowCommentDTO, commentsEntities, {
                excludeExtraneousValues: true,
            })
        }
    }


}