import { ImgurModule } from './../post/imgur.module';
import { Post } from './../database/entities/post.entity';
import { Role } from '../database/entities/role.entity';
import { Module } from '@nestjs/common';
import { User } from '../database/entities/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';

@Module({
  imports: [
      TypeOrmModule.forFeature([User, Role, Post]),
      ImgurModule,
  ],
  controllers: [UsersController],
  providers: [UsersService],
  exports: [UsersService],
})
export class UsersModule {}
