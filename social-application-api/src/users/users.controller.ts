import { ImgurService } from './../post/imgur.service';
import { userDecorator } from './../common/decorators/user.decorator';
import { PostDTO } from './../models/post/post.dto';
import { AuthGuardWithBlacklisting } from './../common/guards/blacklist.guard';
import {
  UpdateUserRolesDTO,
  UserDTO,
  ShowUserDTO,
  CreateUserDTO,
} from './../models/users';
import { UserRole } from './../common/enums/user-role.enum';
import { UsersService } from './users.service';
import {
  ApiUseTags,
  ApiBearerAuth,
  ApiResponse,
  ApiCreatedResponse,
  ApiImplicitQuery,
  ApiImplicitParam,
  ApiConsumes,
} from '@nestjs/swagger';
import {
  Controller,
  Get,
  Param,
  Post,
  HttpCode,
  HttpStatus,
  Body,
  Put,
  Delete,
  UseGuards,
  Query,
  ValidationPipe,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
import { Token, ApiImplicitFormData } from '../common/decorators';
import { FileInterceptor } from '@nestjs/platform-express';

@ApiUseTags('users')
@ApiBearerAuth()
@Controller('users')
export class UsersController {
  public constructor(
    private readonly usersService: UsersService,
    private readonly imgurService: ImgurService,
  ) {}

  @UseGuards(AuthGuardWithBlacklisting)
  @ApiResponse({ status: 200, description: 'All users requested in response' })
  @Get('/:userIdentifier')
  public async getUsers(
    @Param('userIdentifier') userIdentifier: string,
  ): Promise<ShowUserDTO[] | ShowUserDTO> {
    if (userIdentifier) {
      return await this.usersService.getUser(userIdentifier);
    } else {
      return await this.usersService.getAllUsers();
    }
  }

  @ApiImplicitQuery({ name: 'page', required: false })
  @ApiImplicitQuery({ name: 'limit', required: false })
  @ApiImplicitQuery({ name: 'username', required: false })
  @ApiResponse({ status: 200, description: 'All users requested in response' })
  @Get('/')
  public async searchUsers(
    @Query('page') page: number = 0,
    @Query('limit') limit: number = 100,
    @Query('username') username: string = '',
  ): Promise<ShowUserDTO[] | ShowUserDTO> {
    limit = limit > 100 ? 100 : limit;

    return await this.usersService.getAllUsers(page, limit, username);
  }

  @UseGuards(AuthGuardWithBlacklisting)
  @Get('/:userId/posts')
  public async getUserPosts(
    @Param('userId') userId: string,
    @Query('skip') skip: number = 0,
    @Query('take') take: number = 10,
    @userDecorator() user: UserDTO,
  ): Promise<PostDTO[]> {
    return await this.usersService.getUserPosts(user.id, userId, skip, take);
  }

  @UseGuards(AuthGuardWithBlacklisting)
  @ApiCreatedResponse({ description: 'User created' })
  @Post()
  @HttpCode(HttpStatus.CREATED)
  public async addNewUser(@Body() user: CreateUserDTO): Promise<ShowUserDTO> {
    return await this.usersService.create(
      {
        username: user.username,
        email: user.email,
        password: user.password,
      },
      ...(!user.roles || user.roles.length <= 0 ? [UserRole.User] : user.roles),
    );
  }

  @UseGuards(AuthGuardWithBlacklisting)
  @Put('/:id/roles')
  public async updateUserRoles(
    @Param('id') userId: string,
    @Body() updateUserRoles: UpdateUserRolesDTO,
  ): Promise<ShowUserDTO> {
    return await this.usersService.updateUserRoles(userId, updateUserRoles);
  }

  @UseGuards(AuthGuardWithBlacklisting)
  @ApiConsumes('multipart/form-data')
  @ApiImplicitFormData({ name: 'avatar', required: true, type: 'file' })
  @UseInterceptors(FileInterceptor('avatar'))
  @ApiBearerAuth()
  @Put('avatar')
  public async updateUserAvatar(
    @UploadedFile() avatar,
    @userDecorator() user: ShowUserDTO,
  ): Promise<UserDTO> {
    const avatarUrl = (await this.imgurService.uploadImage(avatar)).directLink;
    return await this.usersService.changeUserAvatar(avatarUrl, user.username);
  }

  @UseGuards(AuthGuardWithBlacklisting)
  @Delete('/:id')
  public async deleteUser(@Param('id') userId: string): Promise<ShowUserDTO> {
    return await this.usersService.deleteUser(userId);
  }

  @ApiImplicitParam({ name: 'id', required: true })
  @Put('/:id/followers')
  @UseGuards(AuthGuardWithBlacklisting)
  public async followUser(
    @userDecorator('followerUser') followerUser: ShowUserDTO,
    @Param('id') userToFollowId: string,
  ): Promise<ShowUserDTO> {
    return await this.usersService.follow(followerUser.id, userToFollowId);
  }

  @ApiImplicitParam({ name: 'id', required: true })
  @Delete('/:id/followers')
  @UseGuards(AuthGuardWithBlacklisting)
  public async unFollowUser(
    @userDecorator('followerUser') followerUser: ShowUserDTO,
    @Param('id') userToFollowId: string,
  ): Promise<ShowUserDTO> {

    return await this.usersService.unFollow(followerUser.id, userToFollowId);
  }


  @UseGuards(AuthGuardWithBlacklisting)
  @Get('/:userId/followers')
  public async getUserFollowers(
    @Param('userId') userId: string,
    @userDecorator() user: UserDTO,
  ): Promise<UserDTO[] | UserDTO> {
    return await this.usersService.getUserFollowers(userId);
  }

  @UseGuards(AuthGuardWithBlacklisting)
  @Get('/:userId/following')
  public async getUserFollowing(
    @Param('userId') userId: string,
    @Token() user: UserDTO,
  ): Promise<UserDTO[] | UserDTO> {
    return await this.usersService.getUserFollowing(userId);
  }
}
