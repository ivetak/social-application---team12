import { UserEntityRelations } from './../common/enums/user-entity-relations.enum';
import { PostDTO } from './../models/post/post.dto';
import {
  passwordCompare,
  passwordHash,
} from '../common/constants/auth.constants';
import {
  UpdateUserRolesDTO,
  RegisterUserDTO,
  UserDTO,
  ShowUserDTO,
} from '../models/users';
import { UserRole } from './../common/enums/user-role.enum';
import { plainToClass } from 'class-transformer';
import { Injectable, HttpStatus, UnauthorizedException } from '@nestjs/common';
import { Repository, In, Like } from 'typeorm';
import { ConfigService } from '../config';
import { InjectRepository } from '@nestjs/typeorm';
import { RestException } from '../common/exceptions';
import { User } from '../database/entities/user.entity';
import { Role } from '../database/entities/role.entity';
import { Post } from '../database/entities/post.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(Role) private readonly rolesRepository: Repository<Role>,
    @InjectRepository(Post) private readonly postsRepository: Repository<Post>,
    private readonly config: ConfigService,
  ) {}

  public async getAllUsers(
    page: number = 1,
    limit: number = 10,
    username: string = '',
    withDeleted: boolean = false,
  ): Promise<ShowUserDTO[]> {
    const users: User[] = await this.userRepository.find({
      where: {
        isDeleted: withDeleted,
        username: Like(`%${username}%`),
      },
      take: limit,
      skip: limit * page,
      relations: ['followers', 'following'],
    });
    return plainToClass(UserDTO, users, { excludeExtraneousValues: true });
  }

  public async getUser(
    userIdentifier: string,
    relations: string[] = ['followers', 'following'],
  ): Promise<UserDTO> {
    const foundUser = await this.findUser(userIdentifier, relations);
    if (foundUser) {
      return foundUser;
    } else {
      throw new RestException(
        { error: 'Not found', message: 'User is not found', condition: 404 },
        HttpStatus.NOT_FOUND,
      );
    }
  }

  public async create(
    userDTO: RegisterUserDTO,
    ...roles: UserRole[]
  ): Promise<ShowUserDTO> {
    const foundUser: User = await this.userRepository.findOne({
      username: userDTO.username,
    });
    if (foundUser) {
      throw new RestException(
        {
          error: 'Bad request',
          message: 'User with such username already exists!',
          condition: 400,
        },
        HttpStatus.BAD_REQUEST,
      );
    }

    const userEntity: User = this.userRepository.create(userDTO);
    userEntity.password = await passwordHash(userDTO.password);
    const savedUser: User = await this.userRepository.save(userEntity);
    return await this.updateUserRoles(savedUser.id, { roles });
  }

  public async updateUserRoles(
    id: string,
    updateUserRoles: UpdateUserRolesDTO,
  ): Promise<ShowUserDTO> {
    const foundUser: User = await this.userRepository.findOne({ id });

    if (foundUser && !foundUser.isDeleted && updateUserRoles.roles.length > 0) {
      const roleEntities: Role[] = await this.rolesRepository.find({
        where: { roleName: In(updateUserRoles.roles) },
      });
      foundUser.roles = roleEntities;
      const savedUser: User = await this.userRepository.save(foundUser);

      return plainToClass(ShowUserDTO, savedUser, {
        excludeExtraneousValues: true,
      });
    }
  }

  public async deleteUser(userId: string): Promise<ShowUserDTO> {
    const foundUser: User = await this.userRepository.findOne({
      id: userId,
    });
    if (foundUser) {
      foundUser.isDeleted = true;
      const savedUser: User = await this.userRepository.save(foundUser);

      return;
    }
  }

  public async follow(
    followerId: string,
    userToFollowId: string,
  ): Promise<UserDTO> {
    const userToFollow: User = await this.findUserEntity(userToFollowId);

    const userFollower: User = await this.findUserEntity(followerId, [
      UserEntityRelations.following,
    ]);
    if (userToFollow.id === userFollower.id) {
      throw new RestException(
        {
          error: 'Bad request',
          message: 'Cannot follow yourself!',
          condition: 400,
        },
        HttpStatus.BAD_REQUEST,
      );
    }

    const followedUsers = [...(await userFollower.following)];

    if (followedUsers.includes(userToFollow)) {
      throw new RestException(
        {
          error: 'Bad request',
          message: 'You already followed this user!',
          condition: 400,
        },
        HttpStatus.BAD_REQUEST,
      );
    }

    userFollower.following = [...followedUsers, userToFollow];

    const savedUserFollower = await this.userRepository.save(userFollower);

    const userToFollowEntity = await this.findUserEntity(userToFollow.id);

    return plainToClass(UserDTO, userToFollowEntity, {
      excludeExtraneousValues: true,
    });
  }

  public async unFollow(
    followerId: string,
    userToUnfollowId: string,
  ): Promise<UserDTO> {
    const userToUnfollow: User = await this.findUserEntity(userToUnfollowId, [
      UserEntityRelations.followers,
    ]);

    const userFollower: User = await this.findUserEntity(followerId, [
      UserEntityRelations.following,
    ]);

    if (userToUnfollow.id === userFollower.id) {
      throw new RestException(
        {
          error: 'Bad request',
          message: 'Cannot follow yourself!',
          condition: 400,
        },
        HttpStatus.BAD_REQUEST,
      );
    }

    const deletedUserFromFollowers = userToUnfollow.followers.splice(
      userToUnfollow.followers.findIndex(user => user.id === userFollower.id),
      1,
    );

    const deleteUserFromFollowing = userFollower.following.splice(
      userFollower.following.findIndex(user => user.id === userToUnfollow.id),
      1,
    );

    const savedUserFollower = await this.userRepository.save(userFollower);

    const userToUnfollowEntity = await this.findUserEntity(userToUnfollow.id);

    return plainToClass(UserDTO, userToUnfollowEntity, {
      excludeExtraneousValues: true,
    });
  }

  public async getUserFollowers(userId: string): Promise<UserDTO[] | UserDTO> {
    const foundUser = await this.findUserById(userId);
    if (!foundUser) {
      throw new RestException(
        {
          error: 'Bad request',
          message: 'No user found',
          condition: 400,
        },
        400,
      );
    }

    const foundUserFollowers = await this.userRepository.findOne({
      where: { id: userId },
      relations: ['followers', 'following'],
    });

    const userFollowers = foundUserFollowers.followers.map(user => user);

    return plainToClass(UserDTO, userFollowers, {
      excludeExtraneousValues: true,
    });
  }

  public async getUserFollowing(userId: string): Promise<UserDTO[] | UserDTO> {
    const foundUser = await this.findUser(userId);
    if (!foundUser) {
      throw new RestException(
        {
          error: 'Bad request',
          message: 'No user found',
          condition: 400,
        },
        400,
      );
    }

    const foundUserFollowing = await this.userRepository.findOne({
      where: { id: userId },
      relations: ['followers', 'following'],
    });

    const userFollowing = foundUserFollowing.following.map(user => user);

    return plainToClass(UserDTO, userFollowing, {
      excludeExtraneousValues: true,
    });
  }

  public async validatePassword(user: RegisterUserDTO): Promise<boolean> {
    const userEntity: User = await this.findUserEntity(user.username);
    if (userEntity) {
      return await passwordCompare(user.password, userEntity.password);
    } else {
      throw new RestException(
        {
          error: 'Bad request',
          message: 'User with such username already exists!',
          condition: 400,
        },
        HttpStatus.BAD_REQUEST,
      );
    }
  }

  // userIdentifier = id/ username/ email
  public async findUserEntity(
    userIdentifier: string,
    relations: UserEntityRelations[] = [
      UserEntityRelations.followers,
      UserEntityRelations.following,
    ],
  ): Promise<User> {
    const userRelations = relations.map(relation => relation.toString());

    let foundUser = await this.findUserEntityByUsername(
      userIdentifier,
      userRelations,
    );
    if (foundUser) {
      return foundUser;
    } else {
      foundUser = await this.findUserEntityByEmail(
        userIdentifier,
        userRelations,
      );
    }
    if (foundUser) {
      return foundUser;
    } else {
      foundUser = await this.findUserEntityById(userIdentifier, userRelations);
    }

    return foundUser;
  }

  public async getUserPosts(
    loggedUserId: string,
    userId: string,
    skip: number = 0,
    take: number = 10,
  ): Promise<PostDTO[]> {
    const loggedUser = await this.findUserEntity(loggedUserId);
    if (!loggedUser) {
      throw new UnauthorizedException();
    }
    const foundUser = await this.findUserEntity(userId);
    if (!foundUser) {
      throw new RestException(
        { error: 'Bad request', message: 'No user found', condition: 400 },
        400,
      );
    }
    let foundUserPosts: Post[];
    const loggedUserFollowing =
      loggedUser.following
        .map((followedUser: User) => followedUser.username)
        .includes(foundUser.username) ||
      loggedUser.username === foundUser.username;
    if (loggedUserFollowing) {
      foundUserPosts = await this.postsRepository.find({
        where: { author: { id: foundUser.id } },
        relations: ['author', 'comments', 'likes'],
        order: { createDate: 'DESC' },
        skip,
        take,
      });
      return plainToClass(PostDTO, foundUserPosts, {
        excludeExtraneousValues: true,
      });
    }
    foundUserPosts = await this.postsRepository.find({
      where: { author: { id: userId }, public: true },
      relations: ['author', 'comments', 'likes'],
      order: { createDate: 'DESC' },
      skip,
      take,
    });
    return plainToClass(PostDTO, foundUserPosts, {
      excludeExtraneousValues: true,
    });
  }

  public async changeUserAvatar(
    avatarUrl: string,
    username: string,
  ): Promise<UserDTO> {
    const foundUser = await this.findUserEntity(username);
    if (!foundUser) {
      throw new UnauthorizedException();
    }
    foundUser.avatar = avatarUrl;
    const savedUser = await this.userRepository.save(foundUser);
    const userEntity = await this.findUserEntity(username);

    return plainToClass(UserDTO, userEntity, { excludeExtraneousValues: true });
  }

  private async findUser(
    userIdentifier: string,
    relations: string[] = [
      UserEntityRelations.followers,
      UserEntityRelations.following,
    ],
  ) {
    let foundUser = await this.findUserByUsername(userIdentifier, relations);
    if (foundUser) {
      return foundUser;
    } else {
      foundUser = await this.findUserByEmail(userIdentifier, relations);
    }
    if (foundUser) {
      return foundUser;
    } else {
      foundUser = await this.findUserById(userIdentifier, relations);
    }
    return foundUser;
  }

  private async findUserByUsername(
    username: string,
    relations: string[] = ['followers', 'following'],
  ): Promise<UserDTO> {
    const foundUser: User = await this.userRepository.findOne({
      where: { username, isDeleted: false },
      relations,
    });
    if (foundUser) {
      return plainToClass(
        UserDTO,
        { ...foundUser },
        {
          excludeExtraneousValues: true,
        },
      );
    }
  }

  private async findUserByEmail(
    email: string,
    relations: string[] = [],
  ): Promise<UserDTO> {
    const foundUser: User = await this.userRepository.findOne({
      where: { email, isDeleted: false },
      relations,
    });
    if (foundUser) {
      return plainToClass(
        UserDTO,
        { ...foundUser },
        {
          excludeExtraneousValues: true,
        },
      );
    }
  }

  private async findUserById(
    id: string,
    relations: string[] = [],
  ): Promise<UserDTO> {
    const foundUser: User = await this.userRepository.findOne({
      where: { id, isDeleted: false },
      relations: ['followers', 'following'],
    });
    if (foundUser) {
      return plainToClass(
        UserDTO,
        { ...foundUser },
        {
          excludeExtraneousValues: true,
        },
      );
    }
  }

  private async findUserEntityByUsername(
    username: string,
    relations: string[],
  ): Promise<User> {
    return await this.userRepository.findOne({
      where: { username, isDeleted: false },
      relations,
    });
  }

  private async findUserEntityByEmail(
    email: string,
    relations: string[],
  ): Promise<User> {
    return await this.userRepository.findOne({
      where: { email, isDeleted: false },
      relations,
    });
  }

  private async findUserEntityById(
    id: string,
    relations: string[],
  ): Promise<User> {
    return await this.userRepository.findOne({
      where: { id, isDeleted: false },
      relations,
    });
  }
}
